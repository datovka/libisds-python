
This project provides a simplistic wrapper of some libisds functions for Python.

Dependencies:
-------------

* decently recent version of Python 3 (although Python 2.7 should also work).
* libisds
* (GNU) libtool

Build dependencies:
-------------------
* autoconf-archive
* libisds-dev
* libcurl4-openssl-dev on Debian/Ubuntu
* swig
* python3-dev or python-dev

Installation:
-------------

1. Enter the project root directory.
2. Run autogen.sh
3. PYTHON_VERSION=3.6 PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig ./configure
   You may set the PYTHON_VERSION variable to specify which version of Python
   you want to target.
   Use PKG_CONFIG_PATH if your libraries are located in non-standard locations.

   The resulting command may then look like this:
   PYTHON_VERSION=3.7 PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig ./configure --enable-debug
4. make
5. make install

Documentation:
--------------

If you want to build the documentation then you'll need to run the command:
   make doc
Documentation can the be found inside the directory docs/build/html.
It may happen that sphinx-build will crash because is uses different Python
version that the wrapper has been build for. In that case try:
  PYTHON_VERSION=2.7 ./configure
  make clean
  make doc
and change PYTHON_VERSION to the appropriate version.

Examples:
---------

Examples can be found inside the examples directory.

Hints for various distributions:
--------------------------------

* Fedora:
  dnf install autoconf automake libtool autoconf-archive
  dnf install python3-devel # Or python2-devel for python 2.7 target.
  dnf install swig
  dnf install libisds-devel
  dnf install python3-sphinx # Or python2-sphinx for python 2.7 to build documentation.

* Debian/Ubuntu:
  apt-get install autoconf automake libtool autoconf-archive
  apt-get install python3-dev # Or python-dev for python 2.7 target.
  apt-get install swig
  apt-get install libisds-dev
  apt-get install python3-sphinx # Or python-sphinx for python 2.7 to build documentation.
  apt-get install libcurl4-openssl-dev # To make pkg-config to be able to return libisds entries. (???)
