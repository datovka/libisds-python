#!/bin/sh -e

autoheader
libtoolize --copy --force --install
aclocal -I m4
automake --add-missing --copy || /bin/true
autoconf
