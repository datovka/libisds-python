.. pyisds documentation master file, created by
   sphinx-quickstart on Thu Jul 18 15:21:53 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. :caption: Directive is not suported in toctree before version 1.3
   https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html

Documentation of the libisds Python Wrapper
===========================================

.. toctree::
   :maxdepth: 2

   modules/isds

.. autofunction:: isds.isds_set_logging
.. autofunction:: isds.isds_strerror
.. autofunction:: isds.isds_version
.. autofunction:: isds.wrapper_version

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
