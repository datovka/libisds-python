ISDS Python Module Documentation
================================

Here you can find the documentation of libisds-python extension module. This module consists of several classes and a couple of functions.

.. toctree::
	:maxdepth: 1
	:glob:

	isds_types
	isds_auxiliary
	isds_address
	isds_birth_info
	isds_ctx
	isds_db_owner_info
	isds_db_user_info
	isds_document
	isds_envelope
	isds_event
	isds_fulltext_result
	isds_fulltext_result_meta
	isds_hash
	isds_message_author
	isds_message
	isds_person_name
