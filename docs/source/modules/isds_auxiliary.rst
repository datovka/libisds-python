Auxiliary classes
=================

.. automodule:: isds

Class isds_date
---------------

.. autoclass:: isds_date
	:members:
	:undoc-members:

Class isds_time
---------------

.. autoclass:: isds_time
	:members:
	:undoc-members:
