Class isds_db_owner_info
========================

.. automodule:: isds

Class isds_db_owner_info
------------------------
.. autoclass:: isds_db_owner_info
	:members:
	:undoc-members:
