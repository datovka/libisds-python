Class isds_event
================

.. automodule:: isds

Class isds_event
----------------
.. autoclass:: isds_event
	:members:
	:undoc-members:
