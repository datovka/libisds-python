Class isds_fulltext_result
==========================

.. automodule:: isds

Class isds_fulltext_result
--------------------------
.. autoclass:: isds_fulltext_result
	:members:
	:undoc-members:
