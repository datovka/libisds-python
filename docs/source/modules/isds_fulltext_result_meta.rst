Class isds_fulltext_result_meta
===============================

.. automodule:: isds

Class isds_fulltext_result_meta
-------------------------------
.. autoclass:: isds_fulltext_result_meta
	:members:
	:undoc-members:
