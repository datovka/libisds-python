Class isds_message_author
=========================

.. automodule:: isds

Class isds_message_author
-------------------------
.. autoclass:: isds_message_author
	:members:
	:undoc-members:
