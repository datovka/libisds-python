Class isds_person_name
======================

.. automodule:: isds

Class isds_person_name
----------------------
.. autoclass:: isds_person_name
	:members:
	:undoc-members:
