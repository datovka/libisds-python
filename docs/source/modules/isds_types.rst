Built-in types
==============

.. _isds_error:

isds_error
----------

Integer. Describes an library error state. Available values:

    * IE_SUCCESS - No error, just for convenience (means Ok).
    * IE_ERROR - Unspecified error.
    * IE_NOTSUP
    * IE_INVAL
    * IE_INVALID_CONTEXT
    * IE_NOT_LOGGED_IN
    * IE_CONNECTION_CLOSED
    * IE_TIMED_OUT
    * IE_NOEXIST
    * IE_NOMEM
    * IE_NETWORK
    * IE_HTTP
    * IE_SOAP
    * IE_XML
    * IE_ISDS
    * IE_ENUM
    * IE_DATE
    * IE_2BIG
    * IE_2SMALL
    * IE_NOTUNIQ
    * IE_NOTEQUAL
    * IE_PARTIAL_SUCCESS
    * IE_ABORTED
    * IE_SECURITY

.. autofunction:: isds.isds_error2str

.. _isds_log_level:

isds_log_level
--------------

Integer. Verbosity level. Available values:

    * ILL_NONE
    * ILL_CRIT
    * ILL_ERR
    * ILL_WARNING
    * ILL_INFO
    * ILL_DEBUG
    * ILL_ALL

.. _isds_log_facility:

isds_log_facility
-----------------

Integer. Logging facilities. Available values:

    * ILF_NONE
    * ILF_HTTP
    * ILF_SOAP
    * ILF_ISDS
    * ILF_FILE
    * ILF_SEC
    * ILF_XML
    * ILF_ALL

.. _isds_DbType:

.. _isds_pki_format:

isds_pki_format
---------------

Integer. Describes the cryptographic material encoding format. Available values:

    * PKI_FORMAT_PEM - PEM format.
    * PKI_FORMAT_DER - DER format.
    * PKI_FORMAT_ENG - Stored in cryptographic engine.

.. _isds_otp_resolution:

isds_otp_resolution
-------------------

Integer. Describes the One-time password (OTP) authentication resolution status. Available values:

    * OTP_RESOLUTION_SUCCESS - Authentication succeeded.
    * OTP_RESOLUTION_UNKNOWN - Status is unknown.
    * OTP_RESOLUTION_BAD_AUTHENTICATION - Bad log-in, retry.
    * OTP_RESOLUTION_ACCESS_BLOCKED - Access blocked for 60 minutes (brute force attack detected).
    * OTP_RESOLUTION_PASSWORD_EXPIRED - Password has expired. (OTP or regular password expired?)
    * OTP_RESOLUTION_TO_FAST - OTP cannot be sent repeatedly at this rate (minimal delay depends on TOTP window setting).
    * OTP_RESOLUTION_UNAUTHORIZED -  Username is not allowed to access requested URI.
    * OTP_RESOLUTION_TOTP_SENT - OTP (in an SMS) has been generated and sent by ISDS.
    * OTP_RESOLUTION_TOTP_NOT_SENT - OTP (in an SMS) could not been sent. Retry later.

.. autofunction:: isds.isds_otp_resolution2str

isds_DbType
-----------

Integer. Describes the data box type. Available values:

    * DBTYPE_SYSTEM - Special sender value for messages sent by ISDS.
    * DBTYPE_OVM
    * DBTYPE_OVM_NOTAR - Replaced with OVM_PFO.
    * DBTYPE_OVM_EXEKUT - Replaced with OVM_PFO.
    * DBTYPE_OVM_REQ
    * DBTYPE_OVM_FO
    * DBTYPE_OVM_PFO
    * DBTYPE_OVM_PO
    * DBTYPE_PO
    * DBTYPE_PO_ZAK - Replaced with PO.
    * DBTYPE_PO_REQ
    * DBTYPE_PFO
    * DBTYPE_PFO_ADVOK
    * DBTYPE_PFO_DANPOR
    * DBTYPE_PFO_INSSPR
    * DBTYPE_PFO_AUDITOR
    * DBTYPE_FO

.. autofunction:: isds.isds_DbType2str

.. _isds_message_status:

isds_message_status
-------------------

Integer. Describes data message status. Available values:

    * MESSAGESTATE_SENT - Message has entered ISDS.
    * MESSAGESTATE_STAMPED - Message stamped by TSA.
    * MESSAGESTATE_INFECTED - Message contains viruses.
    * MESSAGESTATE_DELIVERED - Message delivered (`dmDeliveryTime` stored).
    * MESSAGESTATE_SUBSTITUTED - Message delivered through fiction, `dmAcceptanceTime` stored.
    * MESSAGESTATE_RECEIVED - Message accepted (by user log-in or user explicit request), `dmAcceptanceTime` stored.
    * MESSAGESTATE_READ - Message has been read by user.
    * MESSAGESTATE_UNDELIVERABLE - Message could not been delivered (e.g. recipient box has been made inaccessible meantime).
    * MESSAGESTATE_REMOVED - Message content deleted.
    * MESSAGESTATE_IN_SAFE - Message stored in long term storage.

.. autofunction:: isds.isds_message_status2str

.. _isds_hash_algorithm:

isds_hash_algorithm
-------------------

Integer. Hash algorithm type. Available values:

    * HASH_ALGORITHM_MD5
    * HASH_ALGORITHM_SHA_1
    * HASH_ALGORITHM_SHA_224
    * HASH_ALGORITHM_SHA_256
    * HASH_ALGORITHM_SHA_384
    * HASH_ALGORITHM_SHA_512

.. autofunction:: isds.isds_hash_algorithm2str

.. _isds_UserType:

isds_UserType
-------------

Integer. User type. Available values:

    * USERTYPE_PRIMARY - Owner of the data box.
    * USERTYPE_ENTRUSTED - User with limited access to the box.
    * USERTYPE_ADMINISTRATOR - User to manage ENTRUSTED_USERs.
    * USERTYPE_OFFICIAL
    * USERTYPE_OFFICIAL_CERT
    * USERTYPE_LIQUIDATOR - Company liquidator.
    * USERTYPE_RECEIVER - Company receiver.
    * USERTYPE_GUARDIAN - Legal guardian.

.. autofunction:: isds.isds_UserType2str

.. _isds_event_type:

isds_event_type
---------------

Integer. Message processing events. Available values:

    * EVENT_UKNOWN - Unknown to the library.
    * EVENT_ACCEPTED_BY_RECIPIENT - Message has been delivered and accepted by recipient action.
    * EVENT_ACCEPTED_BY_FICTION - Message has been delivered, acceptance timed out, considered as accepted.
    * EVENT_UNDELIVERABLE - Recipient data box made inaccessible thus message is undeliverable.
    * EVENT_COMMERCIAL_ACCEPTED - Recipient confirmed acceptance of commercial message.
    * EVENT_ENTERED_SYSTEM - Message entered ISDS (i.e. has just been sent by sender).
    * EVENT_DELIVERED - Message has been delivered.
    * EVENT_PRIMARY_LOGIN - Primary user has logged in.
    * EVENT_ENTRUSTED_LOGIN - Entrusted user with rights to read has logged in.
    * EVENT_SYSCERT_LOGIN - Application authenticated by `system` certificate has logged in.
    * EVENT_UNDELIVERED_AV_CHECK An attachment didn't pass the antivirus check, message has not been delivered.

.. autofunction:: isds.isds_event_type2str

.. _isds_FileMetaType:

isds_FileMetaType
-----------------

Integer. Describes hierarchy in attached documents. Available values:

    * FILEMETATYPE_MAIN - Main document.
    * FILEMETATYPE_ENCLOSURE - Appendix.
    * FILEMETATYPE_SIGNATURE - Digital signature of another document.
    * FILEMETATYPE_META - XML document for ESS (electronic document information system) purposes.

.. autofunction:: isds.isds_FileMetaType2str

.. _isds_raw_type:

isds_raw_type
-------------

Integer. Raw message content representation type. Available values:

    * RAWTYPE_INCOMING_MESSAGE
    * RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE
    * RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE
    * RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE
    * RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE
    * RAWTYPE_DELIVERYINFO
    * RAWTYPE_PLAIN_SIGNED_DELIVERYINFO
    * RAWTYPE_CMS_SIGNED_DELIVERYINFO

.. autofunction:: isds.isds_raw_type2str

.. _isds_sender_type:

isds_sender_type
----------------

Integer. Message sender type. Similar but not equivalent to :ref:`isds_UserType`. Available values:

    * SENDERTYPE_PRIMARY - Owner of the box.
    * SENDERTYPE_ENTRUSTED - User with limited access to the box.
    * SENDERTYPE_ADMINISTRATOR - User to manage ENTRUSTED_USERs.
    * SENDERTYPE_OFFICIAL - ISDS; sender of system message.
    * SENDERTYPE_VIRTUAL - An application (e.g. document information system).
    * SENDERTYPE_OFFICIAL_CERT - ???; Non-normative.
    * SENDERTYPE_LIQUIDATOR - Liquidator of the company; Non-normative.
    * SENDERTYPE_RECEIVER - Receiver of the company.
    * SENDERTYPE_GUARDIAN

.. autofunction:: isds.isds_sender_type2str

.. _isds_fulltext_target:

isds_fulltext_target
--------------------

Integer. Box attribute to search while performing full-text search. Available values:

    * FULLTEXT_ALL - Search in address, organization identifier, and box ID.
    * FULLTEXT_ADDRESS - Search in address.
    * FULLTEXT_IC - Search in organization identifier.
    * FULLTEXT_BOX_ID - Search in box ID.
