#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Download messages.
#

import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

ret = ctx.login_pwd("username", "password", True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)


ret,received_list = ctx.get_received_messages(None, None, None, isds.MESSAGESTATE_ANY, 0, None)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Get received messages returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Have list of %s received messages.\n" % (len(received_list)))

if len(received_list) > 0:
    dmID = received_list[0].envelope.dmID
    ret,message = ctx.get_signed_received_message(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get received message returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stdout.write("Downloaded signed received message %s with the subject: '%s'.\n" % (message.envelope.dmID, message.envelope.dmAnnotation))

        fName = "DZ_%s.zfo" % (dmID)
        zfoFile = open(fName, "wb")
        zfoFile.write(message.raw)
        zfoFile.close()

        sys.stdout.write("Saved as file '%s'.\n" % (fName))

    ret,author = ctx.get_message_sender(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get message sender returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stderr.write("Message was sent by: %s\n" % (author.authorName))

    ret,message = ctx.get_signed_delivery_info(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get signed delivery info returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stdout.write("Downloaded signed delivery info of message %s.\n" % (message.envelope.dmID))

        fName = "DD_%s.zfo" % (dmID)
        zfoFile = open(fName, "wb")
        zfoFile.write(message.raw)
        zfoFile.close()

        sys.stdout.write("Saved as file '%s'.\n" % (fName))


ret,sent_list = ctx.get_sent_messages(None, None, None, isds.MESSAGESTATE_ANY, 0, None)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Get sent messages returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Have list of %s sent messages.\n" % (len(sent_list)))

if len(sent_list) > 0:
    dmID = sent_list[0].envelope.dmID
    ret,message = ctx.get_signed_sent_message(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get sent message returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stdout.write("Downloaded signed sent message %s with the subject: '%s'.\n" % (message.envelope.dmID, message.envelope.dmAnnotation))

        fName = "DZ_%s.zfo" % (dmID)
        zfoFile = open(fName, "wb")
        zfoFile.write(message.raw)
        zfoFile.close()

        sys.stdout.write("Saved as file '%s'.\n" % (fName))

    ret,author = ctx.get_message_sender(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get message sender returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stderr.write("Message was sent by: %s\n" % (author.authorName))

    ret,message = ctx.get_signed_delivery_info(dmID)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Get signed delivery info returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        sys.stdout.write("Downloaded signed delivery info of message %s.\n" % (message.envelope.dmID))

        fName = "DD_%s.zfo" % (dmID)
        zfoFile = open(fName, "wb")
        zfoFile.write(message.raw)
        zfoFile.close()

        sys.stdout.write("Saved as file '%s'.\n" % (fName))


ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
