#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Find box full-text procedure.
#

import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

ret = ctx.login_pwd("username", "password", True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)


page_size = 100
page_num = 0
last_page = False
ret = isds.IE_SUCCESS

while ((ret == isds.IE_SUCCESS) and (not last_page)):
    ret,result = ctx.find_box_fulltext("podání", isds.FULLTEXT_ALL, isds.DBTYPE_SYSTEM, page_size, page_num, True)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Find box full-text %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    else:
        for box in result.boxes:
            sys.stdout.write("Found box:\n\tid: %s\n\tname: %s\n\taddress: %s\n" % (box.dbID, box.name, box.address))
            nameMatches = box.nameMatches
            if len(nameMatches) > 0:
                sys.stdout.write("\t\tmatches in name:")
                for nameMatch in nameMatches:
                    sys.stdout.write(" [%s]" % (box.name[nameMatch[0]:nameMatch[1]]))
                sys.stdout.write("\n")
            addressMatches = box.addressMatches
            if len(addressMatches) > 0:
                sys.stdout.write("\t\tmatches in address:")
                for addressMatch in addressMatches:
                    sys.stdout.write(" [%s]" % (box.address[addressMatch[0]:addressMatch[1]]))
                sys.stdout.write("\n")

    page_num += 1 # Next page.
    last_page = result.last_page
    if last_page:
        sys.stdout.write("Received total: %d.\n" % (result.total_matching_boxes))


ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
