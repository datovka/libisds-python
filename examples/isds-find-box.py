#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Find box procedure.
#

import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

ret = ctx.login_pwd("username", "password", True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

# Search criteria
crit = isds.isds_db_owner_info()
crit.dbType = isds.DBTYPE_OVM
crit.firmName = "Barbucha Holding"

# The first search should fail, because Barbucha Holding is not OVM.
ret,identifiers = ctx.find_box(crit)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Find box %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))

crit.dbType = isds.DBTYPE_PO
ret,identifiers = ctx.find_box(crit)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Find box %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
else:
    for info in identifiers:
        sys.stdout.write("Found box:\n\tid: %s\n\tfirm name: %s\n" % (info.dbID, info.firmName))

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
