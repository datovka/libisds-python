#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Find box procedure.
#

import isds
import sys

sys.stderr.write("The find_personal_box() method is currently not supported.\n")
sys.exit(0)

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

ret = ctx.login_pwd("username", "password", True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

# Search criteria
person_name = isds.isds_person_name()
person_name.pnFirstName = "John"
person_name.pnLastName = "Appleseed"
crit = isds.isds_db_owner_info()
crit.personName = person_name

ret,identifiers = ctx.find_personal_box(crit)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Find box %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
else:
    for info in identifiers:
        sys.stdout.write("Found box:\n\tid: %s\n\tname: %s %s\n" % (info.dbID, info.personName.pnFirstName, info.personName.pnLastName))

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
