#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Load delivery infos and messages.
#

import isds
import sys

def loadFileBinary(fileName):
    f = open(fileName, "rb")
    data = f.read()
    f.close()
    return data

ctx = isds.isds_ctx()

# There is no need to be logged in when handling local messages.

delinfoFileName = "DD_6983498.zfo"
delinfoRaw = loadFileBinary(delinfoFileName)

ret,delinfoType = ctx.guess_raw_type(delinfoRaw)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Could not determine type of %s.\n" % (delinfoFileName))
    sys.stderr.write("Raw type guess returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Determined delivery info type: %s\n" % (isds.isds_raw_type2str(delinfoType)))

ret,message = ctx.load_delivery_info(delinfoType, delinfoRaw)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Could not load delivery info from file %s.\n" % (delinfoFileName))
    sys.stderr.write("Load delivery info returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Loaded delivery info %s from file %s.\n" % (message.envelope.dmID, delinfoFileName))


msgFileName = "DZ_6983498.zfo"
msgRaw = loadFileBinary(msgFileName)

ret,msgType = ctx.guess_raw_type(msgRaw)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Could not determine type of %s.\n" % (msgFileName))
    sys.stderr.write("Raw type guess returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Determined message type: %s\n" % (isds.isds_raw_type2str(msgType)))

ret,message = ctx.load_message(msgType, msgRaw)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Could not load message from file %s.\n" % (msgFileName))
    sys.stderr.write("Load message returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Loaded message %s from file %s.\n" % (message.envelope.dmID, msgFileName))
