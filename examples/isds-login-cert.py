#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username and a certificate.
#

import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

# Certificate and unencrypted key are stored in same file.
cert_file_format = isds.PKI_FORMAT_PEM
cert_file = "cert/cert.p12"

ret = ctx.login_cert("username", password="password",
    cert_format=cert_file_format, cert=cert_file, key_format=cert_file_format, key=cert_file,
    test_env=True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.ping()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.ping()
if (ret != isds.IE_CONNECTION_CLOSED):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
