#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username, password and a HOTP code.
#

import getpass
import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

# Login loop.
logged_in = False
cnt = 0
cnt_max = 5
while not logged_in:
    if (cnt >= cnt_max):
        sys.stderr.write("Maximum amount of retires exceeded.\n")
        sys.exit(1)

    hotp_code = getpass.getpass("Enter HMAC OTP code:")
    ret,otp_ret = ctx.login_hotp("username", "password", hotp_code, True)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
        sys.stderr.write("OTP authentication status is: %s\n" % (isds.isds_otp_resolution2str(otp_ret)))
        sys.stderr.write("HOTP authentication failed. Generate a new code and/or enter it correctly.\n")
    else:
        logged_in = True

    cnt += 1

ret = ctx.ping()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.ping()
if (ret != isds.IE_CONNECTION_CLOSED):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
