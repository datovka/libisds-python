#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username, password and an SMS code.
#

import getpass
import isds
import sys

ctx = isds.isds_ctx()

# Greater timeout is needed for TOTP authentication.
if isds.IE_SUCCESS != ctx.set_timeout(30000):
    sys.stderr.write("Error setting timeout.")

username = "username"
password = "password"

# Ask whether to request an SMS (which you have to pay for).
answer = ""
while answer != "y":
    answer = input("Do you want to request a SMS from the ISDS? (y/n): ")
    if answer == "n":
        sys.exit(0)
    if answer != "y":
        sys.stderr.write("Please use just 'y' or 'n'.\n")
# Request the SMS.
ret,otp_ret = ctx.login_totp(username, password, None, True)
if (ret != isds.IE_PARTIAL_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.stderr.write("OTP authentication status is: %s\n" % (isds.isds_otp_resolution2str(otp_ret)))
    sys.stderr.write("TOTP authentication failed. Server failed to send the SMS with the OTP code.\n")
    sys.exit(1)
else:
    sys.stdout.write("The SMS with the OTP has been sent.\n")

# Login loop.
logged_in = False
cnt = 0
cnt_max = 5
while not logged_in:
    if (cnt >= cnt_max):
        sys.stderr.write("Maximum amount of retires exceeded.\n")
        sys.exit(1)

    totp_code = getpass.getpass("Enter OTP code from the SMS:")
    ret,otp_ret = ctx.login_totp(username, password, totp_code, True)
    if (ret != isds.IE_SUCCESS):
        sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
        sys.stderr.write("OTP authentication status is: %s\n" % (isds.isds_otp_resolution2str(otp_ret)))
        sys.stderr.write("TOTP authentication failed. Enter the received code correctly.\n")
    else:
        logged_in = True

    cnt += 1

ret = ctx.ping()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

ret = ctx.ping()
if (ret != isds.IE_CONNECTION_CLOSED):
    sys.stderr.write("Ping returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
