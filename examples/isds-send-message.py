#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username and a password.
#

import isds
import sys

ctx = isds.isds_ctx()

if isds.IE_SUCCESS != ctx.set_timeout(3000):
    sys.stderr.write("Error setting timeout.")

ret = ctx.login_pwd("username", "password", True)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Login returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)

envelope = isds.isds_envelope()
envelope.dmAnnotation = "isds-python-test"
envelope.dbIDRecipient = "qrdae26"
# Optional:
envelope.dmPublishOwnID = True

documents = []
document = isds.isds_document()
document.dmFileDescr = "text.txt" # Document title (i.e. attachment file name).
document.data = b'Python test.\n'
document.dmMimeType = "" # Let the sever determine this value.
document.dmFileMetaType = isds.FILEMETATYPE_MAIN
documents.append(document)

message = isds.isds_message()
message.envelope = envelope
message.documents = documents

ret,dmID = ctx.send_message(message)
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Send message returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
else:
    sys.stdout.write("Message has been sent under the ID: %s.\n" % (dmID))

ret = ctx.logout()
if (ret != isds.IE_SUCCESS):
    sys.stderr.write("Logout returned %s: %s, %s.\n" % (isds.isds_error2str(ret), isds.isds_strerror(ret), ctx.long_message()))
    sys.exit(1)
