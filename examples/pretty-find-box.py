#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Find box procedure.
#

import sys
from pretty_isds import AuthenticationFailed, Client, ConnectionFailed, Password, DataBoxType

USERNAME = "username"
PASSWORD = "password"
IS_TEST = True

client = Client(Password(USERNAME, PASSWORD), test=IS_TEST)

try:
    data_boxes = client.find_box(DataBoxType.OVM, firm_name="Barbucha Holding")
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
else:
    sys.stdout.write("Found {} boxes.\n".format(len(data_boxes)))
    for data_box in data_boxes:
        sys.stdout.write("{}\n".format(data_box))

try:
    data_boxes = client.find_box(DataBoxType.PO, firm_name="Barbucha Holding")
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
else:
    sys.stdout.write("Found {} boxes.\n".format(len(data_boxes)))
    for data_box in data_boxes:
        sys.stdout.write("{}\n".format(data_box))
