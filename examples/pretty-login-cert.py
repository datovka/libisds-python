#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username and a certificate.
#

import sys
from pretty_isds import AuthenticationFailed, Certificate, Client, ConnectionFailed, KeyFormat

USERNAME = "username"
PASSWORD = "password"
CERT_FILE_FORMAT = KeyFormat.PEM
CERT_FILE = "cert/cert.p12"
KEY_FORMAT = CERT_FILE_FORMAT
KEY_FILE = CERT_FILE
PASSPHRASE = None
IS_TEST = True

client = Client(Certificate(USERNAME, PASSWORD, CERT_FILE_FORMAT, CERT_FILE, KEY_FORMAT, KEY_FILE, PASSPHRASE), test=IS_TEST)
try:
    client.connect()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
else:
    sys.stdout.write("Authentication succeeded.\n")

try:
    client.ping()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
