#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username, password and a HOTP code.
#

import getpass
import sys
from pretty_isds import AuthenticationFailed, Client, ConnectionFailed, Hotp

USERNAME = "username"
PASSWORD = "password"
IS_TEST = True

client = None

# Login loop.
logged_in = False
cnt = 0
cnt_max = 5
while not logged_in:
    if (cnt >= cnt_max):
        sys.stderr.write("Maximum amount of retires exceeded.\n")
        sys.exit(1)

    hotp_code = getpass.getpass("Enter HMAC OTP code:")
    client = Client(Hotp(USERNAME, PASSWORD, hotp_code), test=IS_TEST)

    try:
        client.connect()
    except ConnectionFailed as error:
        sys.stderr.write("Connection failed: {}.\n".format(error))
        sys.exit(1)
    except AuthenticationFailed as error:
        sys.stderr.write("Authentication failed: {}.\n".format(error))
    else:
        sys.stdout.write("Authentication succeeded.\n")
        logged_in = True

    cnt += 1

try:
    client.ping()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
