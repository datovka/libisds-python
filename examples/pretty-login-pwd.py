#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username and a certificate.
#

import sys
from pretty_isds import AuthenticationFailed, Client, ConnectionFailed, Password

USERNAME = "username"
PASSWORD = "password"
IS_TEST = True

client = Client(Password(USERNAME, PASSWORD), test=IS_TEST)
try:
    client.connect()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
else:
    sys.stdout.write("Authentication succeeded.\n")

try:
    client.ping()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
