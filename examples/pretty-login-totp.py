#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Login procedure using a username, password and an SMS code.
#

import getpass
import sys
from pretty_isds import AuthenticationFailed, Client, ConnectionFailed, Totp

USERNAME = "username"
PASSWORD = "password"
IS_TEST = True

# Ask whether to request an SMS (which you have to pay for).
answer = ""
while answer != "y":
    answer = input("Do you want to request a SMS from the ISDS? (y/n): ")
    if answer == "n":
        sys.exit(0)
    if answer != "y":
        sys.stderr.write("Please use just 'y' or 'n'.\n")

client = Client(Totp(USERNAME, PASSWORD), test=IS_TEST)

# Request the SMS.
try:
    client.connect()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
except:
    sys.stdout.write("The SMS with the OTP code has been sent.\n")
else:
    sys.stderr.write("Undefined state.\n")
    sys.exit(1)

# Login loop.
logged_in = False
cnt = 0
cnt_max = 5
while not logged_in:
    if (cnt >= cnt_max):
        sys.stderr.write("Maximum amount of retires exceeded.\n")
        sys.exit(1)

    # Just update the TOTP code.
    client.credentials.totp_code = getpass.getpass("Enter OTP code from the SMS:")

    try:
        client.connect()
    except ConnectionFailed as error:
        sys.stderr.write("Connection failed: {}.\n".format(error))
    except AuthenticationFailed as error:
        sys.stderr.write("Authentication failed: {}.\n".format(error))
    else:
        sys.stdout.write("Authentication succeeded.\n")
        logged_in = True

    cnt += 1

try:
    client.ping()
except ConnectionFailed as error:
    sys.stderr.write("Connection failed: {}.\n".format(error))
    sys.exit(1)
except AuthenticationFailed as error:
    sys.stderr.write("Authentication failed: {}.\n".format(error))
    sys.exit(1)
