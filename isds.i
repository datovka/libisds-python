/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%module isds
%{
	#include <assert.h>
	#include <isds.h>
	#include <stdbool.h>
	#include <string.h>
	#include <sys/time.h> /* struct timeval */

	#include "config.h"
	#include "isds_memory_management.h"
%}

//#define ISDS_DEBUG

%include <attribute.i>
%include <stdint.i> // uint_16_t is a known type now
%include <typemaps.i>

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%include "isds_typemaps.i"
%include "isds_macros.i"

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%include "isds_types.i"

%define DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_fname, _libisds_type_name, _libisds_copy_fname, _swig_type_name)
%ignore _fname;
%inline
%{
	/*!
	 * @brief Take message data from obtained list and transfer them to Python list.
	 *
	 * @param[in,out] item First list item.
	 * @param[in]     copy If non-zero then message data are copied recursively,
	 *                     if 0 then data are extracted from the list.
	 * @return Python list object.
	 */
	static
	PyObject *_fname(struct isds_list *item, int copy)
	{
		PyObject *list = PyList_New(0);
		while (NULL != item) {
			_libisds_type_name *data = (_libisds_type_name *)item->data;
			if (NULL != data) {
				if (0 != copy) {
					data = _libisds_copy_fname(data);
					if (NULL == data) {
						assert(0);
						continue;
					}
				} else {
					item->data = NULL;
				}
				PyObject *obj = SWIG_NewPointerObj(SWIG_as_voidptr(data), _swig_type_name, SWIG_POINTER_OWN |  0 );
				PyList_Append(list, obj);
				/* The list takes sole ownership. */
				Py_DECREF(obj);
			}
			item = item->next;
		}
		return list;
	}
%}
%enddef

%ignore _data_access;
%ignore _data_copy;
%inline
%{
	/*!
	 * @brief Access data from some Python objects.
	 *
	 * @param[out] data_ptr Pointer to data.
	 * @param[out] data_length Number of bytes.
	 * param[in]  py_obj Python object holding the data.
	 * @return 0 on success, -1 on failure.
	 */
	static
	int _data_access(const void **data_ptr, size_t *data_length, PyObject *py_obj)
	{
		if ((NULL == data_ptr) || (NULL == data_length) || (NULL == py_obj)) {
			assert(0);
			return -1;
		}

		Py_ssize_t py_data_len = 0;
		const void *py_data = NULL;

		if (PyBytes_Check(py_obj)) {
			/* PyObject_IsInstance(py_obj, (PyObject *)&PyBytes_Type) */
			/* PyType_IsSubtype(Py_TYPE(py_obj), &PyBytes_Type) */
			py_data = PyBytes_AsString(py_obj);
			py_data_len = PyBytes_Size(py_obj);
		} else if (PyByteArray_Check(py_obj)) {
			/* PyObject_IsInstance(py_obj, (PyObject *)&PyByteArray_Type) */
			/* PyType_IsSubtype(Py_TYPE(py_obj), &PyByteArray_Type) */
			py_data = PyByteArray_AsString(py_obj);
			py_data_len = PyByteArray_Size(py_obj);
		} else if (py_obj == Py_None) {
			/* Already is NULL. */
		} else {
			SWIG_exception_fail(SWIG_ArgError(SWIG_ERROR),
			    "in function '" "_data_access" "', argument " "" " not of type '" "bytes or bytearray" "'");
			/*
			assert(0);
			return -1;
			 */
		}

		*data_ptr = py_data;
		*data_length = py_data_len;

		return 0;

	fail:
		return -1;
	}

	/*!
	 * @brief Copy data from some Python objects.
	 *
	 * @param[out] copy_ptr Pointer no newly created copy. Old value is freed.
	 * @param[out] copy_length Number of bytes.
	 * @param[in]  py_obj Python object holding data to be copied.
	 * @return 0 on success, -1 on failure.
	 */
	static
	int _data_copy(void **copy_ptr, size_t *copy_length, PyObject *py_obj)
	{
		if ((NULL == copy_ptr) || (NULL == copy_length) || (NULL == py_obj)) {
			assert(0);
			return -1;
		}

		size_t py_data_len = 0;
		const void *py_data = NULL;

		if (0 != _data_access(&py_data, &py_data_len, py_obj)) {
			return -1;
		}

		/* Create a copy. */
		void *copy = NULL;
		if ((NULL != py_data) && (0 < py_data_len)) {
			copy = malloc(py_data_len);
			if (NULL == copy) {
				assert(0);
				return -1;
			}
			memcpy(copy, py_data, py_data_len);
		}

		/* Free previous content. */
		if (NULL != *copy_ptr) {
			free(*copy_ptr);
		}

		*copy_ptr = copy;
		*copy_length = py_data_len;

		return 0;
	}
%}

%newobject isds_version;

%feature("docstring") wrapper_version "Return version string of the wrapper code.
The format of the version string may vary.

:note: Do no try to parse it.

:rerurn: (string) Version string.
"
%inline
%{
	const char *wrapper_version(void)
	{
		return PACKAGE_VERSION;
	}
%}

/* ========================================================================= */
/* Preliminary Python code. */
/* ========================================================================= */

%pythoncode
%{
	#
	# Use and do not ignore DeprecationWarning and
	# PendingDeprecationWarning.
	#
	import warnings
	warnings.filterwarnings("module", category=DeprecationWarning)
	warnings.filterwarnings("module", category=PendingDeprecationWarning)
%}

%init
%{
	isds_init();
	//isds_set_logging(ILF_ALL, ILL_ALL);
%}

/* Types added by the wrapper. */
%include "isds_message_author.i"
%include "isds_fulltext_result_meta.i"

/* Types existing in the library. */
%include "isds_address.i"
%include "isds_auxiliary.i"
%include "isds_birth_info.i"
%include "isds_ctx.i"
%include "isds_db_owner_info.i"
%include "isds_db_user_info.i"
%include "isds_document.i"
%include "isds_envelope.i"
%include "isds_event.i"
%include "isds_fulltext_result.i"
%include "isds_ignored.i"
%include "isds_hash.i"
%include "isds_message.i"
%include "isds_person_name.i"

/*
 * %feature("docstring") has to come before the declaration of the method to SWIG.
 */

%feature("docstring") isds_strerror "Return text description of the library error.

:param error: Error code.
:type error: :ref:`isds_error`
:return: (string) Text description.
"

%feature("docstring") isds_version "Return version string of the underlying library.
Version of dependencies can be embedded.

:note: Do no try to parse it.

:rerurn: (string) Version string.
"

%feature("docstring") isds_set_logging "Sets the logging verbosity of the library.

:note: By default the library logs into standard error output.

:param facilities: Bit-masked value derived from :ref:`isds_log_facility`.
:type facilities: int
:param level: Verbosity level.
:type level: :ref:`isds_log_level`
";

%include <isds.h>

typedef struct isds_ctx { }; /* Make SWIG to handle the opaque pointer as a structure. */
