/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_address) isds_Address;

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_Address;

%delobject _isds_Address_free;
%ignore isds_Address_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_Address_free(struct isds_Address *ad)
	{
		fprintf(stderr, "******** ISDS_ADDRESS free 0x%" PRIXPTR " ********\n", (uintptr_t)ad);
		if (NULL != ad) {
			isds_Address_free(&ad);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_Address_free(struct isds_Address *ad)
	{
		if (NULL != ad) {
			isds_Address_free(&ad);
		}
	}
%}
#endif /* ISDS_DEBUG */

%ignore isds_Address_duplicate;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_Address "ISDS address.

This class holds the address content.
"

#if 0 /* Not present in libisds 0.10.8. */
%feature("docstring") isds_Address::adCode "RUIAN address code.

:type: int
"
#endif

%feature("docstring") isds_Address::adCity "City (in Czech: obec).

:type: string
"

#if 0 /* Not present in libisds 0.10.8. */
%feature("docstring") isds_Address::adDistrict "City borough (in Czech: část obce).

:type: string
"
#endif

%feature("docstring") isds_Address::adStreet "Street.

:type: string
"

%feature("docstring") isds_Address::adNumberInStreet "Orientational number (in Czech: orientační číslo).

:type: string
"

%feature("docstring") isds_Address::adNumberInMunicipality "Conscription number (in Czech: popisné číslo).

:type: string
"

%feature("docstring") isds_Address::adZipCode "Postal code (in Czech: PSČ).

:type: string
"

%feature("docstring") isds_Address::adState "State.

:type: string
"


%extend isds_Address {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_Address_free
	%}
}
