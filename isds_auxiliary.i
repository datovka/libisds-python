/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_time) timeval;

%nodefaultctor timeval;
%nodefaultdtor timeval;

typedef struct timeval { };

%feature ("docstring") timeval::sec "Seconds.

:type: int
"
%attribute_custom(timeval, uint32_t, sec, get_sec, set_sec, _isds_timeval_get_sec(self_), _isds_timeval_set_sec(self_, val_));
%ignore _isds_timeval_get_sec;
%ignore _isds_timeval_set_sec;

%feature ("docstring") timeval::usec "Microseconds.

:type: int
"
%attribute_custom(timeval, uint32_t, usec, get_usec, set_usec, _isds_timeval_get_usec(self_), _isds_timeval_set_usec(self_, val_));
%ignore _isds_timeval_get_usec;
%ignore _isds_timeval_set_usec;

%rename(isds_date) tm;

%nodefaultctor tm;
%nodefaultdtor tm;

typedef struct tm { };

%feature ("docstring") tm::year "Year.

:type: int
"
%attribute_custom(tm, uint32_t, year, get_year, set_year, _isds_tm_get_year(self_), _isds_tm_set_year(self_, val_));
%ignore _isds_tm_get_year;
%ignore _isds_tm_set_year;

%feature ("docstring") tm::mon "Month.

:type: int
"
%attribute_custom(tm, uint32_t, mon, get_mon, set_mon, _isds_tm_get_mon(self_), _isds_tm_set_mon(self_, val_));
%ignore _isds_tm_get_mon;
%ignore _isds_tm_set_mon;

%feature ("docstring") tm::mday "Day in month.

:type: int
"
%attribute_custom(tm, uint32_t, mday, get_mday, set_mday, _isds_tm_get_mday(self_), _isds_tm_set_mday(self_, val_));
%ignore _isds_tm_get_mday;
%ignore _isds_tm_set_mday;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%newobject _isds_make_timeval;
#ifdef ISDS_DEBUG
%inline
%{
	struct timeval *_isds_make_timeval(uint32_t sec, uint32_t usec)
	{
		struct timeval *tv = (struct timeval *)malloc(sizeof(*tv));
		fprintf(stderr, "******** TIMEVAL create 0x%" PRIXPTR " ********\n", (uintptr_t)tv);
		if (NULL != tv) {
			tv->tv_sec = sec;
			tv->tv_usec = usec;
		}
		return tv;
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	struct timeval *_isds_make_timeval(uint32_t sec, uint32_t usec)
	{
		struct timeval *tv = (struct timeval *)malloc(sizeof(*tv));
		if (NULL != tv) {
			tv->tv_sec = sec;
			tv->tv_usec = usec;
		}
		return tv;
	}
%}
#endif /* ISDS_DEBUG */

%delobject _isds_timeval_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_timeval_free(struct timeval *tv)
	{
		fprintf(stderr, "******** TIMEVAL free 0x%" PRIXPTR " ********\n", (uintptr_t)tv);
		free(tv);
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_timeval_free(struct timeval *tv)
	{
		free(tv);
	}
%}
#endif /* ISDS_DEBUG */

%inline
%{
	int _isds_timeval_cmp(const struct timeval *tv1, const struct timeval *tv2)
	{
		if ((NULL == tv1) && (NULL == tv2)) {
			return 0;
		} else if ((NULL != tv1) && (NULL != tv2)) {
			if (timercmp(tv1, tv2, <)) {
				return -1;
			} else if (timercmp(tv1, tv2, >)) {
				return 1;
			} else {
				return 0;
			}
		} else if (NULL == tv1) {
			return -1;
		} else {
			return 1;
		}
	}

	uint32_t _isds_timeval_get_sec(const struct timeval *tv)
	{
		assert(NULL != tv);
		return (uint32_t)tv->tv_sec;
	}

	void _isds_timeval_set_sec(struct timeval *tv, uint32_t sec)
	{
		assert(NULL != tv);
		tv->tv_sec = sec;
	}

	uint32_t _isds_timeval_get_usec(const struct timeval *tv)
	{
		assert(NULL != tv);
		return (uint32_t)tv->tv_usec;
	}

	void _isds_timeval_set_usec(struct timeval *tv, uint32_t usec)
	{
		assert(NULL != tv);
		tv->tv_usec = usec;
	}
%}

%newobject _isds_make_tm;
#ifdef ISDS_DEBUG
%inline
%{
	struct tm *_isds_make_tm(int year, int mon, int mday)
	{
		struct tm *tm = (struct tm *)calloc(1, sizeof(*tm));
		fprintf(stderr, "******** TM create 0x%" PRIXPTR " ********\n", (uintptr_t)tm);
		if (NULL != tm) {
			tm->tm_year = year;
			tm->tm_mon = mon;
			tm->tm_mday = mday;
		}
		return tm;
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	struct tm *_isds_make_tm(int year, int mon, int mday)
	{
		struct tm *tm = (struct tm *)calloc(1, sizeof(*tm));
		if (NULL != tm) {
			tm->tm_year = year;
			tm->tm_mon = mon;
			tm->tm_mday = mday;
		}
		return tm;
	}
%}
#endif /* ISDS_DEBUG */

%delobject _isds_tm_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_tm_free(struct tm *tm)
	{
		fprintf(stderr, "******** TM free 0x%" PRIXPTR " ********\n", (uintptr_t)tm);
		free(tm);
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_tm_free(struct tm *tm)
	{
		free(tm);
	}
%}
#endif /* ISDS_DEBUG */

%inline
%{
	int _isds_tm_cmp(const struct tm *tm1, const struct tm *tm2)
	{
		if ((NULL == tm1) && (NULL == tm2)) {
			return 0;
		} else if ((NULL != tm1) && (NULL != tm2)) {
			if (tm1->tm_year != tm2->tm_year) {
				return (tm1->tm_year < tm2->tm_year) ? -1 : 1;
			} else if (tm1->tm_mon != tm2->tm_mon) {
				return (tm1->tm_mon < tm2->tm_mon) ? -1 : 1;
			} else if (tm1->tm_mday != tm2->tm_mday) {
				return (tm1->tm_mday < tm2->tm_mday) ? -1 : 1;
			} else {
				return 0;
			}
		} else if (NULL == tm1) {
			return -1;
		} else {
			return 1;
		}
	}

	int _isds_tm_get_year(const struct tm *tm)
	{
		assert(NULL != tm);
		return tm->tm_year + 1900; /* Years since 1900. */
	}

	void _isds_tm_set_year(struct tm *tm, int year)
	{
		assert(NULL != tm);
		tm->tm_year = year - 1900;
	}

	int _isds_tm_get_mon(const struct tm *tm)
	{
		assert(NULL != tm);
		return tm->tm_mon + 1; /* Months since January. */
	}

	void _isds_tm_set_mon(struct tm *tm, int mon)
	{
		assert(NULL != tm);
		tm->tm_mon = mon - 1;
	}

	int _isds_tm_get_mday(const struct tm *tm)
	{
		assert(NULL != tm);
		return tm->tm_mday;
	}

	void _isds_tm_set_mday(struct tm *tm, int mday)
	{
		assert(NULL != tm);
		tm->tm_mday = mday;
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") timeval "Time value.

This class holds a time value.
"

%extend timeval {
	%pythoncode
	%{
		def __init__(self, sec, usec):
			"""
				Creates a new :class:`isds_time` instance with
				the specified value.

				:param sec: Number of seconds.
				:type sec: int
				:param usec: Number of microseconds.
				:type usec: int
				:return: (:class:`isds_time`)
			"""
			self.this = _isds._isds_make_timeval(sec, usec)

		__swig_destroy__ = _isds._isds_timeval_free

		def __cmp__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (int) -1, 0 or 1 if `self` comes before `other`,
				    is equal or `self` comes after `other` respectively.
			"""
			return _isds._isds_timeval_cmp(self, other)

		def __lt__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is less than `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) == -1

		def __le__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is less than or equal to `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) != 1

		def __eq__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is equal to `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) == 0

		def __ne__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is not equal to `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) != 0

		def __gt__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is greater than `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) == 1

		def __ge__(self, other):
			"""
				Compares two :class:`isds_time` instances according to their values.

				:param other: The second time.
				:type other: :class:`isds_time`
				:return: (bool) True if `self` is greater than or equal to `other`.
			"""
			return _isds._isds_timeval_cmp(self, other) != -1
	%}
}

%feature("docstring") tm "Date value.

This class holds a date value.
"

%extend tm {
	%pythoncode
	%{
		def __init__(self, year, mon, mday):
			"""
				Creates a new :class:`isds_date` instance with
				the specified value.

				:param year: Year.
				:type year: int
				:param mon: Month.
				:type mon: int
				:param mday: Day in month.
				:type mday: int
				:return: (:class:`isds_date`)
			"""
			self.this = _isds._isds_make_tm(year - 1900, mon - 1, mday)

		__swig_destroy__ = _isds._isds_tm_free

		def __cmp__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (int) -1, 0 or 1 if `self` comes before `other`,
				    is equal or `self` comes after `other` respectively.
			"""
			return _isds._isds_tm_cmp(self, other)

		def __lt__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is less than `other`.
			"""
			return _isds._isds_tm_cmp(self, other) == -1

		def __le__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is less than or equal to `other`.
			"""
			return _isds._isds_tm_cmp(self, other) != 1

		def __eq__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is equal to `other`.
			"""
			return _isds._isds_tm_cmp(self, other) == 0

		def __ne__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is not equal to `other`.
			"""
			return _isds._isds_tm_cmp(self, other) != 0

		def __gt__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is greater than `other`.
			"""
			return _isds._isds_tm_cmp(self, other) == 1

		def __ge__(self, other):
			"""
				Compares two :class:`isds_date` instances according to their values.

				:param other: The second date.
				:type other: :class:`isds_date`
				:return: (bool) True if `self` is greater than or equal to `other`.
			"""
			return _isds._isds_tm_cmp(self, other) != -1
	%}
}
