/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_birth_info) isds_BirthInfo;

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_BirthInfo;

/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_BirthInfo::biDate "Birth info.

:note: A copy of the value is returned when read. A copy of the value is stored when written.
:note: Date of birth is in local time at birth place.

:type: :class:`isds_date`
"
%attribute_custom_nodisown_(isds_BirthInfo, struct tm *, biDate,
    get_biDate, set_biDate,
    _isds_BirthInfo_get_biDate(self_), _isds_BirthInfo_set_biDate(self_, val_));
%ignore _isds_BirthInfo_get_biDate;
%ignore _isds_BirthInfo_set_biDate;

%delobject _isds_BirthInfo_free;
%ignore isds_BirthInfo_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_BirthInfo_free(struct isds_BirthInfo *bi)
	{
		fprintf(stderr, "******** ISDS_BIRTH_INFO free 0x%" PRIXPTR " ********\n", (uintptr_t)bi);
		if (NULL != bi) {
			isds_BirthInfo_free(&bi);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_BirthInfo_free(struct isds_BirthInfo *bi)
	{
		if (NULL != bi) {
			isds_BirthInfo_free(&bi);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_BirthInfo_get_biDate;
%inline
%{
	struct tm *_isds_BirthInfo_get_biDate(const struct isds_BirthInfo *bi)
	{
		if (NULL != bi) {
			struct tm *ret = NULL;
			ret = _tm_copy(bi->biDate);
			return ret;
		}
		return NULL;
	}

	void _isds_BirthInfo_set_biDate(struct isds_BirthInfo *bi, const struct tm *tm)
	{
		if (NULL == bi) {
			return;
		}
		if (NULL != bi->biDate) {
			free(bi->biDate);
		}
		bi->biDate = _tm_copy(tm);
	}
%}

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_BirthInfo "ISDS birth info.

This class holds the birth info content.
"

%feature("docstring") isds_BirthInfo::biCity "City of birth.

:type: string
"

%feature("docstring") isds_BirthInfo::biCounty "County of birth (in Czech: okres).

:type: string
"

%feature("docstring") isds_BirthInfo::biState "State of birth.

:type: string
"

%extend isds_BirthInfo {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_BirthInfo_free
	%}
}
