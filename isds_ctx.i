/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%nodefaultctor isds_ctx;
%nodefaultdtor isds_ctx;

%newobject _isds_ctx_create;
#ifdef ISDS_DEBUG
%ignore isds_ctx_create;
%inline
%{
	struct isds_ctx *_isds_ctx_create(void)
	{
		struct isds_ctx *ctx = isds_ctx_create();
		fprintf(stderr, "******** ISDS_CTX create 0x%" PRIXPTR " ********\n", (uintptr_t)ctx);
		return ctx;
	}
%}
#else /* !ISDS_DEBUG */
%rename(_isds_ctx_create) isds_ctx_create;
#endif /* ISDS_DEBUG */

%delobject _isds_ctx_free;
%ignore isds_ctx_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_ctx_free(struct isds_ctx *ctx)
	{
		fprintf(stderr, "******** ISDS_CTX free 0x%" PRIXPTR " ********\n", (uintptr_t)ctx);
		if (NULL != ctx) {
			isds_ctx_free(&ctx);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_ctx_free(struct isds_ctx *ctx)
	{
		if (NULL != ctx) {
			isds_ctx_free(&ctx);
		}
	}
%}
#endif /* ISDS_DEBUG */

%ignore isds_login; /* Preferring dedicated login methods instead. */

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	isds_error _isds_login_pwd(struct isds_ctx *ctx, const char *uname,
	    const char *pwd, bool test_env)
	{
		return isds_login(ctx,
		    test_env ? isds_testing_locator : isds_locator,
		    uname, pwd, NULL, NULL);
	}

	isds_error _isds_login_cert(struct isds_ctx *ctx, const char *uname,
	    const char *pwd, const char *engine,
	    isds_pki_format cert_format, const char *cert,
	    isds_pki_format key_format, const char *key,
	    const char *passphrase, bool test_env)
	{
		struct isds_pki_credentials pki_cred = {
			.engine = (char *)engine,
			.certificate_format = cert_format,
			.certificate = (char *)cert,
			.key_format = key_format,
			.key = (char *)key,
			.passphrase = (char *)passphrase
		};

		return isds_login(ctx,
		    test_env ? isds_cert_testing_locator : isds_cert_locator,
		    uname, pwd, &pki_cred, NULL);
	}

	PyObject *_isds_login_hotp(struct isds_ctx *ctx, const char *uname,
	    const char *pwd, const char *hotp_code, bool test_env)
	{
		struct isds_otp otp = {
			.method = OTP_HMAC,
			.otp_code = (char *)hotp_code,
			.resolution = OTP_RESOLUTION_SUCCESS
		};

		isds_error ret = isds_login(ctx,
		    test_env ? isds_otp_testing_locator : isds_otp_locator,
		    uname, pwd, NULL, &otp);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, SWIG_From_int(otp.resolution));

		return tuple;
	}

	PyObject *_isds_login_totp(struct isds_ctx *ctx, const char *uname,
	    const char *pwd, const char *totp_code, bool test_env)
	{
		struct isds_otp otp = {
			.method = OTP_TIME,
			.otp_code = (char *)totp_code,
			.resolution = OTP_RESOLUTION_SUCCESS
		};

		isds_error ret = isds_login(ctx,
		    test_env ? isds_otp_testing_locator : isds_otp_locator,
		    uname, pwd, NULL, &otp);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, SWIG_From_int(otp.resolution));

		return tuple;
	}
%}

DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_extract_db_owner_info_list2python_list,
    struct isds_DbOwnerInfo, isds_DbOwnerInfo_duplicate, SWIGTYPE_p_isds_DbOwnerInfo)

%ignore isds_FindDataBox;
%inline
%{
	PyObject *_isds_find_box(struct isds_ctx *ctx,
	    const struct isds_DbOwnerInfo *criteria)
	{
		struct isds_list *db_owner_info_list = NULL;
		isds_error ret = isds_FindDataBox(ctx, criteria,
		    &db_owner_info_list);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, ((IE_SUCCESS == ret) || (IE_2BIG == ret)) ?
		    _extract_db_owner_info_list2python_list(db_owner_info_list, 0) :
		    SWIG_Py_Void());

		isds_list_free(&db_owner_info_list);

		return tuple;
	}
%}

#if 0 /* Not present in libisds 0.10.8. */
%ignore isds_FindPersonalDataBox;
%inline
%{
	PyObject *_isds_find_personal_box(struct isds_ctx *ctx,
	    const struct isds_DbOwnerInfo *criteria)
	{
		struct isds_list *db_owner_info_list = NULL;
		isds_error ret = isds_FindPersonalDataBox(ctx, criteria,
		    &db_owner_info_list);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, ((IE_SUCCESS == ret) || (IE_2BIG == ret)) ?
		    _extract_db_owner_info_list2python_list(db_owner_info_list, 0) :
		    SWIG_Py_Void());

		isds_list_free(&db_owner_info_list);

		return tuple;
	}
%}
#endif

%ignore isds_find_box_by_fulltext;
%inline
%{
	PyObject *_isds_find_box_fulltext(struct isds_ctx *ctx, const char *query,
	    const isds_fulltext_target *target, const isds_DbType *box_type,
	    const unsigned long int *page_size, const unsigned long int *page_number,
	    const _Bool *track_matches)
	{
		unsigned long int *total_matching_boxes = NULL;
		unsigned long int *current_page_beginning = NULL;
		unsigned long int *current_page_size = NULL;
		_Bool *last_page = NULL;
		struct isds_list *boxes = NULL;

		isds_error ret = isds_find_box_by_fulltext(ctx, query, target,
		    box_type, page_size, page_number, track_matches,
		    &total_matching_boxes, &current_page_beginning, &current_page_size, &last_page, &boxes);

		struct isds_fulltext_result_meta *frm = NULL;

		if (IE_SUCCESS == ret) {
			frm = malloc(sizeof(*frm));

			frm->total_matching_boxes = total_matching_boxes;
			frm->current_page_beginning = current_page_beginning;
			frm->current_page_size = current_page_size;
			frm->last_page = last_page;
			frm->boxes = boxes;
		}

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(frm), SWIGTYPE_p_isds_fulltext_result_meta, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_extract_message_list2python_list,
    struct isds_message, _isds_message_copy, SWIGTYPE_p_isds_message)

%ignore isds_get_list_of_sent_messages;
%inline
%{
	PyObject *_isds_get_list_of_sent_messages(struct isds_ctx *ctx,
	    const struct timeval *from_time, const struct timeval *to_time,
	    const long int *dmSenderOrgUnitNum, const unsigned int status_filter,
	    const unsigned long int offset, const unsigned long int *max_num)
	{
		unsigned long int number = 0;
		unsigned long int *number_ptr = NULL;
		if (NULL != max_num) {
			number = *max_num;
			number_ptr = &number;
		}

		struct isds_list *message_list = NULL;
		isds_error ret = isds_get_list_of_sent_messages(ctx, from_time,
		    to_time, dmSenderOrgUnitNum, status_filter, offset,
		    number_ptr, &message_list);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    _extract_message_list2python_list(message_list, 0) :
		    SWIG_Py_Void());

		isds_list_free(&message_list);

		return tuple;
	}
%}

%ignore isds_get_list_of_received_messages;
%inline
%{
	PyObject *_isds_get_list_of_received_messages(struct isds_ctx *ctx,
	   const struct timeval *from_time, const struct timeval *to_time,
	   const long int *dmRecipientOrgUnitNum, const unsigned int status_filter,
	   const unsigned long int offset, const unsigned long int *max_num)
	{
		unsigned long int number = 0;
		unsigned long int *number_ptr = NULL;
		if (NULL != max_num) {
			number = *max_num;
			number_ptr = &number;
		}

		struct isds_list *message_list = NULL;
		isds_error ret = isds_get_list_of_received_messages(ctx, from_time,
		    to_time, dmRecipientOrgUnitNum, status_filter, offset,
		    number_ptr, &message_list);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    _extract_message_list2python_list(message_list, 0) :
		    SWIG_Py_Void());

		isds_list_free(&message_list);

		return tuple;
	}
%}

%ignore isds_send_message;
%inline
%{
	PyObject *_isds_send_message(struct isds_ctx *ctx, struct isds_message *msg)
	{
		isds_error ret = isds_send_message(ctx, msg);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		  SWIG_FromCharPtr(msg->envelope->dmID) : SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_get_signed_delivery_info;
%inline
%{
	PyObject *_isds_get_signed_delivery_info(struct isds_ctx *ctx, const char *dmID)
	{
		struct isds_message *msg = NULL;
		isds_error ret = isds_get_signed_delivery_info(ctx, dmID, &msg);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(msg), SWIGTYPE_p_isds_message, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_load_delivery_info;
%inline
%{
	PyObject *_isds_load_delivery_info(struct isds_ctx *ctx,
	    const isds_raw_type raw_type, PyObject *py_obj)
	{
		size_t data_len = 0;
		const void *data = NULL;

		_data_access(&data, &data_len, py_obj);

		struct isds_message *msg = NULL;
		isds_error ret = isds_load_delivery_info(ctx, raw_type,
		    data, data_len, &msg, BUFFER_COPY);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(msg), SWIGTYPE_p_isds_message, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_load_message;
%inline
%{
	PyObject *_isds_load_message(struct isds_ctx *ctx,
	    const isds_raw_type raw_type, PyObject *py_obj)
	{
		size_t data_len = 0;
		const void *data = NULL;

		_data_access(&data, &data_len, py_obj);

		struct isds_message *msg = NULL;
		isds_error ret = isds_load_message(ctx, raw_type,
		    data, data_len, &msg, BUFFER_COPY);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(msg), SWIGTYPE_p_isds_message, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_guess_raw_type;
%inline
%{
	PyObject *_isds_guess_raw_type(struct isds_ctx *ctx, PyObject *py_obj)
	{
		size_t data_len = 0;
		const void *data = NULL;

		_data_access(&data, &data_len, py_obj);

		isds_raw_type raw_type = RAWTYPE_INCOMING_MESSAGE;
		isds_error ret = isds_guess_raw_type(ctx, &raw_type, data, data_len);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_From_int(raw_type) : SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_get_signed_sent_message;
%inline
%{
	PyObject *_isds_get_signed_sent_message(struct isds_ctx *ctx, const char *dmID)
	{
		struct isds_message *msg = NULL;
		isds_error ret = isds_get_signed_sent_message(ctx, dmID, &msg);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(msg), SWIGTYPE_p_isds_message, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_get_signed_received_message;
%inline
%{
	PyObject *_isds_get_signed_received_message(struct isds_ctx *ctx, const char *dmID)
	{
		struct isds_message *msg = NULL;
		isds_error ret = isds_get_signed_received_message(ctx, dmID, &msg);

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(msg), SWIGTYPE_p_isds_message, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

%ignore isds_get_message_sender;
%inline
%{
	PyObject *_isds_get_message_sender(struct isds_ctx *ctx, const char *dmID)
	{
		isds_sender_type *type = NULL;
		char *raw_type = NULL;
		char *name = NULL;

		isds_error ret = isds_get_message_sender(ctx, dmID,
		    &type, &raw_type, &name);

		struct isds_message_author *author = NULL;

		if (IE_SUCCESS == ret) {
			author = malloc(sizeof(*author));

			author->userType = type;
			author->rawUserType = raw_type;
			author->authorName = name;
		}

		PyObject *tuple = PyTuple_New(2);
		PyTuple_SetItem(tuple, 0, SWIG_From_int(ret));
		PyTuple_SetItem(tuple, 1, (IE_SUCCESS == ret) ?
		    SWIG_NewPointerObj(SWIG_as_voidptr(author), SWIGTYPE_p_isds_message_author, SWIG_POINTER_OWN |  0 ) :
		    SWIG_Py_Void());

		return tuple;
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_ctx "This class holds the communication context.

Note that after logging in to the ISDS server the server will shut the connection after a period of inactivity.
You may use a timer which will periodically call :meth:`isds_ctx.ping` to keep the connection alive.

The :class:`isds_ctx` does not provide any means of thread safety.
If you want to share a single :class:`isds_ctx` instance between multiple threads then you have to explicitly protect it against simultaneous access.
"

%extend isds_ctx {
	%pythoncode
	%{
		def __init__(self):
			self.this = _isds._isds_ctx_create()
			if not self.this:
				raise Exception("Can't create new ISDS_CTX")
       
		__swig_destroy__ = _isds._isds_ctx_free

		def long_message(self):
			"""
				Return long message text produced by the library
				(e.g. detailed error message).

				:return: (string) Error message.
			"""
			return _isds.isds_long_message(self)

		def set_timeout(self, timeout_ms):
			"""
				Sets timeout in ms. This value applies for every
				network-related operation (e.g. connecting to
				the server or sending a message).
				Use 0 to disable the timeout limit.

				:param timeout_ms: Timeout in ms.
				:type timeout_ms: int
			"""
			return _isds.isds_set_timeout(self, timeout_ms)

		#def set_opt(self):
		#	pass

		def login_pwd(self, username, password, test_env=False):
			"""
				Connect and log in to the ISDS server using
				the supplied username and password.

				:param username: User login name.
				:type username: string
				:param password: User password.
				:type password: string
				:param test_env: False if connecting to production environment,
				    True if connecting to testing environment.
				:type test_env: bool
				:return: (:ref:`isds_error`) Error state.
			"""
			if not isinstance(test_env, bool):
				raise TypeError("Parameter test_env must be bool.")
			return _isds._isds_login_pwd(self, username, password, test_env)

		def login_cert(self, username, password=None, engine=None,
		    cert_format=PKI_FORMAT_PEM, cert=None, key_format=PKI_FORMAT_PEM, key=None,
		    passphrase=None, test_env=False):
			"""
				Connect and log in to the ISDS server using
				the supplied username, password and certificate data.

				:param username: User login name.
				:type username: string
				:param password: User password. You don't have to supply the password if the account just uses the certificate to log in.
				:type password: string
				:param engine: String identifier of cryptographic engine to use (i.e. where key is stored).
				:type engine: string
				:param cert_format: Certificate format.
				:type cert_format: :ref:`isds_pki_format`
				:param cert: Path to client certificate
				    (or certificate nickname in case of NSS as curl back-end)
				    or key slot identifier inside cryptographic engine.
				    Some cryptographic engines can pair certificates with keys automatically (None value).
				:type cert: string
				:param key_format: Private key format.
				:type key_format: :ref:`isds_pki_format`
				:param key: Path to client private key, or key identifier in case of used engine.
				:param key: string
				:param passphrase: String with password for private key decryption or engine PIN.
				    Use None for no pass-phrase or to be questioned by the underlying engine.
				:type passphrase: string
				:param test_env: False if connecting to production environment,
				    True if connecting to testing environment.
				:type test_env: bool
				:return: (:ref:`isds_error`) Error state.
			"""
			if not isinstance(test_env, bool):
				raise TypeError("Parameter test_env must be bool.")
			return _isds._isds_login_cert(self, username,
			    password, engine, cert_format, cert,
			    key_format, key, passphrase, test_env)

		def login_hotp(self, username, password, hotp_code, test_env=False):
			"""
				Connect and log in to the ISDS server using
				the supplied username, password and HMAC-based OTP method code.

				:param username: User login name.
				:type username: string
				:param password: User password.
				:type password: string
				:param hotp_code: HMAC-based OTP code.
				:type hotp_code: string
				:param test_env: False if connecting to production environment,
				    True if connecting to testing environment.
				:type test_env: bool
				:return: Tuple containing (:ref:`isds_error`, :ref:`isds_otp_resolution`).
			"""
			return _isds._isds_login_hotp(self, username, password, hotp_code, test_env)

		def login_totp(self, username, password, totp_code, test_env=False):
			"""
				Connect and log in to the ISDS server using
				the supplied username, password and time-based OTP method code (SMS code).

				:param username: User login name.
				:type username: string
				:param password: User password.
				:type password: string
				:param totp_code: Time-based OTP code. Use None to request a new SMS code from ISDS.
				:type totp_code: string
				:param test_env: False if connecting to production environment,
				    True if connecting to testing environment.
				:type test_env: bool
				:return: Tuple containing (:ref:`isds_error`, :ref:`isds_otp_resolution`).
				    The returned :ref:`isds_error` usually contains:

				    * IE_SUCCESS - Authentication succeeded.
				    * IE_NOT_LOGGED_IN - Authentication failed. \
				        Fine-grade status can be obtained from :ref:`isds_otp_resolution`. \
				        Error message from server can be obtained using :meth:`isds_ctx.long_message`.
				    * IE_PARTIAL_SUCCESS - Time-based OTP authentication has been requested and the server has sent the OTP code through a side channel. \
				        The application is expected to fill-in the obtained code into `totp_code`. \
				        Keep other arguments unchanged and retry this call to complete the second phase of the TOTP authentication.
			"""
			return _isds._isds_login_totp(self, username, password, totp_code, test_env)

		def logout(self):
			"""
				Log out from ISDS and close connection.

				:return: (:ref:`isds_error`) Error state.
			"""
			return _isds.isds_logout(self)

		def ping(self):
			"""
				Test whether connection to ISDS is alive.
				Sends a dummy request and expects a dummy response.

				:return: (:ref:`isds_error`) Error state.
			"""
			return _isds.isds_ping(self)

		def find_box(self, criteria):
			"""
				Find boxes suiting given criteria.

				:param criteria: Filter criteria, You should fill in at least some members. Following entries are ignored:

				    * :attr:`isds_db_owner_info.aifoIsds`
				    * :attr:`isds_address.adCode` in :attr:`isds_db_owner_info.address`
				    * :attr:`isds_address.adDistrict` in :attr:`isds_db_owner_info.address`

				:type criteria: :class:`isds_db_owner_info`
				:return: Tuple containing (:ref:`isds_error`, list of :class:`isds_db_owner_info`). Returns None instead of list on error.
				    Usual return codes are:

				    * IE_SUCCESS - Search succeeded.
				    * IE_NOEXIST - Data box does not exist.
				    * IE_2BIG - Too many matching boxes exist. Server truncated the results. Resulting list contains valid data.
			"""
			return _isds._isds_find_box(self, criteria)
	%}
}

#if 0 /* Not present in libisds 0.10.8. */
%extend isds_ctx {
	%pythoncode
	%{
		def find_personal_box(self, criteria):
			"""
				Find FO-type boxes suiting given criteria.

				:param criteria: Filter criteria, You should fill in at least some members. Following entries are ignored:

				    * :attr:`isds_db_owner_info.dbType`
				    * :attr:`isds_db_owner_info.ic`
				    * :attr:`isds_person_name.pnLastNameAtBirth` in :attr:`isds_db_owner_info.personName`
				    * :attr:`isds_db_owner_info.firmName`
				    * :attr:`isds_db_owner_info.email`
				    * :attr:`isds_db_owner_info.telNumber`
				    * :attr:`isds_db_owner_info.identifier`
				    * :attr:`isds_db_owner_info.registryCode`
				    * :attr:`isds_db_owner_info.dbState`
				    * :attr:`isds_db_owner_info.dbEffectiveOVM`
				    * :attr:`isds_db_owner_info.dbOpenAddressing`

				:type criteria: :class:`isds_db_owner_info`
				:return: Tuple containing (:ref:`isds_error`, list of :class:`isds_db_owner_info`). Returns None instead of list on error.
				    Usual return codes are:

				    * IE_SUCCESS - Search succeeded.
				    * IE_NOEXIST - Data box does not exist.
				    * IE_2BIG - Too many matching boxes exist. Server truncated the results. Resulting list contains valid data.
			"""
			return _isds._isds_find_personal_box(self, criteria)
	%}
}
#endif

%extend isds_ctx {
	%pythoncode
	%{
		def find_box_fulltext(self, query, target, box_type, page_size, page_number, track_matches):
			"""
				Find boxes matching a given full-text criteria.

				:param query: A non-empty string which consists a phrase to search for.
				:type query: string
				:param target: Selects box attributes to search for `query` words. Pass None if you don't care.
				:type target: :ref:`isds_fulltext_target`
				:param box_type: Restricts searching to given box type. Values have the following meaning:

				    * DBTYPE_SYSTEM - Search in all box types.

				    Pass None to let server to use default value which is DBTYPE_SYSTEM.
				:type box_type: :ref:`isds_DbType`
				:param page_size: Defines the count of boxes to constitute a response page. It counts from zero. Pass None to let the server to use a default value.
				:type page_size: int
				:param page_number: Defines ordinary number of the response page to return. It counts from zero. Pass None to let the server to use a default value.
				:type page_number: int
				:param track_matches: True for marking `query` words found in the box attributes, False for not marking. Pass None to let the server to use default value.
				:type track_matches: bool
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_fulltext_result_meta`). Returns None instead of :class:`isds_fulltext_result_meta` on error.
				    Usual return codes are:

				    * IE_SUCCESS - Search succeeded.
				    * IE_2BIG - If `page_size` is too large.
			"""
			return _isds._isds_find_box_fulltext(self, query, target, box_type, page_size, page_number, track_matches)

		def get_sent_messages(self, from_time, to_time, dmSenderOrgUnitNum, message_status_filter, offset, max_num):
			"""
				Get list of sent messages.

				:param from_time: Minimum time of message sending inclusive. Pass None if you don't care.
				:type from_time: :class:`isds_time`
				:param to_time: Maximum time of message sending inclusive. Pass None if you don't care.
				:type to_time: :class:`isds_time`
				:param dmSenderOrgUnitNum: Sender organisation unit as defined in :class:`isds_envelope`. Pass None if you don't care.
				:type dmSenderOrgUnitNum: int
				:param message_status_filter: Specifies status of the messages you want to download.
				    Use bit operations to select between desired values from :ref:`isds_message_status`.
				:type message_status_filter: int
				:param offset: Index of the first message in the sequence. First message has the index 1.
				    Use 0 or 1 if you want to download all messages.
				:type offset: int
				:param max_num: Specifies the maximum length of the list you want to get. Pass None if you don't care.
				:type max_num: int
				:return: Tuple containing (:ref:`isds_error`, list of :class:`isds_message`). Returns None instead of list on error.
			"""
			return _isds._isds_get_list_of_sent_messages(self, from_time, to_time, dmSenderOrgUnitNum, message_status_filter, offset, max_num)

		def get_received_messages(self, from_time, to_time, dmRecipientOrgUnitNum, message_status_filter, offset, max_num):
			"""
				Get list of received messages.

				:param from_time: Minimum time of message sending inclusive. Pass None if you don't care.
				:type from_time: :class:`isds_time`
				:param to_time: Maximum time of message sending inclusive. Pass None if you don't care.
				:type to_time: :class:`isds_time`
				:param dmSenderOrgUnitNum: Recipient organisation unit as defined in :class:`isds_envelope`. Pass None if you don't care.
				:type dmSenderOrgUnitNum: int
				:param message_status_filter: Specifies status of the messages you want to download.
				    Use bit operations to select between desired values from :ref:`isds_message_status`:
				:type message_status_filter: int
				:param offset: Index of the first message in the sequence. First message has the index 1.
				    Use 0 or 1 if you want to download all messages.
				:type offset: int
				:param max_num: Specifies the maximum length of the list you want to get. Pass None if you don't care.
				:type max_num: int
				:return: Tuple containing (:ref:`isds_error`, list of :class:`isds_message`). Returns None instead of list on error.
			"""
			return _isds._isds_get_list_of_received_messages(self, from_time, to_time, dmRecipientOrgUnitNum, message_status_filter, offset, max_num)

		def send_message(self, message):
			"""
				Send message.

				:param message: Message to be sent. Some members (e.g. dbIDRecipient) are mandatory,
				    some are optional an some are ignored (e.g. data about the sender.)
				    The document list must contain al least one document of the type FILEMETATYPE_MAIN.
				:type message: :class:`isds_message`
				:throws TypeError: When `message` of non-:class:`isds_message` type.
				:return: Tuple containing (:ref:`isds_error`, dmID). Returns None instead of dmID on error.
			"""
			return _isds._isds_send_message(self, message)

		def get_signed_delivery_info(self, dmID):
			"""
				Download signed delivery info.

				:note: The obtained :class:`isds_message` will miss :attr:`isds_message.documents`.
				    Use :meth:`isds_ctx.get_signed_sent_message` or :meth:`isds_ctx.get_signed_received_message`
				    if you are interested in the documents (i.e. message attachments).
				    On the other hand only this method can get the event
				    list which the message has gone through.

				:param dmID: Message identifier (see :attr:`isds_envelope.dmID`).
				    You can use :meth:`isds_ctx.get_sent_messages` or :meth:`isds_ctx.get_received_messages`
				    to obtain a list of message identifiers.
				:type dmID: string
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message`). Returns None instead of :class:`isds_message` on error.
			"""
			return _isds._isds_get_signed_delivery_info(self, dmID)

		def load_delivery_info(self, raw_type, raw):
			"""
				Load delivery info of given format.

				:param raw_type: Advertises format of `raw` content. Only delivery info types are accepted.
				:type raw_type: :ref:`isds_raw_type`
				:param raw: Raw delivery info data.
				:type raw: bytes, bytearray
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message`). Returns None instead of :class:`isds_message` on error.
			"""
			return _isds._isds_load_delivery_info(self, raw_type, raw)

		def load_message(self, raw_type, raw):
			"""
				Load message of given format.

				:param raw_type: Advertises format of `raw` content. Only message types are accepted.
				:type raw_type: :ref:`isds_raw_type`
				:param raw: Raw message data.
				:type raw: bytes, bytearray
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message`). Returns None instead of :class:`isds_message` on error.
			"""
			return _isds._isds_load_message(self, raw_type, raw)

		def guess_raw_type(self, raw):
			"""
				Determine type of raw message or delivery info according some heuristics.

				:note: It does not validate the raw blob.

				:param raw: Raw message or delivery info data.
				:type raw: bytes, bytearray
				:return: Tuple containing (:ref:`isds_error`, :ref:`isds_raw_type`). Returns None instead of :ref:`isds_raw_type` on error.
			"""
			return _isds._isds_guess_raw_type(self, raw)

		def get_signed_sent_message(self, dmID):
			"""
				Download signed sent message.

				:note: The :attr:`isds_message.raw` of the returned :class:`isds_message` will be filled with PKCS#7 data in DER format.

				:param dmID: Message identifier (see :attr:`isds_envelope.dmID`). You can use :meth:`isds_ctx.get_sent_messages` to obtain a list of message identifiers.
				:type dmID: string
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message`). Returns None instead of :class:`isds_message` on error.
			"""
			return _isds._isds_get_signed_sent_message(self, dmID)

		def get_signed_received_message(self, dmID):
			"""
				Download signed received message.

				:note: The :attr:`isds_message.raw` of the returned :class:`isds_message` will be filled with PKCS#7 data in DER format.

				:param dmID: Message identifier (see :attr:`isds_envelope.dmID`). You can use :meth:`isds_ctx.get_received_messages` to obtain a list of message identifiers.
				:type dmID: string
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message`). Returns None instead of :class:`isds_message` on error.
			"""
			return _isds._isds_get_signed_received_message(self, dmID)

		def get_message_sender(self, dmID):
			"""
				Download information about the message author.

				:param dmID: Message identifier (see :attr:`isds_envelope.dmID`).
				:type dmID: string
				:return: Tuple containing (:ref:`isds_error`, :class:`isds_message_author`). Returns None instead of :class:`isds_message_author` on error.
			"""
			return _isds._isds_get_message_sender(self, dmID)
	%}
}
