/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_db_owner_info) isds_DbOwnerInfo;

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_DbOwnerInfo;

/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_DbOwnerInfo::personName "Person name.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_person_name`
"
%attribute_custom_nodisown_(isds_DbOwnerInfo, struct isds_PersonName *, personName,
    get_personName, set_personName,
    _isds_DbOwnerInfo_get_personName(self_), _isds_DbOwnerInfo_set_personName(self_, val_));
%ignore _isds_DbOwnerInfo_get_personName;
%ignore _isds_DbOwnerInfo_set_personName;

%feature ("docstring") isds_DbOwnerInfo::birthInfo "Person birth information.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_birth_info`
"
%attribute_custom_nodisown_(isds_DbOwnerInfo, struct isds_BirthInfo *, birthInfo,
    get_birthInfo, set_birthInfo,
    _isds_DbOwnerInfo_get_birthInfo(self_), _isds_DbOwnerInfo_set_birthInfo(self_, val_));
%ignore _isds_DbOwnerInfo_get_birthInfo;
%ignore _isds_DbOwnerInfo_set_birthInfo;

%feature ("docstring") isds_DbOwnerInfo::address "Post address.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_address`
"
%attribute_custom_nodisown_(isds_DbOwnerInfo, struct isds_Address *, address,
    get_address, set_address,
    _isds_DbOwnerInfo_get_address(self_), _isds_DbOwnerInfo_set_address(self_, val_));
%ignore _isds_DbOwnerInfo_get_address;
%ignore _isds_DbOwnerInfo_set_address;

%delobject _isds_DbOwnerInfo_free;
%ignore isds_DbOwnerInfo_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_DbOwnerInfo_free(struct isds_DbOwnerInfo *oi)
	{
		fprintf(stderr, "******** ISDS_DB_OWNER_INFO free 0x%" PRIXPTR " ********\n", (uintptr_t)oi);
		if (NULL != oi) {
			isds_DbOwnerInfo_free(&oi);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_DbOwnerInfo_free(struct isds_DbOwnerInfo *oi)
	{
		if (NULL != oi) {
			isds_DbOwnerInfo_free(&oi);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_DbOwnerInfo_get_personName;
%inline
%{
	struct isds_PersonName *_isds_DbOwnerInfo_get_personName(const struct isds_DbOwnerInfo *oi)
	{
		if (NULL != oi) {
			struct isds_PersonName *ret = NULL;
			ret = isds_PersonName_duplicate(oi->personName);
			return ret;
		}
		return NULL;
	}

	void _isds_DbOwnerInfo_set_personName(struct isds_DbOwnerInfo *oi, const struct isds_PersonName *pn)
	{
		if (NULL == oi) {
			return;
		}
		if (NULL != oi->personName) {
			isds_PersonName_free(&(oi->personName));
		}
		oi->personName = isds_PersonName_duplicate(pn);
	}
%}

%newobject _isds_DbOwnerInfo_get_birthInfo;
%inline
%{
	struct isds_BirthInfo *_isds_DbOwnerInfo_get_birthInfo(const struct isds_DbOwnerInfo *oi)
	{
		if (NULL != oi) {
			struct isds_BirthInfo *ret = NULL;
			ret = _isds_BirthInfo_copy(oi->birthInfo);
			return ret;
		}
		return NULL;
	}

	void _isds_DbOwnerInfo_set_birthInfo(struct isds_DbOwnerInfo *oi, const struct isds_BirthInfo *bi)
	{
		if (NULL == oi) {
			return;
		}
		if (NULL != oi->birthInfo) {
			isds_BirthInfo_free(&(oi->birthInfo));
		}
		oi->birthInfo = _isds_BirthInfo_copy(bi);
	}
%}

%newobject _isds_DbOwnerInfo_get_address;
%inline
%{
	struct isds_Address *_isds_DbOwnerInfo_get_address(const struct isds_DbOwnerInfo *oi)
	{
		if (NULL != oi) {
			struct isds_Address *ret = NULL;
			ret = isds_Address_duplicate(oi->address);
			return ret;
		}
		return NULL;
	}

	void _isds_DbOwnerInfo_set_address(struct isds_DbOwnerInfo *oi, const struct isds_Address *ad)
	{
		if (NULL == oi) {
			return;
		}
		if (NULL != oi->address) {
			isds_Address_free(&(oi->address));
		}
		oi->address = isds_Address_duplicate(ad);
	}
%}

%ignore isds_DbOwnerInfo_duplicate;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_DbOwnerInfo "ISDS data box owner information.

This class holds the data box owner information content.
"

%feature("docstring") isds_DbOwnerInfo::dbID "Data box identifier.

:note: 7 characters

:type: string
"

%feature("docstring") isds_DbOwnerInfo::dbType "Data box type.

:type: :ref:`isds_DbType`
"

%feature("docstring") isds_DbOwnerInfo::ic "Identifier (in Czech: IČ).

:type: string
"

%feature("docstring") isds_DbOwnerInfo::firmName "Firm name.

:type: string
"

%feature("docstring") isds_DbOwnerInfo::nationality "Nationality.

:type: string
"

%feature("docstring") isds_DbOwnerInfo::email "Email.

:type: string
"

%feature("docstring") isds_DbOwnerInfo::telNumber "Telephone number.

:type: string
"

%feature("docstring") isds_DbOwnerInfo::identifier "External box identifier for data provider (OVM, PO, maybe PFO).

:note: max 20 characters

:type: string
"

#if 0 /* Not present in libisds 0.10.8. */
%feature("docstring") isds_DbOwnerInfo::aifoIsds "Whether the person if has an entry in the centralised Person register (ROB).

:type: bool
"
#endif

%feature("docstring") isds_DbOwnerInfo::registryCode "PFO external registry code.

:note: max 5 characters

:type: string
"

%feature("docstring") isds_DbOwnerInfo::dbState "Box state.

:note: 1 means active state.

:type: int
"

%feature("docstring") isds_DbOwnerInfo::dbEffectiveOVM "Data box has OVM role (§ 5a).

:type: bool
"

%feature("docstring") isds_DbOwnerInfo::dbOpenAddressing "Non-OVM data box is free to receive messages from anybody.

:type: bool
"

%extend isds_DbOwnerInfo {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_DbOwnerInfo_free
	%}
}
