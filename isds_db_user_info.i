/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_db_user_info) isds_DbUserInfo;

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_DbUserInfo;

/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_DbUserInfo::personName "Person name.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_person_name`
"
%attribute_custom_nodisown_(isds_DbUserInfo, struct isds_PersonName *, personName,
    get_personName, set_personName,
    _isds_DbUserInfo_get_personName(self_), _isds_DbUserInfo_set_personName(self_, val_));
%ignore _isds_DbUserInfo_get_personName;
%ignore _isds_DbUserInfo_set_personName;

%feature ("docstring") isds_DbUserInfo::address "Post address.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_address`
"
%attribute_custom_nodisown_(isds_DbUserInfo, struct isds_Address *, address,
    get_address, set_address,
    _isds_DbUserInfo_get_address(self_), _isds_DbUserInfo_set_address(self_, val_));
%ignore _isds_DbUserInfo_get_address;
%ignore _isds_DbUserInfo_set_address;

%feature ("docstring") isds_DbUserInfo::biDate "User birth info.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_date`
"
%attribute_custom_nodisown_(isds_DbUserInfo, struct tm *, biDate,
    get_biDate, set_biDate,
    _isds_DbUserInfo_get_biDate(self_), _isds_DbUserInfo_set_biDate(self_, val_));
%ignore _isds_DbUserInfo_get_biDate;
%ignore _isds_DbUserInfo_set_biDate;

%delobject _isds_DbUserInfo_free;
%ignore isds_DbUserInfo_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_DbUserInfo_free(struct isds_DbUserInfo *ui)
	{
		fprintf(stderr, "******** ISDS_DB_USER_INFO free 0x%" PRIXPTR " ********\n", (uintptr_t)ui);
		if (NULL != ui) {
			isds_DbUserInfo_free(&ui);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_DbUserInfo_free(struct isds_DbUserInfo *ui)
	{
		if (NULL != ui) {
			isds_DbUserInfo_free(&ui);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_DbUserInfo_get_personName;
%inline
%{
	struct isds_PersonName *_isds_DbUserInfo_get_personName(const struct isds_DbUserInfo *ui)
	{
		if (NULL != ui) {
			struct isds_PersonName *ret = NULL;
			ret = isds_PersonName_duplicate(ui->personName);
			return ret;
		}
		return NULL;
	}

	void _isds_DbUserInfo_set_personName(struct isds_DbUserInfo *ui, const struct isds_PersonName *pn)
	{
		if (NULL == ui) {
			return;
		}
		if (NULL != ui->personName) {
			isds_PersonName_free(&(ui->personName));
		}
		ui->personName = isds_PersonName_duplicate(pn);
	}
%}

%newobject _isds_DbUserInfo_get_address;
%inline
%{
	struct isds_Address *_isds_DbUserInfo_get_address(const struct isds_DbUserInfo *ui)
	{
		if (NULL != ui) {
			struct isds_Address *ret = NULL;
			ret = isds_Address_duplicate(ui->address);
			return ret;
		}
		return NULL;
	}

	void _isds_DbUserInfo_set_address(struct isds_DbUserInfo *ui, const struct isds_Address *ad)
	{
		if (NULL == ui) {
			return;
		}
		if (NULL != ui->address) {
			isds_Address_free(&(ui->address));
		}
		ui->address = isds_Address_duplicate(ad);
	}
%}

%newobject _isds_DbUserInfo_get_biDate;
%inline
%{
	struct tm *_isds_DbUserInfo_get_biDate(const struct isds_DbUserInfo *ui)
	{
		if (NULL != ui) {
			struct tm *ret = NULL;
			ret = _tm_copy(ui->biDate);
			return ret;
		}
		return NULL;
	}

	void _isds_DbUserInfo_set_biDate(struct isds_DbUserInfo *ui, const struct tm *tm)
	{
		if (NULL == ui) {
			return;
		}
		if (NULL != ui->biDate) {
			free(ui->biDate);
		}
		ui->biDate = _tm_copy(tm);
	}
%}

%ignore isds_DbUserInfo_duplicate;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_DbUserInfo "ISDS data box user information.

This class holds the data box user information content.
"

%feature("docstring") isds_DbUserInfo::userID "User identifier.

:note: min 6, max 12 characters

:type: string
"

%feature("docstring") isds_DbUserInfo::userType "User type.

:type: :ref:`isds_UserType`
"

%feature("docstring") isds_DbUserInfo::userPrivils "Set of user permissions.

:type: int
"

%feature("docstring") isds_DbUserInfo::ic "Identifier of a supervising firm (in Czech: IČ).

:note: max 8 characters

:type: string
"

%feature("docstring") isds_DbUserInfo::firmName "Name of supervising firm.

:note: max 100 characters

:type: string
"

%feature("docstring") isds_DbUserInfo::caStreet "Contact address street and number.

:type: string
"

%feature("docstring") isds_DbUserInfo::caCity "Contact address city.

:type: string
"

%feature("docstring") isds_DbUserInfo::caZipCode "Contact address postal code.

:type: string
"

%feature("docstring") isds_DbUserInfo::caState "Abbreviated contact address country (e.g. 'CZ').

:note: Optional. Implicit value is 'CZ'.

:type: string
"

#if 0 /* Not present in libisds 0.10.8. */
%feature("docstring") isds_DbUserInfo::aifo_ticket "AIFO ticket.

:note: Optional.

:type: string
"
#endif

%extend isds_DbUserInfo {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_DbUserInfo_free
	%}
}
