/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_document;

/* Don't wrap these structure elements. */
%ignore isds_document::xml_node_list;
%ignore isds_document::dmFormat;

/* Manually added properties into Python proxy class. */
%ignore isds_document::data_length;

%feature ("docstring") isds_document::data "Document data.

:note: A copy of the value is returned when read. A copy of the value is stored when written.
:note: You may also use the bytes type when storing the value.

:type: bytearray
"
%attribute_custom(isds_document, PyObject *, data,
    get_data, set_data,
    _isds_document_get_data_as_bytearray(self_), _isds_document_set_data_as_bytearray(self_, val_));
%ignore _isds_document_get_data_as_bytearray;
%ignore _isds_document_set_data_as_bytearray;

%delobject _isds_document_free;
%ignore isds_document_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_document_free(struct isds_document *doc)
	{
		fprintf(stderr, "******** ISDS_DOCUMENT free 0x%" PRIXPTR " ********\n", (uintptr_t)doc);
		if (NULL != doc) {
			isds_document_free(&doc);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_document_free(struct isds_document *doc)
	{
		if (NULL != doc) {
			isds_document_free(&doc);
		}
	}
%}
#endif /* ISDS_DEBUG */

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/*!
	 * @brief Returns the document data organised into a list of bytes.
	 */
	PyObject *_isds_document_get_data_as_bytearray(const struct isds_document *doc)
	{
		assert(NULL != doc);

		Py_ssize_t len = doc->data_length;
		void *data = doc->data;

		return PyByteArray_FromStringAndSize((char *)data, len);
	}

	/*!
	 * @brief Set document data from a byte list.
	 *
	 * @param[in,out] doc Document to have its data set.
	 * @param[in] py_obj Python object holding the data, its type may be bytes, bytearray or None.
	 */
	void _isds_document_set_data_as_bytearray(struct isds_document *doc, PyObject *py_obj)
	{
		if ((NULL == doc) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		_data_copy(&doc->data, &doc->data_length, py_obj);
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_document "ISDS document.

This class holds the document content. A document is effectively an attachment file.

When creating a message if is fine to set the dmMimeType to "" (empty).
ISDS will determine the MIME type automatically.

If creating a message to be sent then the first document should have
the FILEMETATYPE_MAIN meta-type set.
Other files should have the FILEMETATYPE_ENCLOSURE meta-type.
Other types are not currently supported in the wrapper code.
"

%feature("docstring") isds_document::is_xml "True if document is an ISDS XML (embedded) document.
False if document is an ISDS binary document.

:note: The Python wrapper does not know how to handle embedded documents. The value should therefore be False.

:note: You are free to treat XML documents as ordinary (binary) files.

:type: bool
"

%feature("docstring") isds_document::dmMimeType "Document MIME type.

:note: Mandatory. If you specify an empty sting then the ISDS server will try to determine the MIME type automatically.

:type: string
"

%feature("docstring") isds_document::dmFileMetaType "Document type which is needed to create attachment hierarchy.

:note: At least one document must be of the type FILEMETATYPE_MAIN when sending a message.

:type: :ref:`isds_FileMetaType`
"

%feature("docstring") isds_document::dmFileGuid "Message-local document identifier.

:note: Optional.

:type: string
"

%feature("docstring") isds_document::dmUpFileGuid "Reference to upper document identifier (:attr:`dmFileGuid`).

:note: Optional.

:type: string
"

%feature("docstring") isds_document::dmFileDescr "Document name (title) - I.e. file name.

:note: Mandatory.

:type: string
"

%extend isds_document {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_document_free
	%}
}
