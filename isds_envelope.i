/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_envelope;

/* Manually added properties into Python proxy class. */
%ignore isds_envelope::timestamp_length;

%feature ("docstring") isds_envelope::dmDeliveryTime "Message delivery time.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_time`
"
%attribute_custom_nodisown_(isds_envelope, struct timeval *, dmDeliveryTime,
    get_dmDeliveryTime, set_dmDeliveryTime,
    _isds_envelope_get_dmDeliveryTime(self_), _isds_envelope_set_dmDeliveryTime(self_, val_));
%ignore _isds_envelope_get_dmDeliveryTime;
%ignore _isds_envelope_set_dmDeliveryTime;

%feature ("docstring") isds_envelope::dmAcceptanceTime "Message acceptance time.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_time`
"
%attribute_custom_nodisown_(isds_envelope, struct timeval *, dmAcceptanceTime,
    get_dmAcceptanceTime, set_dmAcceptanceTime,
    _isds_envelope_get_dmAcceptanceTime(self_), _isds_envelope_set_dmAcceptanceTime(self_, val_));
%ignore _isds_envelope_get_dmAcceptanceTime;
%ignore _isds_envelope_set_dmAcceptanceTime;

%feature ("docstring") isds_envelope::hash "Message hash.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_hash`
"
%attribute_custom_nodisown_(isds_envelope, struct isds_hash *, hash,
    get_hash, set_hash,
    _isds_envelope_get_hash(self_), _isds_envelope_set_hash(self_, val_));
%ignore _isds_envelope_get_hash;
%ignore _isds_envelope_set_hash;

%feature ("docstring") isds_envelope::timestamp "Envelope time stamp.

:note: A copy of the value is returned when read. A copy of the value is stored when written.
:note: You may also use the bytes type when storing the value.

:type: bytearray
"
%attribute_custom(isds_envelope, PyObject *, timestamp,
    get_timestamp, set_timestamp,
    _isds_envelope_get_timestamp_as_bytearray(self_), _isds_envelope_set_timestamp_as_bytearray(self_, val_));
%ignore _isds_envelope_get_timestamp_as_bytearray;
%ignore _isds_envelope_set_timestamp_as_bytearray;

%feature ("docstring") isds_envelope::events "List of events.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: list of :class:`isds_event`
"
%attribute_custom(isds_envelope, PyObject *, events,
    get_events, set_events,
    _isds_envelope_get_events(self_), _isds_envelope_set_events(self_, val_));
%ignore _isds_envelope_get_events;
%ignore _isds_envelope_set_events;

%delobject _isds_envelope_free;
%ignore isds_envelope_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_envelope_free(struct isds_envelope *env)
	{
		fprintf(stderr, "******** ISDS_ENVELOPE free 0x%" PRIXPTR " ********\n", (uintptr_t)env);
		if (NULL != env) {
			isds_envelope_free(&env);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_envelope_free(struct isds_envelope *env)
	{
		if (NULL != env) {
			isds_envelope_free(&env);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_envelope_get_dmDeliveryTime;
%inline
%{
	struct timeval *_isds_envelope_get_dmDeliveryTime(const struct isds_envelope *env)
	{
		if (NULL != env) {
			struct timeval *ret = NULL;
			ret = _timeval_copy(env->dmDeliveryTime);
			return ret;
		}
		return NULL;
	}

	void _isds_envelope_set_dmDeliveryTime(struct isds_envelope *env, const struct timeval *tv)
	{
		if (NULL == env) {
			return;
		}
		if (NULL != env->dmDeliveryTime) {
			free(env->dmDeliveryTime);
		}
		env->dmDeliveryTime = _timeval_copy(tv);
	}
%}

%newobject _isds_envelope_get_dmAcceptanceTime;
%inline
%{
	struct timeval *_isds_envelope_get_dmAcceptanceTime(const struct isds_envelope *env)
	{
		if (NULL != env) {
			struct timeval *ret = NULL;
			ret = _timeval_copy(env->dmAcceptanceTime);
			return ret;
		}
		return NULL;
	}

	void _isds_envelope_set_dmAcceptanceTime(struct isds_envelope *env, const struct timeval *tv)
	{
		if (NULL == env) {
			return;
		}
		if (NULL != env->dmAcceptanceTime) {
			free(env->dmAcceptanceTime);
		}
		env->dmAcceptanceTime = _timeval_copy(tv);
	}
%}

%newobject _isds_envelope_get_hash;
%inline
%{
	struct isds_hash *_isds_envelope_get_hash(const struct isds_envelope *env)
	{
		if (NULL != env) {
			struct isds_hash *ret = NULL;
			ret = _isds_hash_copy(env->hash);
			return ret;
		}
		return NULL;
	}

	void _isds_envelope_set_hash(struct isds_envelope *env, const struct isds_hash *hash)
	{
		if (NULL == env) {
			return;
		}
		if (NULL != env->hash) {
			isds_hash_free(&(env->hash));
		}
		env->hash = _isds_hash_copy(hash);
	}
%}

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/*!
	 * @brief Returns the time stamp data organised into a list of bytes.
	 */
	PyObject *_isds_envelope_get_timestamp_as_bytearray(const struct isds_envelope *env)
	{
		assert(NULL != env);

		Py_ssize_t len = env->timestamp_length;
		void *data = env->timestamp;

		return PyByteArray_FromStringAndSize((char *)data, len);
	}

	/*!
	 * @brief Set time stamp data from a byte list.
	 *
	 * @param[in,out] env Envelope to have its time-stamp data set.
	 * @param[in] py_obj Python object holding the data, its type may be bytes, bytearray or None.
	 */
	void _isds_envelope_set_timestamp_as_bytearray(struct isds_envelope *env, PyObject *py_obj)
	{
		if ((NULL == env) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		_data_copy(&env->timestamp, &env->timestamp_length, py_obj);
	}
%}

DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_extract_event_list2python_list,
    struct isds_event, _isds_event_copy, SWIGTYPE_p_isds_event)

%inline
%{
	PyObject *_isds_envelope_get_events(struct isds_envelope *env)
	{
		if (NULL == env) {
			assert(0);
			return SWIG_Py_Void();
		}

		return _extract_event_list2python_list(env->events, 1);
	}

	void _isds_envelope_set_events(struct isds_envelope *env, PyObject *py_obj)
	{
		if ((NULL == env) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		if (!PyList_Check(py_obj)) {
			assert(0);
			return;
		}

		struct isds_list *list = NULL;
		struct isds_list *cur = NULL;
		struct isds_list *last = NULL;

		/* Create copy of event list. */
		Py_ssize_t size = PyList_Size(py_obj);
		for (Py_ssize_t i = 0; i < size; ++i) {
			PyObject *py_item = PyList_GetItem(py_obj, i);
			void *ptr = NULL;
			int res = SWIG_ConvertPtr(py_item, &ptr, SWIGTYPE_p_isds_event, 0 |  0 );
			if ((!SWIG_IsOK(res)) || (NULL == ptr)) {
				/* Null is translated by SWIG onto a NULL pointer. */
				SWIG_exception_fail(SWIG_ArgError(res), "in method '" "_isds_envelope_set_events" "', list item " """ not of type '" "struct isds_event *""'");
			}

			const struct isds_event *ev = (const struct isds_event *)ptr;
			assert(NULL != ev);

			cur = calloc(1, sizeof(*cur));
			if (NULL == cur) {
				goto fail;
			}
			if (NULL == list) {
				list = cur;
			}
			if (NULL != last) {
				last->next = cur;
			}

			cur->data = _isds_event_copy(ev);
			if (NULL == cur->data) {
				goto fail;
			}
			cur->destructor = (void (*)(void **))isds_event_free;

			last = cur;
		}

		/* Free any events if some ale already present. */
		isds_list_free(&env->events);

		/* Store new copy. */
		env->events = list;

		return;

	fail:
		if (NULL != list) {
			isds_list_free(&list);
		}
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_envelope "ISDS envelope.

This class holds the envelope content.
"

%feature("docstring") isds_envelope::dmID "Message identifier.

:note: Message identifiers are integers but are stored as strings.
:type: string
"

%feature("docstring") isds_envelope::dbIDSender "Sender data box identifier (7 characters).

:note: Special value 'aaaaaaa' means sent by ISDS.
:type: string
"

%feature("docstring") isds_envelope::dmSender "Sender name (max 100 characters).

:type: string
"

%feature("docstring") isds_envelope::dmSenderAddress "Sender postal address (max 100 characters).

:type: string
"

%feature("docstring") isds_envelope::dmSenderType "Approximate sender data box type.

:type: int
"

%feature("docstring") isds_envelope::dmRecipient "Recipient name (max 100 characters).

:type: string
"

%feature("docstring") isds_envelope::dmRecipientAddress "Recipient postal address (max 100 chracters).

:type: string
"

%feature("docstring") isds_envelope::dmAmbiguousRecipient "Recipient has OVM role.

:type: bool
"

%feature("docstring") isds_envelope::dmOrdinal "Ordinal number in list of incoming/outgoing messages.

:type: int
"

%feature("docstring") isds_envelope::dmMessageStatus "Message state.

:type: :ref:`isds_message_status`
"

%feature("docstring") isds_envelope::dmAttachmentSize "Rounded dize of message documents in kilobytes.

:type: int
"

%feature("docstring") isds_envelope::dmSenderOrgUnit "Sender organisation unit.

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmSenderOrgUnitNum "Sender organisation unit number.

:note: Optional.

:type: int
"

%feature("docstring") isds_envelope::dbIDRecipient "Recipient data box identifier (7 characters).

:note: Madatory.

:type: string
"

%feature("docstring") isds_envelope::dmRecipientOrgUnit "Recipient organisation unit.

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmRecipientOrgUnitNum "Recipient organisation unit number.

:note: Optional.

:type: int
"

%feature("docstring") isds_envelope::dmToHands "Person in recipient organisation.

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmAnnotation "Subject (title) of the message (max 255 characters).

:note: Mandatory.

:type: string
"

%feature("docstring") isds_envelope::dmRecipientRefNumber "Czech: jednací číslo příjemce (max 50 characters).

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmSenderRefNumber "Czech: jednací číslo odesílatele (max 50 characters).

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmRecipientIdent "Czech: spisová značka příjemce (max 50 characters).

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmSenderIdent "Czech: spisová značka odesílatele (max 50 characters).

:note: Optional.

:type: string
"

%feature("docstring") isds_envelope::dmLegalTitleLaw "Number of act mandating authority.

:type: int
"

%feature("docstring") isds_envelope::dmLegalTitleYear "Year of act issue mandating authority.

:type: int
"

%feature("docstring") isds_envelope::dmLegalTitleSect "Section of act mandating authority. Czech: paragraf.

:type: string
"

%feature("docstring") isds_envelope::dmLegalTitlePar "Paragraph of act mandating authority. Czech: odstavec.

:type: string
"

%feature("docstring") isds_envelope::dmLegalTitlePoint "Point of act mandating authority. Czech: písmeno.

:type: string
"

%feature("docstring") isds_envelope::dmPersonalDelivery "If True then only person with higher privileges can read this message.

:type: bool
"

%feature("docstring") isds_envelope::dmAllowSubstDelivery "Allow delivery through fiction.

:note: Even if recipient did not read this message the message is considered to
    be delivered after (currently) 10 days. This is delivery through fiction.
    This applies only to OVM dbType sender.

:type: bool
"

%feature("docstring") isds_envelope::dmType "Message type (commercial subtypes or government message):

:note: The length is exactly 1 UTF-8 character.

:type: string

Use these input values (when sending a message):
    * 'I' - commercial message offering the payment of the response (initiatory message); then it's necessary to define :attr:`dmSenderRefNumber`.
    * 'K' - commercial message paid by sender if this message
    * 'O' - commercial response paid by sender of initiatory message; it's necessary to copy value from :attr:`dmSenderRefNumber` of initiatory message to :attr:`dmRecipientRefNumber` of the response message
    * 'V' - non-commercial government message

Default value while sending may is undefined which has the same meaning as 'V'.

Output values (when retrieving a message):
    * 'A' - subsidized initiatory commercial message which can pay a response
    * 'B' - subsidized initiatory commercial message which has already paid the response
    * 'C' - subsidized initiatory commercial message where the response offer has expired
    * 'D' - externally subsidized commercial message
    * 'E' - commercial message prepaid by a stamp
    * 'G' - commercial message paid by a sponsor
    * 'I'
    * 'K'
    * 'O'
    * 'V'
    * 'X' - initiatory commercial message where the response offer has expired
    * 'Y' - initiatory commercial message which has already paid the response
    * 'Z' - limitedly subsidized commercial message
"

%feature("docstring") isds_envelope::dmOVM "OVM sending mode.

:note: Optional. Non-OVM dbType boxes that has `dbEffectiveOVM` == True MUST select between True (OVM mode) and False (non-OVM mode).
:note: Implicit value is True.

:type: bool
"

%feature("docstring") isds_envelope::dmPublishOwnID "Allow sender to express his name.
It shall be available to recipient using :meth:`isds_ctx.isds_get_message_sender()`.
Sender type will be always available. Default value is False.

:note: Optional.

:type: bool
"

%extend isds_envelope {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_envelope_free
	%}
}
