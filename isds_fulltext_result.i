/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_fulltext_result;

/* TODO - Provide a convenient interface for the start/stop data. */
%ignore isds_fulltext_result::name_match_start;
%ignore isds_fulltext_result::name_match_end;
%ignore isds_fulltext_result::address_match_start;
%ignore isds_fulltext_result::address_match_end;
/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_fulltext_result::nameMatches "Indexes of start/stop pairs of highlighted `name` text which match the sought element.

:note: A copy of the value is returned when read. Nothing is done when written.

:type: list of tuples (int, int) each representing a start/stop index pair
"
%attribute_custom(isds_fulltext_result, PyObject *, nameMatches,
    get_nameMatches, set_nameMatches,
    _isds_fulltext_result_get_nameMatches(self_), _isds_fulltext_result_set_nameMatches(self_, val_));
%ignore _isds_fulltext_result_get_nameMatches;
%ignore _isds_fulltext_result_set_nameMatches;

%feature ("docstring") isds_fulltext_result::addressMatches "Indexes of start/stop pairs of highlighted `address` text which match the sought element.

:note: A copy of the value is returned when read. Nothing is done when written.

:type: list of tuples (int, int) each representing a start/stop index pair
"
%attribute_custom(isds_fulltext_result, PyObject *, addressMatches,
    get_addressMatches, set_addressMatches,
    _isds_fulltext_result_get_addressMatches(self_), _isds_fulltext_result_set_addressMatches(self_, val_));
%ignore _isds_fulltext_result_get_addressMatches;
%ignore _isds_fulltext_result_set_addressMatches;

%feature ("docstring") isds_fulltext_result::biDate "Date of birth in local time at birth place.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_date`
"
%attribute_custom_nodisown_(isds_fulltext_result, struct tm *, biDate,
    get_biDate, set_biDate,
    _isds_fulltext_result_get_biDate(self_), _isds_fulltext_result_set_biDate(self_, val_));
%ignore _isds_fulltext_result_get_biDate;
%ignore _isds_fulltext_result_set_biDate;

%delobject _isds_fulltext_result_free;
%ignore isds_fulltext_result_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_fulltext_result_free(struct isds_fulltext_result *fr)
	{
		fprintf(stderr, "******** ISDS_FULLTEXT_RESULT free 0x%" PRIXPTR " ********\n", (uintptr_t)fr);
		if (NULL != fr) {
			isds_fulltext_result_free(&fr);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_fulltext_result_free(struct isds_fulltext_result *fr)
	{
		if (NULL != fr) {
			isds_fulltext_result_free(&fr);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_fulltext_result_get_biDate;
%inline
%{
	struct tm *_isds_fulltext_result_get_biDate(const struct isds_fulltext_result *fr)
	{
		if (NULL != fr) {
			struct tm *ret = NULL;
			ret = _tm_copy(fr->biDate);
			return ret;
		}
		return NULL;
	}

	void _isds_fulltext_result_set_biDate(struct isds_fulltext_result *fr, const struct tm *tm)
	{
		if (NULL == fr) {
			return;
		}
		if (NULL != fr->biDate) {
			free(fr->biDate);
		}
		fr->biDate = _tm_copy(tm);
	}
%}

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%ignore _py_string;
%ignore _py_string_len;
%ignore _characters_between_ptrs;
%ignore _libisds_start_stop2start_stop;
%inline
%{
	static
	PyObject *_py_string(const char *str, ssize_t len)
	{
#if PY_VERSION_HEX >= 0x03000000
		return PyUnicode_FromStringAndSize(str, len);
#else
		return PyString_FromStringAndSize(str, len);
#endif
	}

	static
	long _py_string_len(PyObject *string)
	{
#if PY_VERSION_HEX >= 0x03030000
		return PyUnicode_GetLength(string);
#elif PY_VERSION_HEX >= 0x03000000
		return PyUnicode_GetSize(string);
#else
		return PyString_Size(string);
#endif
	}

	static
	long _characters_between_ptrs(const char *start, const char *stop)
	{
		if (start > stop) {
			assert(0);
			return -1;
		}

		ssize_t len = stop - start;
		if (len == 0) {
			return 0;
		}

		PyObject *py_str = _py_string(start, len);
		len = _py_string_len(py_str);
		Py_DECREF(py_str);
		return len;
	}

	static
	PyObject *_libisds_start_stop2start_stop(const char *str,
	    const struct isds_list *starts, const struct isds_list *stops)
	{
		PyObject *list = PyList_New(0);

		if ((NULL == str) || ((NULL == starts) && (NULL == stops))) {
			return list;
		}

		if ((NULL == starts) || (NULL == stops)) {
			assert(0);
			goto fail;
		}

		while ((starts != NULL) && (stops != NULL)) {
			const char *start = (char *)starts->data;
			const char *stop = (char *)stops->data;

			/* Destructor functions must not be set. */
			if ((NULL != starts->destructor) || (NULL != stops->destructor)) {
				assert(0);
				goto fail;
			}

			long diffStart;
			long diffStop;
			/*
			 * UTF8 characters skew the positions, therefore simple pointer subtraction
			 * diffStart = start - str;
			 * diffStop = stop - str;
			 * canot be performed.
			 */
			diffStart = _characters_between_ptrs(str, start);
			diffStop = _characters_between_ptrs(str, stop);

			if ((diffStart < 0) || (diffStop < 0) || (diffStart > diffStop)) {
				assert(0);
				goto fail;
			}

			PyObject *tuple = PyTuple_New(2);
			PyTuple_SetItem(tuple, 0, PyLong_FromLong(diffStart));
			PyTuple_SetItem(tuple, 1, PyLong_FromLong(diffStop));

			PyList_Append(list, tuple);
			Py_DECREF(tuple);

			starts = starts->next;
			stops = stops->next;
		}

		/* Both lists must be equally long. */
		if ((NULL != starts) || (NULL != stops)) {
			assert(0);
			goto fail;
		}

		return list;

	fail:
		Py_DECREF(list);
		return PyList_New(0);
	}
%}

%inline
%{
	PyObject *_isds_fulltext_result_get_nameMatches(const struct isds_fulltext_result *fr)
	{
		if (NULL == fr) {
			assert(0);
			return SWIG_Py_Void();
		}

		return _libisds_start_stop2start_stop(fr->name,
		    fr->name_match_start, fr->name_match_end);
	}

	void _isds_fulltext_result_set_nameMatches(struct isds_fulltext_result *fr, PyObject *py_obj)
	{
		(void)fr;
		(void)py_obj;
		/* TODO*/
	}
%}

%inline
%{
	PyObject *_isds_fulltext_result_get_addressMatches(const struct isds_fulltext_result *fr)
	{
		if (NULL == fr) {
			assert(0);
			return SWIG_Py_Void();
		}

		return _libisds_start_stop2start_stop(fr->address,
		    fr->address_match_start, fr->address_match_end);
	}

	void _isds_fulltext_result_set_addressMatches(struct isds_fulltext_result *fr, PyObject *py_obj)
	{
		(void)fr;
		(void)py_obj;
		/* TODO*/
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_fulltext_result "ISDS full-text result.

This class holds the full-text search result content.
"

%feature("docstring") isds_fulltext_result::dbID "Data box identifier.

:type: string
"

%feature("docstring") isds_fulltext_result::dbType "Data box type.

:type: :ref:`isds_DbType`
"

%feature("docstring") isds_fulltext_result::name "Subject owning the box.

:type: string
"

%feature("docstring") isds_fulltext_result::address "Post address.

:type: string
"

%feature("docstring") isds_fulltext_result::ic "Organisation identifier (in Czech: IČ).

:type: string
"

%feature("docstring") isds_fulltext_result::dbEffectiveOVM "Data box has OVM role (§ 5a).

:type: bool
"

%feature("docstring") isds_fulltext_result::active "Data box is active.

:type: bool
"

%feature("docstring") isds_fulltext_result::public_sending "Current data box (which has been used to obtain the search result) can send non-commercial messages into this box.

:type: bool
"

%feature("docstring") isds_fulltext_result::commercial_sending "Current data box (which has been used to obtain the search result) can send commercial messages into this box.

:type: bool
"

%extend isds_fulltext_result {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_fulltext_result_free
	%}
}
