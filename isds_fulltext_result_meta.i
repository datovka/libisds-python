/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_fulltext_result_meta;

/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_fulltext_result_meta::boxes "List of found data boxes.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: list of :class:`isds_fulltext_result`
"
%attribute_custom(isds_fulltext_result_meta, PyObject *, boxes,
    get_boxes, set_boxes,
    _isds_fulltext_result_meta_get_boxes(self_), _isds_fulltext_result_meta_set_boxes(self_, val_));
%ignore _isds_fulltext_result_meta_get_boxes;
%ignore _isds_fulltext_result_meta_set_boxes;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_fulltext_result_meta "ISDS full-text result and information about the result.

This class holds the full-text search result data and information about the result.
"

%feature("docstring") isds_fulltext_result_meta::total_matching_boxes "Number of all boxes matching the query.
May be None if server did not provide the value.

:type: int
"

%feature("docstring") isds_fulltext_result_meta::current_page_beginning "Number of the first box in the :attr:`isds_fulltext_result_meta.boxes` list.
May be None if server did not provide the value.

:note: In count from 0.

:type: int
"

%feature("docstring") isds_fulltext_result_meta::current_page_size "Count of boxes in the :attr:`isds_fulltext_result_meta.boxes` list.
May be None if server did not provide the value.

:type: int
"

%feature("docstring") isds_fulltext_result_meta::last_page "True if :attr:`isds_fulltext_result_meta.boxes` is the last result page, false if more results match.
May be None if server did not provide the value.

:type: int
"

%extend isds_fulltext_result_meta {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_fulltext_result_meta_free
	%}
}

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/*!
	 * @brief Convenience class. Aggregates full-text search result.
	 */
	struct isds_fulltext_result_meta {
		unsigned long int *total_matching_boxes; /*!<
		                                          * Number of all boxes matching the query.
		                                          * Pointer to NULL if server did not provide the value.
		                                          */
		unsigned long int *current_page_beginning; /*!<
		                                            * Number of the first box in this @boxes page.
		                                            * It counts from zero.
		                                            * Pointer to NULL if the server did not provide the value.
		                                            */
		unsigned long int *current_page_size; /*!<
		                                       * Count of boxes in the this @boxes page.
		                                       * Pointer to NULL if the server did not provide the value.
		                                       */
		_Bool *last_page; /*!<
		                   * True if this @boxes page is the last one,
		                   * false if more boxes match,
		                   * NULL if the server did not provide the value.
		                   */
		struct isds_list *boxes; /*!< List of isds_fulltext_result structures, possibly empty. */
	};
%}

%delobject _isds_fulltext_result_meta_free;
%inline
%{
	void _isds_fulltext_result_meta_free(struct isds_fulltext_result_meta *frm)
	{
		if (NULL == frm) {
			return;
		}

		free(frm->total_matching_boxes);
		free(frm->current_page_beginning);
		free(frm->current_page_size);

		free(frm->last_page);

		isds_list_free(&frm->boxes);

		free(frm);
	}
%}

DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_extract_fulltext_result_list2python_list,
    struct isds_fulltext_result, _isds_fulltext_result_copy, SWIGTYPE_p_isds_fulltext_result)

%inline
%{
	PyObject *_isds_fulltext_result_meta_get_boxes(struct isds_fulltext_result_meta *frm)
	{
		if (NULL == frm) {
			assert(0);
			return SWIG_Py_Void();
		}

		return _extract_fulltext_result_list2python_list(frm->boxes, 1);
	}

	void _isds_fulltext_result_meta_set_boxes(struct isds_fulltext_result_meta *frm, PyObject *py_obj)
	{
		if ((NULL == frm) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		if (!PyList_Check(py_obj)) {
			assert(0);
			return;
		}

		struct isds_list *list = NULL;
		struct isds_list *cur = NULL;
		struct isds_list *last = NULL;

		/* Create copy of full-text result list. */
		Py_ssize_t size = PyList_Size(py_obj);
		for (Py_ssize_t i = 0; i < size; ++i) {
			PyObject *py_item = PyList_GetItem(py_obj, i);
			void *ptr = NULL;
			int res = SWIG_ConvertPtr(py_item, &ptr, SWIGTYPE_p_isds_fulltext_result, 0 |  0 );
			if ((!SWIG_IsOK(res)) || (NULL == ptr)) {
				/* Null is translated by SWIG onto a NULL pointer. */
				SWIG_exception_fail(SWIG_ArgError(res), "in method '" "_isds_fulltext_result_meta_set_boxes" "', list item " """ not of type '" "struct isds_fulltext_result *""'");
			}

			const struct isds_fulltext_result *fr = (const struct isds_fulltext_result *)ptr;
			assert(NULL != fr);

			cur = calloc(1, sizeof(*cur));
			if (NULL == cur) {
				goto fail;
			}
			if (NULL == list) {
				list = cur;
			}
			if (NULL != last) {
				last->next = cur;
			}

			cur->data = _isds_fulltext_result_copy(fr);
			if (NULL == cur->data) {
				goto fail;
			}
			cur->destructor = (void (*)(void **))isds_fulltext_result_free;

			last = cur;
		}

		/* Free any full-text results if some ale already present. */
		isds_list_free(&frm->boxes);

		/* Store new copy. */
		frm->boxes = list;

		return;

	fail:
		if (NULL != list) {
			isds_list_free(&list);
		}
	}
%}
