/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_hash;

/* Manually added properties into Python proxy class. */
%ignore isds_hash::length;

%feature ("docstring") isds_hash::value "Hash value.

:note: A copy of the value is returned when read. A copy of the value is stored when written.
:note: You may also use the bytes type when storing the value.

:type: bytearray
"
%attribute_custom(isds_hash, PyObject *, value,
    get_value, set_value,
    _isds_hash_get_value_as_bytearray(self_), _isds_hash_set_value_as_bytearray(self_, val_));
%ignore _isds_hash_get_value_as_bytearray;
%ignore _isds_hash_set_value_as_bytearray;

%delobject _isds_hash_free;
%ignore isds_hash_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_hash_free(struct isds_hash *hash)
	{
		fprintf(stderr, "******** ISDS_HASH free 0x%" PRIXPTR " ********\n", (uintptr_t)hash);
		if (NULL != hash) {
			isds_hash_free(&hash);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_hash_free(struct isds_hash *hash)
	{
		if (NULL != hash) {
			isds_hash_free(&hash);
		}
	}
%}
#endif /* ISDS_DEBUG */

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/* Returns boolean values. */
	int _isds_hash_eq(const struct isds_hash *h1, const struct isds_hash *h2)
	{
		if ((NULL == h1) && (NULL == h2)) {
			return true;
		} else if ((NULL != h1) && (NULL != h2)) {
			return (h1->algorithm == h2->algorithm) &&
			    (h1->length == h2->length) &&
			    (memcmp(h1->value, h2->value, h1->length) == 0);
		} else {
			return false;
		}
	}

	/*!
	 * @brief Returns the hash value organised into a list of bytes.
	 */
	PyObject *_isds_hash_get_value_as_bytearray(const struct isds_hash *hash)
	{
		assert(NULL != hash);

		Py_ssize_t len = hash->length;
		void *data = hash->value;

		return PyByteArray_FromStringAndSize((char *)data, len);
	}

	/*!
	 * @brief Set hash value from a byte list.
	 *
	 * @param[in,out] hash Hash to have its value set.
	 * @param[in] py_obj Python object holding the value, its type may be bytes, bytearray or None.
	 */
	void _isds_hash_set_value_as_bytearray(struct isds_hash *hash, PyObject *py_obj)
	{
		if ((NULL == hash) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		_data_copy(&hash->value, &hash->length, py_obj);
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_hash "ISDS hash.

This class holds the hash content.
"

%feature("docstring") isds_hash::algorithm "Hash algorithm.

:type: :ref:`isds_hash_algorithm`
"

%extend isds_hash {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_hash_free

		def __eq__(self, other):
			"""
				Compares two :class:`isds_hash` instances according to their values.

				:param other: The second hash.
				:type other: :class:`isds_hash`
				:return: (bool) True if `self` is equal to `other`.
			"""
			return _isds._isds_hash_eq(self, other)

		def __ne__(self, other):
			"""
				Compares two :class:`isds_hash` instances according to their values.

				:param other: The second hash.
				:type other: :class:`isds_hash`
				:return: (bool) True if `self` is not equal to `other`.
			"""
			return not _isds._isds_hash_eq(self, other)
	%}
}
