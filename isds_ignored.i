/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* The following symbols are excluded from the wrapper code. */

%ignore isds_locator;
%ignore isds_cert_locator;
%ignore isds_otp_locator;
%ignore isds_testing_locator;
%ignore isds_cert_testing_locator;
%ignore isds_otp_testing_locator;

%ignore isds_option;
%ignore isds_tls_option;
%ignore isds_pki_credentials;
%ignore isds_otp_method;
%ignore isds_otp;
%ignore isds_DbState;
%ignore isds_priviledges;
%ignore isds_buffer_strategy;
%ignore isds_message_copy;
%ignore isds_message_status_change;
%ignore isds_payment_type;
%ignore isds_commercial_permission;
%ignore isds_credit_event_type;
%ignore isds_credit_event_charged;
%ignore isds_credit_event_discharged;
%ignore isds_credit_event_message_sent;
%ignore isds_credit_event_storage_set;
/* -- Nested unions are problematic in SWIG.
%ignore isds_credit_event;
%ignore isds_credit_event::details;
%ignore isds_credit_event::charged;
%ignore isds_credit_event::discharged;
%ignore isds_credit_event::message_sent;
%ignore isds_credit_event::storage_set;
*/
%ignore isds_list;
%ignore isds_approval;
%ignore isds_credentials_delivery;
%ignore isds_box_state_period;

%ignore isds_init; /* The function is called automatically when the wrapper is loaded. */
%ignore isds_cleanup; /* Unused. */

%ignore isds_log_callback;
%ignore isds_set_log_callback;

%ignore isds_progress_callback;
%ignore isds_set_progress_callback;
%ignore isds_set_opt;

%ignore isds_GetOwnerInfoFromLogin;
%ignore isds_GetUserInfoFromLogin;
%ignore isds_get_password_expiration;
%ignore isds_change_password;
%ignore isds_add_box;
%ignore isds_add_pfoinfo;
%ignore isds_delete_box;
%ignore isds_delete_box_promptly;
%ignore isds_UpdateDataBoxDescr;
%ignore isds_GetDataBoxUsers;
%ignore isds_UpdateDataBoxUser;
%ignore isds_activate;
%ignore isds_reset_password;
%ignore isds_add_user;
%ignore isds_delete_user;
%ignore isds_get_box_list_archive;
%ignore isds_CheckDataBox;
%ignore isds_get_box_state_history;
%ignore isds_get_commercial_permissions;
%ignore isds_get_commercial_credit;
%ignore isds_switch_commercial_receiving;
%ignore isds_switch_effective_ovm;
%ignore isds_switch_box_accessibility_on_owner_request;
%ignore isds_disable_box_accessibility_externaly;
%ignore isds_send_message_to_multiple_recipients;
%ignore isds_get_list_of_sent_message_state_changes;
%ignore isds_get_received_envelope;
%ignore isds_get_delivery_info;
%ignore isds_get_received_message;
%ignore isds_download_message_hash;
%ignore isds_compute_message_hash;
%ignore isds_hash_cmp;
%ignore isds_verify_message_hash;
%ignore isds_authenticate_message;
%ignore isds_resign_message;
%ignore isds_delete_message_from_storage;
%ignore isds_mark_message_read;
%ignore isds_mark_message_received;
%ignore isds_bogus_request;
%ignore czp_convert_document;
%ignore czp_close_connection;
%ignore isds_request_new_testing_box;
%ignore isds_find_document_by_id;
%ignore isds_normalize_mime_type;

%ignore isds_pki_credentials_free;
%ignore isds_list_free;
%ignore isds_message_copy_free;
%ignore isds_message_status_change_free;
%ignore isds_approval_free;
%ignore isds_commercial_permission_free;
%ignore isds_credit_event_free;
%ignore isds_credentials_delivery_free;
%ignore isds_box_state_period_free;

%ignore isds_box_state_period_duplicate;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

/* None. */
