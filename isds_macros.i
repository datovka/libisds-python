/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * The generated interface for the classes needs to create a copy whenever
 * a value is stored or when a value is returned.
 */
%define %attribute_custom_nodisown_(Class, AttributeType, AttributeName, GetMethod, SetMethod, GetMethodCall, SetMethodCall)
  %ignore Class::GetMethod();
  %ignore Class::GetMethod() const;
  #if #SetMethod != #AttributeName
    %ignore Class::SetMethod;
  #endif

  %newobject Class::AttributeName;
  %typemap(in, noblock=1) AttributeType AttributeName (void *argp = 0, int res = 0) {
    /* Ignoring DISOWN. */
    /* res = SWIG_ConvertPtr($input, &argp, $descriptor, $disown |  0 ); */
    res = SWIG_ConvertPtr($input, &argp, $descriptor, 0 |  0 );
    if (!SWIG_IsOK(res)) {
      SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$type""'");
    }
    $1 = ($ltype)(argp);
  }

  %extend Class {
    AttributeType AttributeName;
  }

  %{
    #define %mangle(Class) ##_## AttributeName ## _get(self_) GetMethodCall
    #define %mangle(Class) ##_## AttributeName ## _set(self_, val_) SetMethodCall
  %}
%enddef
