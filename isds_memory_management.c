/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <isds.h>
#include <stdlib.h>
#include <string.h>

#include "isds_memory_management.h"

static
char *_str_copy(const char *src)
{
	char *tgt = NULL;
	size_t len = 0;

	if (NULL == src) {
		return tgt;
	}

	len = strlen(src) + 1;
	tgt = malloc(len);
	if (NULL == tgt) {
		return tgt;
	}

	memcpy(tgt, src, len);
	return tgt;
}

#define goto_fail_on_type_copy_fail(dst, src, type) \
	do { \
		if (NULL != (src)) { \
			(dst) = malloc(sizeof(*(dst))); \
			if (NULL == (dst)) { \
				goto fail; \
			} \
			*(dst) = *(src); \
		} else { \
			(dst) = NULL; \
		} \
	} while (0)

#define goto_fail_on_data_copy_fail(dst, dst_len, src, src_len) \
	do { \
		if ((NULL != (src)) && (0 != (src_len))) { \
			(dst) = malloc(src_len); \
			if (NULL == (dst)) { \
				goto fail; \
			} \
			memcpy((dst), (src), (src_len)); \
			(dst_len) = (src_len); \
		} \
	} while (0)

#define goto_fail_on_copy_fail(dst, src, copy_func) \
	do { \
		if (NULL != (src)) { \
			(dst) = (copy_func)(src); \
			if (NULL == (dst)) { \
				goto fail; \
			} \
		} \
	} while (0)

struct isds_document *_isds_document_copy(const struct isds_document *src)
{
	struct isds_document *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	tgt->is_xml = src->is_xml;

	/* FIXME -- The function ignores the xml entry.
	 * tgt->xml_node_list = NULL;
	 */

	goto_fail_on_data_copy_fail(tgt->data, tgt->data_length, src->data, src->data_length);

	goto_fail_on_copy_fail(tgt->dmMimeType, src->dmMimeType, _str_copy);

	tgt->dmFileMetaType = src->dmFileMetaType;

	goto_fail_on_copy_fail(tgt->dmFileGuid, src->dmFileGuid, _str_copy);

	goto_fail_on_copy_fail(tgt->dmUpFileGuid, src->dmUpFileGuid, _str_copy);

	goto_fail_on_copy_fail(tgt->dmFileDescr, src->dmFileDescr, _str_copy);

	goto_fail_on_copy_fail(tgt->dmFormat, src->dmFormat, _str_copy);

	return tgt;

fail:
	isds_document_free(&tgt);
	return NULL;
}

static
struct isds_list *_isds_document_list_copy(const struct isds_list *src)
{
	struct isds_list *tgt = NULL; /* Fisr list item. */
	struct isds_list *cur = NULL;
	struct isds_list *last = NULL;

	while (NULL != src) {
		if (src->destructor != (void (*)(void **))isds_document_free) {
			goto fail;
		}
		cur = calloc(1, sizeof(*cur));
		if (NULL == cur) {
			goto fail;
		}
		if (NULL == tgt) {
			tgt = cur;
		}
		if (NULL != last) {
			last->next = cur;
		}

		goto_fail_on_copy_fail(cur->data, src->data, _isds_document_copy);
		cur->destructor = (void (*)(void **))isds_document_free;

		last = cur;
		src = src->next;
	}

	return tgt;

fail:
	isds_list_free(&tgt);
	return NULL;
}

static
struct isds_list *_isds_event_list_copy(const struct isds_list *src)
{
	struct isds_list *tgt = NULL; /* Fisr list item. */
	struct isds_list *cur = NULL;
	struct isds_list *last = NULL;

	while (NULL != src) {
		if (src->destructor != (void (*)(void **))isds_event_free) {
			goto fail;
		}
		cur = calloc(1, sizeof(*cur));
		if (NULL == cur) {
			goto fail;
		}
		if (NULL == tgt) {
			tgt = cur;
		}
		if (NULL != last) {
			last->next = cur;
		}

		goto_fail_on_copy_fail(cur->data, src->data, _isds_event_copy);
		cur->destructor = (void (*)(void **))isds_event_free;

		last = cur;
		src = src->next;
	}

	return tgt;

fail:
	isds_list_free(&tgt);
	return NULL;
}

struct isds_envelope *_isds_envelope_copy(const struct isds_envelope *src)
{
	struct isds_envelope *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	goto_fail_on_copy_fail(tgt->dmID, src->dmID, _str_copy);

	goto_fail_on_copy_fail(tgt->dbIDSender, src->dbIDSender, _str_copy);

	goto_fail_on_copy_fail(tgt->dmSender, src->dmSender, _str_copy);

	goto_fail_on_copy_fail(tgt->dmSenderAddress, src->dmSenderAddress, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmSenderType, src->dmSenderType, long int);

	goto_fail_on_copy_fail(tgt->dmRecipient, src->dmRecipient, _str_copy);

	goto_fail_on_copy_fail(tgt->dmRecipientAddress, src->dmRecipientAddress, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmAmbiguousRecipient, src->dmAmbiguousRecipient, _Bool);

	goto_fail_on_type_copy_fail(tgt->dmOrdinal, src->dmOrdinal, unsigned long int);

	goto_fail_on_type_copy_fail(tgt->dmMessageStatus, src->dmMessageStatus, isds_message_status);

	goto_fail_on_type_copy_fail(tgt->dmAttachmentSize, src->dmAttachmentSize, long int);

	goto_fail_on_type_copy_fail(tgt->dmDeliveryTime, src->dmDeliveryTime, struct timeval);

	goto_fail_on_type_copy_fail(tgt->dmAcceptanceTime, src->dmAcceptanceTime, struct timeval);

	goto_fail_on_copy_fail(tgt->hash, src->hash, _isds_hash_copy);

	goto_fail_on_data_copy_fail(tgt->timestamp, tgt->timestamp_length, src->timestamp, src->timestamp_length);

	goto_fail_on_copy_fail(tgt->events, src->events, _isds_event_list_copy);

	goto_fail_on_copy_fail(tgt->dmSenderOrgUnit, src->dmSenderOrgUnit, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmSenderOrgUnitNum, src->dmSenderOrgUnitNum, long int);

	goto_fail_on_copy_fail(tgt->dbIDRecipient, src->dbIDRecipient, _str_copy);

	goto_fail_on_copy_fail(tgt->dmRecipientOrgUnit, src->dmRecipientOrgUnit, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmRecipientOrgUnitNum, src->dmRecipientOrgUnitNum, long int);

	goto_fail_on_copy_fail(tgt->dmToHands, src->dmToHands, _str_copy);

	goto_fail_on_copy_fail(tgt->dmAnnotation, src->dmAnnotation, _str_copy);

	goto_fail_on_copy_fail(tgt->dmRecipientRefNumber, src->dmRecipientRefNumber, _str_copy);

	goto_fail_on_copy_fail(tgt->dmSenderRefNumber, src->dmSenderRefNumber, _str_copy);

	goto_fail_on_copy_fail(tgt->dmRecipientIdent, src->dmRecipientIdent, _str_copy);

	goto_fail_on_copy_fail(tgt->dmSenderIdent, src->dmSenderIdent, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmLegalTitleLaw, src->dmLegalTitleLaw, long int);

	goto_fail_on_type_copy_fail(tgt->dmLegalTitleYear, src->dmLegalTitleYear, long int);

	goto_fail_on_copy_fail(tgt->dmLegalTitleSect, src->dmLegalTitleSect, _str_copy);

	goto_fail_on_copy_fail(tgt->dmLegalTitlePar, src->dmLegalTitlePar, _str_copy);

	goto_fail_on_copy_fail(tgt->dmLegalTitlePoint, src->dmLegalTitlePoint, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmPersonalDelivery, src->dmPersonalDelivery, _Bool);

	goto_fail_on_type_copy_fail(tgt->dmAllowSubstDelivery, src->dmAllowSubstDelivery, _Bool);

	goto_fail_on_copy_fail(tgt->dmType, src->dmType, _str_copy);

	goto_fail_on_type_copy_fail(tgt->dmOVM, src->dmOVM, _Bool);

	goto_fail_on_type_copy_fail(tgt->dmPublishOwnID, src->dmPublishOwnID, _Bool);

	return tgt;

fail:
	isds_envelope_free(&tgt);
	return tgt;
}

struct isds_event *_isds_event_copy(const struct isds_event *src)
{
	struct isds_event *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	goto_fail_on_type_copy_fail(tgt->time, src->time, struct timeval);

	goto_fail_on_type_copy_fail(tgt->type, src->type, isds_event_type);

	goto_fail_on_copy_fail(tgt->description, src->description, _str_copy);

	return tgt;

fail:
	isds_event_free(&tgt);
	return NULL;
}

struct isds_hash *_isds_hash_copy(const struct isds_hash *src)
{
	struct isds_hash *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	tgt->algorithm = src->algorithm;

	goto_fail_on_data_copy_fail(tgt->value, tgt->length, src->value, src->length);

	return tgt;

fail:
	isds_hash_free(&tgt);
	return NULL;
}

struct isds_message *_isds_message_copy(const struct isds_message *src)
{
	struct isds_message *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	goto_fail_on_data_copy_fail(tgt->raw, tgt->raw_length, src->raw, src->raw_length);

	tgt->raw_type = src->raw_type;

	/*
	 * FIXME -- The function ignores the xml entry.
	 * tgt->xml = NULL;
	 */

	goto_fail_on_copy_fail(tgt->envelope, src->envelope, _isds_envelope_copy);

	goto_fail_on_copy_fail(tgt->documents, src->documents, _isds_document_list_copy);

	return tgt;

fail:
	isds_message_free(&tgt);
	return NULL;
}

struct isds_BirthInfo *_isds_BirthInfo_copy(const struct isds_BirthInfo *src)
{
	struct isds_BirthInfo *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	goto_fail_on_copy_fail(tgt->biDate, src->biDate, _tm_copy);

	goto_fail_on_copy_fail(tgt->biCity, src->biCity, _str_copy);

	goto_fail_on_copy_fail(tgt->biCounty, src->biCounty, _str_copy);

	goto_fail_on_copy_fail(tgt->biState, src->biState, _str_copy);

	return tgt;

fail:
	isds_BirthInfo_free(&tgt);
	return NULL;
}

static
struct isds_list *_isds_relative_pointer_list_copy(const char *tgt_ref,
    const struct isds_list *src, const char *src_ref)
{
	if ((NULL == tgt_ref) || (NULL == src_ref)) {
		return NULL;
	}

	struct isds_list *tgt = NULL; /* Fisr list item. */
	struct isds_list *cur = NULL;
	struct isds_list *last = NULL;

	while (NULL != src) {
		if (src->destructor != NULL) {
			goto fail;
		}
		cur = calloc(1, sizeof(*cur));
		if (NULL == cur) {
			goto fail;
		}
		if (NULL == tgt) {
			tgt = cur;
		}
		if (NULL != last) {
			last->next = cur;
		}

		cur->data = (void *)(((const char *)src->data - src_ref) + tgt_ref);
		/* cur->destructor = NULL; */

		last = cur;
		src = src->next;
	}

	return tgt;

fail:
	isds_list_free(&tgt);
	return NULL;
}

#define goto_fail_on_relative_ptr_list_copy_fail(dst_list, dst_ref, src_list, src_ref) \
	do { \
		if (NULL != (src_list)) { \
			(dst_list) = _isds_relative_pointer_list_copy((dst_ref), (src_list), (src_ref)); \
			if (NULL == (dst_list)) { \
				goto fail; \
			} \
		} else { \
			(dst_list) = NULL; \
		} \
	} while (0)

struct isds_fulltext_result *_isds_fulltext_result_copy(
    const struct isds_fulltext_result *src)
{
	struct isds_fulltext_result *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = calloc(1, sizeof(*tgt));
	if (NULL == tgt) {
		goto fail;
	}

	goto_fail_on_copy_fail(tgt->dbID, src->dbID, _str_copy);

	tgt->dbType = src->dbType;

	goto_fail_on_copy_fail(tgt->name, src->name, _str_copy);

	goto_fail_on_relative_ptr_list_copy_fail(tgt->name_match_start, tgt->name, src->name_match_start, src->name);
	goto_fail_on_relative_ptr_list_copy_fail(tgt->name_match_end, tgt->name, src->name_match_end, src->name);

	goto_fail_on_copy_fail(tgt->address, src->address, _str_copy);

	goto_fail_on_relative_ptr_list_copy_fail(tgt->address_match_start, tgt->address, src->address_match_start, src->address);
	goto_fail_on_relative_ptr_list_copy_fail(tgt->address_match_end, tgt->address, src->address_match_end, src->address);

	goto_fail_on_copy_fail(tgt->ic, src->ic, _str_copy);

	goto_fail_on_copy_fail(tgt->biDate, src->biDate, _tm_copy);

	tgt->dbEffectiveOVM = src->dbEffectiveOVM;

	tgt->active = src->active;

	tgt->public_sending = src->public_sending;

	tgt->commercial_sending = src->commercial_sending;

	return tgt;

fail:
	isds_fulltext_result_free(&tgt);
	return NULL;
}

struct timeval *_timeval_copy(const struct timeval *src)
{
	struct timeval *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = malloc(sizeof(*tgt));
	if (NULL == tgt) {
		return tgt;
	}

	*tgt = *src;
	return tgt;
}

struct tm *_tm_copy(const struct tm *src)
{
	struct tm *tgt = NULL;

	if (NULL == src) {
		return tgt;
	}

	tgt = malloc(sizeof(*tgt));
	if (NULL == tgt) {
		return tgt;
	}

	*tgt = *src;
	return tgt;
}
