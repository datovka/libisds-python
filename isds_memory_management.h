/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

/* Libisds lacks functionality for copying of its objects. */

struct isds_document *_isds_document_copy(const struct isds_document *src);
struct isds_envelope *_isds_envelope_copy(const struct isds_envelope *src);
struct isds_event *_isds_event_copy(const struct isds_event *src);
struct isds_hash *_isds_hash_copy(const struct isds_hash *src);
struct isds_message *_isds_message_copy(const struct isds_message *src);

struct isds_BirthInfo *_isds_BirthInfo_copy(const struct isds_BirthInfo *src);
struct isds_fulltext_result *_isds_fulltext_result_copy(const struct isds_fulltext_result *src);

struct timeval *_timeval_copy(const struct timeval *src);
struct tm *_tm_copy(const struct tm *src);
