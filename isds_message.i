/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_message;

/* Don't wrap these structure elements. */
%ignore isds_message::raw_length;
%ignore isds_message::xml;

/* Manually added properties into Python proxy class. */
%feature ("docstring") isds_message::raw "Raw data.

:note: A copy of the value is returned when read. A copy of the value is stored when written.
:note: You may also use the bytes type when storing the value.
:note: The wrapper code does not handle this entry properly. But it is generally
    safe to read the raw data when a :class:`isds_message` instance has been
    obtained as a result of a library function call (e.g. when downloading a message).

:type: bytearray
"
%attribute_custom(isds_message, PyObject *, raw,
    get_raw, set_raw,
    _isds_message_get_raw_as_bytearray(self_), _isds_message_set_raw_as_bytearray(self_, val_));
%ignore _isds_message_get_raw_as_bytearray;
%ignore _isds_message_set_raw_as_bytearray;

%feature ("docstring") isds_message::envelope "Message envelope.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: :class:`isds_envelope`
"
%attribute_custom_nodisown_(isds_message, struct isds_envelope *, envelope,
    get_envelope, set_envelope,
    _isds_message_get_envelope(self_), _isds_message_set_envelope(self_, val_));
%ignore _isds_message_get_envelope;
%ignore _isds_message_set_envelope;

%feature ("docstring") isds_message::documents "List of documents.

:note: A copy of the value is returned when read. A copy of the value is stored when written.

:type: list of :class:`isds_document`
"
%attribute_custom(isds_message, PyObject *, documents,
    get_documents, set_documents,
    _isds_message_get_documents(self_), _isds_message_set_documents(self_, val_));
%ignore _isds_message_get_documents;
%ignore _isds_message_set_documents;

%delobject _isds_message_free;
%ignore isds_message_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_message_free(struct isds_message *msg)
	{
		fprintf(stderr, "******** ISDS_MESSAGE free 0x%" PRIXPTR " ********\n", (uintptr_t)msg);
		if (NULL != msg) {
			isds_message_free(&msg);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_message_free(struct isds_message *msg)
	{
		if (NULL != msg) {
			isds_message_free(&msg);
		}
	}
%}
#endif /* ISDS_DEBUG */

%newobject _isds_message_get_envelope;
%inline
%{
	struct isds_envelope *_isds_message_get_envelope(const struct isds_message *msg)
	{
		if (NULL != msg) {
			struct isds_envelope *ret = NULL;
			ret = _isds_envelope_copy(msg->envelope);
			return ret;
		}
		return NULL;
	}

	void _isds_message_set_envelope(struct isds_message *msg, const struct isds_envelope *env)
	{
		if (NULL == msg) {
			return;
		}
		if (NULL != msg->envelope) {
			isds_envelope_free(&(msg->envelope));
		}
		msg->envelope = _isds_envelope_copy(env);
	}
%}

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/*!
	 * @brief Returns the raw value organised into a list of bytes.
	 */
	PyObject *_isds_message_get_raw_as_bytearray(const struct isds_message *msg)
	{
		assert(NULL != msg);

		Py_ssize_t len = msg->raw_length;
		void *data = msg->raw;

		return PyByteArray_FromStringAndSize((char *)data, len);
	}

	/*!
	 * @brief Set raw value from a byte list.
	 *
	 * @param[in,out] msg Message to have its raw set.
	 * @param[in] py_obj Python object holding the raw data, its type may be bytes, bytearray or None.
	 */
	void _isds_message_set_raw_as_bytearray(struct isds_message *msg, PyObject *py_obj)
	{
		if ((NULL == msg) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		_data_copy(&msg->raw, &msg->raw_length, py_obj);
	}
%}

DEFINE_LIBISDS2PYTHON_LIST_EXTRACTOR(_extract_document_list2python_list,
    struct isds_document, _isds_document_copy, SWIGTYPE_p_isds_document)

%inline
%{
	PyObject *_isds_message_get_documents(struct isds_message *msg)
	{
		if (NULL == msg) {
			assert(0);
			return SWIG_Py_Void();
		}

		return _extract_document_list2python_list(msg->documents, 1);
	}

	void _isds_message_set_documents(struct isds_message *msg, PyObject *py_obj)
	{
		if ((NULL == msg) || (NULL == py_obj)) {
			assert(0);
			return;
		}

		if (!PyList_Check(py_obj)) {
			assert(0);
			return;
		}

		struct isds_list *list = NULL;
		struct isds_list *cur = NULL;
		struct isds_list *last = NULL;

		/* Create copy of document list. */
		Py_ssize_t size = PyList_Size(py_obj);
		for (Py_ssize_t i = 0; i < size; ++i) {
			PyObject *py_item = PyList_GetItem(py_obj, i);
			void *ptr = NULL;
			int res = SWIG_ConvertPtr(py_item, &ptr, SWIGTYPE_p_isds_document, 0 |  0 );
			if ((!SWIG_IsOK(res)) || (NULL == ptr)) {
				/* Null is translated by SWIG onto a NULL pointer. */
				SWIG_exception_fail(SWIG_ArgError(res), "in method '" "_isds_message_set_documents" "', list item " """ not of type '" "struct isds_document *""'");
			}

			const struct isds_document *doc = (const struct isds_document *)ptr;
			assert(NULL != doc);

			cur = calloc(1, sizeof(*cur));
			if (NULL == cur) {
				goto fail;
			}
			if (NULL == list) {
				list = cur;
			}
			if (NULL != last) {
				last->next = cur;
			}

			cur->data = _isds_document_copy(doc);
			if (NULL == cur->data) {
				goto fail;
			}
			cur->destructor = (void (*)(void **))isds_document_free;

			last = cur;
		}

		/* Free any documents if some ale already present. */
		isds_list_free(&msg->documents);

		/* Store new copy. */
		msg->documents = list;

		return;

	fail:
		if (NULL != list) {
			isds_list_free(&list);
		}
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_message "ISDS message.

This class holds the message content.
"

%feature("docstring") isds_message::raw_type "Content type of :attr:`isds_message.raw` data.

:note: Meaningful only with non-empty raw member.

:type: :ref:`isds_raw_type`
"

%extend isds_message {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_message_free
	%}
}
