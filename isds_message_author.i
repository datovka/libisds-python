/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_message_author;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_message_author "ISDS message author.

This class holds the message author data.
This is a convenience class and it does not exist in the underlying library.
"

%feature("docstring") isds_message_author::userType "User type as identified by the library.

:note: May be None if not recognised by the library.

:type: :ref:`isds_sender_type`
"

%feature("docstring") isds_message_author::rawUserType "User type as identified by ISDS.

:note: May be None if not recognised by ISDS.

:type: string
"

%feature("docstring") isds_message_author::authorName "Author name.

:type: string
"

%extend isds_message_author {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_message_author_free
	%}
}

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%inline
%{
	/*!
	 * @brief Convenience class.
	 */
	struct isds_message_author {
		isds_sender_type *userType; /*!< User type. */
		char *rawUserType; /*!< String holding the user type. */
		char *authorName; /*!< Author name. */
	};
%}

%delobject _isds_message_author_free;
%inline
%{
	void _isds_message_author_free(struct isds_message_author *ma)
	{
		if (NULL == ma) {
			return;
		}

		free(ma->userType);
		free(ma->rawUserType);
		free(ma->authorName);

		free(ma);
	}
%}
