/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

%rename(isds_person_name) isds_PersonName;

/* Let SWIG generate the default constructor. */
%nodefaultdtor isds_PersonName;

%delobject _isds_PersonName_free;
%ignore isds_PersonName_free;
#ifdef ISDS_DEBUG
%inline
%{
	void _isds_PersonName_free(struct isds_PersonName *pn)
	{
		fprintf(stderr, "******** ISDS_PERSON_NAME free 0x%" PRIXPTR " ********\n", (uintptr_t)pn);
		if (NULL != pn) {
			isds_PersonName_free(&pn);
		}
	}
%}
#else /* !ISDS_DEBUG */
%inline
%{
	void _isds_PersonName_free(struct isds_PersonName *pn)
	{
		if (NULL != pn) {
			isds_PersonName_free(&pn);
		}
	}
%}
#endif /* ISDS_DEBUG */

%ignore isds_PersonName_duplicate;

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

%feature("docstring") isds_PersonName "ISDS person name.

This class holds the person name content.
"

%feature("docstring") isds_PersonName::pnFirstName "First name.

:type: string
"

%feature("docstring") isds_PersonName::pnMiddleName "Middle name.

:type: string
"

%feature("docstring") isds_PersonName::pnLastName "Last name.

:type: string
"

%feature("docstring") isds_PersonName::pnLastNameAtBirth "Last name at birth.

:type: string
"

%extend isds_PersonName {
	%pythoncode
	%{
		__swig_destroy__ = _isds._isds_PersonName_free
	%}
}
