/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%typemap(in,numinputs=1,noblock=1) _Bool
{
	int $1_ecode = 0;
	$1_ecode = SWIG_AsVal_bool($input, &$1);
	if (!SWIG_IsOK($1_ecode)) {
		SWIG_exception_fail(SWIG_ArgError($1_ecode),
		    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
	}
}

%typemap(in,numinputs=1,noblock=1) const _Bool
{
	int $1_ecode = 0;
	$1_ecode = SWIG_AsVal_bool($input, &$1);
	if (!SWIG_IsOK($1_ecode)) {
		SWIG_exception_fail(SWIG_ArgError($1_ecode),
		    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
	}
}

%typemap(in,numinputs=1,noblock=1) _Bool *
{
	_Bool $1_bool;
	int $1_ecode = 0;
	if (Py_None != $input) { /* Py_None is a singleton (https://stackoverflow.com/a/29732914). */
		$1_ecode = SWIG_AsVal_bool($input, &$1_bool);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_bool;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) const _Bool *
{
	_Bool $1_bool;
	int $1_ecode = 0;
	if (Py_None != $input) {
		$1_ecode = SWIG_AsVal_bool($input, &$1_bool);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_bool;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) long int *
{
	long int $1_long;
	int $1_ecode = 0;
	if (Py_None != $input) {
		$1_ecode = SWIG_AsVal_long($input, &$1_long);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_long;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) const long int *
{
	long int $1_long;
	int $1_ecode = 0;
	if (Py_None != $input) {
		$1_ecode = SWIG_AsVal_long($input, &$1_long);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_long;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) unsigned long int *
{
	unsigned long int $1_long;
	int $1_ecode = 0;
	if (Py_None != $input) {
		$1_ecode = SWIG_AsVal_unsigned_SS_long($input, &$1_long);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_long;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) const unsigned long int *
{
	unsigned long int $1_long;
	int $1_ecode = 0;
	if (Py_None != $input) {
		$1_ecode = SWIG_AsVal_unsigned_SS_long($input, &$1_long);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1 = &$1_long;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) _Bool *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) long int *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) unsigned long int *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

/* Using typemap(out) instead of typemap(memberout). */

%typemap(out) _Bool
{
	$result = PyBool_FromLong($1);
}

%typemap(out) const _Bool
{
	$result = PyBool_FromLong($1);
}

%typemap(out) _Bool *
{
	$result = (NULL != $1) ? PyBool_FromLong(*($1)) : SWIG_Py_Void();
}

%typemap(out) const _Bool *
{
	$result = (NULL != $1) ? PyBool_FromLong(*($1)) : SWIG_Py_Void();
}

%typemap(out) long int *
{
	$result = (NULL != $1) ? PyLong_FromLong(*($1)) : SWIG_Py_Void();
}

%typemap(out) const long int *
{
	$result = (NULL != $1) ? PyLong_FromLong(*($1)) : SWIG_Py_Void();
}

%typemap(out) unsigned long int *
{
	$result = (NULL != $1) ? PyLong_FromUnsignedLong(*($1)) : SWIG_Py_Void();
}

%typemap(out) const unsigned long int *
{
	$result = (NULL != $1) ? PyLong_FromUnsignedLong(*($1)) : SWIG_Py_Void();
}

/* Libisds enum values. */

%typemap(in,numinputs=1,noblock=1) isds_message_status *
{
	isds_message_status $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_message_status *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_message_status *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_message_status *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(in,numinputs=1,noblock=1) isds_event_type *
{
	isds_event_type $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_event_type *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_event_type *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_event_type *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(in,numinputs=1,noblock=1) isds_DbType *
{
	isds_DbType $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) const isds_DbType *
{
	isds_DbType $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_DbType *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_DbType *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_DbType *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(in,numinputs=1,noblock=1) isds_UserType *
{
	isds_UserType $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_UserType *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_UserType *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_UserType *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(in,numinputs=1,noblock=1) isds_sender_type *
{
	isds_sender_type $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_sender_type *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_sender_type *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_sender_type *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(in,numinputs=1,noblock=1) isds_fulltext_target *
{
	isds_fulltext_target $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(in,numinputs=1,noblock=1) const isds_fulltext_target *
{
	isds_fulltext_target $1_enum;
	int $1_ecode = 0;
	if (Py_None != $input) {
		int $1_int;
		$1_ecode = SWIG_AsVal_int($input, &$1_int);
		if (!SWIG_IsOK($1_ecode)) {
			SWIG_exception_fail(SWIG_ArgError($1_ecode),
			    "in method '" "$symname" "', argument " "$argnum" " of type '" "$type""'");
		}
		$1_enum = $1_int;
		$1 = &$1_enum;
	} else {
		$1 = NULL;
	}
}

%typemap(memberin) isds_fulltext_target *
{
	free($1);
	if (NULL != $input) {
		$1 = ($1_type)malloc(sizeof(*($1)));
		*($1) = *$input;
	} else {
		$1 = NULL;
	}
}

%typemap(out) isds_fulltext_target *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}

%typemap(out) const isds_fulltext_target *
{
	$result = (NULL != $1) ? SWIG_From_int(*($1)) : SWIG_Py_Void();
}
