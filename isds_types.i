/*
 * Copyright (C) 2019 CZ.NIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ========================================================================= */
/* SWIG setting and definitions. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Debugging related code. */
/* ========================================================================= */

/* None. */

/* ========================================================================= */
/* Added C code. */
/* ========================================================================= */

%feature("docstring") isds_error2str "Return text code of the library error.

:param error: Error code.
:type error: :ref:`isds_error`
:return: (string) Text error code.
"
%inline
%{
	/*!
	 * @brief Returns a human readable string describing an error state.
	 */
	const char *isds_error2str(isds_error error)
	{
		switch (error) {
		case IE_SUCCESS: return "IE_SUCCESS"; break;
		case IE_ERROR: return "IE_ERROR"; break;
		case IE_NOTSUP: return "IE_NOTSUP"; break;
		case IE_INVAL: return "IE_INVAL"; break;
		case IE_INVALID_CONTEXT: return "IE_INVALID_CONTEXT"; break;
		case IE_NOT_LOGGED_IN: return "IE_NOT_LOGGED_IN"; break;
		case IE_CONNECTION_CLOSED: return "IE_CONNECTION_CLOSED"; break;
		case IE_TIMED_OUT: return "IE_TIMED_OUT"; break;
		case IE_NOEXIST: return "IE_NOEXIST"; break;
		case IE_NOMEM: return "IE_NOMEM"; break;
		case IE_NETWORK: return "IE_NETWORK"; break;
		case IE_HTTP: return "IE_HTTP"; break;
		case IE_SOAP: return "IE_SOAP"; break;
		case IE_XML: return "IE_XML"; break;
		case IE_ISDS: return "IE_ISDS"; break;
		case IE_ENUM: return "IE_ENUM"; break;
		case IE_DATE: return "IE_DATE"; break;
		case IE_2BIG: return "IE_2BIG"; break;
		case IE_2SMALL: return "IE_2SMALL"; break;
		case IE_NOTUNIQ: return "IE_NOTUNIQ"; break;
		case IE_NOTEQUAL: return "IE_NOTEQUAL"; break;
		case IE_PARTIAL_SUCCESS: return "IE_PARTIAL_SUCCESS"; break;
		case IE_ABORTED: return "IE_ABORTED"; break;
		case IE_SECURITY: return "IE_SECURITY"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_otp_resolution2str "Return text code of the library OTP resolution status.

:param resolution: OTP resolution status.
:type: :ref:`isds_otp_resolution`
:return: (string) Text OTP resolution status.
"
%inline
%{
	const char *isds_otp_resolution2str(isds_otp_resolution resolution)
	{
		switch (resolution) {
		case OTP_RESOLUTION_SUCCESS: return "OTP_RESOLUTION_SUCCESS"; break;
		case OTP_RESOLUTION_UNKNOWN: return "OTP_RESOLUTION_UNKNOWN"; break;
		case OTP_RESOLUTION_BAD_AUTHENTICATION: return "OTP_RESOLUTION_BAD_AUTHENTICATION"; break;
		case OTP_RESOLUTION_ACCESS_BLOCKED: return "OTP_RESOLUTION_ACCESS_BLOCKED"; break;
		case OTP_RESOLUTION_PASSWORD_EXPIRED: return "OTP_RESOLUTION_PASSWORD_EXPIRED"; break;
		case OTP_RESOLUTION_TO_FAST: return "OTP_RESOLUTION_TO_FAST"; break;
		case OTP_RESOLUTION_UNAUTHORIZED: return "OTP_RESOLUTION_UNAUTHORIZED"; break;
		case OTP_RESOLUTION_TOTP_SENT: return "OTP_RESOLUTION_TOTP_SENT"; break;
		case OTP_RESOLUTION_TOTP_NOT_SENT: return "OTP_RESOLUTION_TOTP_NOT_SENT"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_DbType2str "Return text code of the library data box type.

:param dbType: Data box type.
:type dbType: :ref:`isds_DbType`
:return: (string) Text data box type.
"
%inline
%{
	/*!
	 * @brief returns a human readable string describing a dbType.
	 */
	const char *isds_DbType2str(isds_DbType dbType)
	{
		switch (dbType) {
		case DBTYPE_SYSTEM: return "DBTYPE_SYSTEM"; break;
		case DBTYPE_OVM: return "DBTYPE_OVM"; break;
		case DBTYPE_OVM_NOTAR: return "DBTYPE_OVM_NOTAR"; break;
		case DBTYPE_OVM_EXEKUT: return "DBTYPE_OVM_EXEKUT"; break;
		case DBTYPE_OVM_REQ: return "DBTYPE_OVM_REQ"; break;
		case DBTYPE_OVM_FO: return "DBTYPE_OVM_FO"; break;
		case DBTYPE_OVM_PFO: return "DBTYPE_OVM_PFO"; break;
		case DBTYPE_OVM_PO: return "DBTYPE_OVM_PO"; break;
		case DBTYPE_PO: return "DBTYPE_PO"; break;
		case DBTYPE_PO_ZAK: return "DBTYPE_PO_ZAK"; break;
		case DBTYPE_PO_REQ: return "DBTYPE_PO_REQ"; break;
		case DBTYPE_PFO: return "DBTYPE_PFO"; break;
		case DBTYPE_PFO_ADVOK: return "DBTYPE_PFO_ADVOK"; break;
		case DBTYPE_PFO_DANPOR: return "DBTYPE_PFO_DANPOR"; break;
		case DBTYPE_PFO_INSSPR: return "DBTYPE_PFO_INSSPR"; break;
		case DBTYPE_PFO_AUDITOR: return "DBTYPE_PFO_AUDITOR"; break;
		case DBTYPE_FO: return "DBTYPE_FO"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_message_status2str "Return text code of the library message status.

:param status: Message status.
:type status: :ref:`isds_message_status`
:return: (string) Text message status.
"
%inline
%{
	const char *isds_message_status2str(isds_message_status status)
	{
		switch (status) {
		case MESSAGESTATE_SENT: return "MESSAGESTATE_SENT"; break;
		case MESSAGESTATE_STAMPED: return "MESSAGESTATE_STAMPED"; break;
		case MESSAGESTATE_INFECTED: return "MESSAGESTATE_INFECTED"; break;
		case MESSAGESTATE_DELIVERED: return "MESSAGESTATE_DELIVERED"; break;
		case MESSAGESTATE_SUBSTITUTED: return "MESSAGESTATE_SUBSTITUTED"; break;
		case MESSAGESTATE_RECEIVED: return "MESSAGESTATE_RECEIVED"; break;
		case MESSAGESTATE_READ: return "MESSAGESTATE_READ"; break;
		case MESSAGESTATE_UNDELIVERABLE: return "MESSAGESTATE_UNDELIVERABLE"; break;
		case MESSAGESTATE_REMOVED: return "MESSAGESTATE_REMOVED"; break;
		case MESSAGESTATE_IN_SAFE: return "MESSAGESTATE_IN_SAFE"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_hash_algorithm2str "Return text code of the library hash algorithm.

:param algorithm: Hash algorithm.
:type algorithm: :ref:`isds_hash_algorithm`
:return: (string) Text hash algorithm.
"
%inline
%{
	const char *isds_hash_algorithm2str(isds_hash_algorithm algorithm)
	{
		switch (algorithm) {
		case HASH_ALGORITHM_MD5: return "HASH_ALGORITHM_MD5"; break;
		case HASH_ALGORITHM_SHA_1: return "HASH_ALGORITHM_SHA_1"; break;
		case HASH_ALGORITHM_SHA_224: return "HASH_ALGORITHM_SHA_224"; break;
		case HASH_ALGORITHM_SHA_256: return "HASH_ALGORITHM_SHA_256"; break;
		case HASH_ALGORITHM_SHA_384: return "HASH_ALGORITHM_SHA_384"; break;
		case HASH_ALGORITHM_SHA_512: return "HASH_ALGORITHM_SHA_512"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_UserType2str "Return text code of the library user type.

:param userType: User type.
:type userType: :ref:`isds_UserType`
:return: (string) Text user type.
"
%inline
%{
	const char *isds_UserType2str(isds_UserType userType)
	{
		switch (userType) {
		case USERTYPE_PRIMARY: return "USERTYPE_PRIMARY"; break;
		case USERTYPE_ENTRUSTED: return "USERTYPE_ENTRUSTED"; break;
		case USERTYPE_ADMINISTRATOR: return "USERTYPE_ADMINISTRATOR"; break;
		case USERTYPE_OFFICIAL: return "USERTYPE_OFFICIAL"; break;
		case USERTYPE_OFFICIAL_CERT: return "USERTYPE_OFFICIAL_CERT"; break;
		case USERTYPE_LIQUIDATOR: return "USERTYPE_LIQUIDATOR"; break;
		case USERTYPE_RECEIVER: return "USERTYPE_RECEIVER"; break;
		case USERTYPE_GUARDIAN: return "USERTYPE_GUARDIAN"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_event_type2str "Return text code of the library event type.

:param event: Event type.
:type event: :ref:`isds_event_type`
:return: (string) Text event type.
"
%inline
%{
	const char *isds_event_type2str(isds_event_type event)
	{
		switch (event) {
		case EVENT_UKNOWN: return "EVENT_UKNOWN"; break;
		case EVENT_ACCEPTED_BY_RECIPIENT: return "EVENT_ACCEPTED_BY_RECIPIENT"; break;
		case EVENT_ACCEPTED_BY_FICTION: return "EVENT_ACCEPTED_BY_FICTION"; break;
		case EVENT_UNDELIVERABLE: return "EVENT_UNDELIVERABLE"; break;
		case EVENT_COMMERCIAL_ACCEPTED: return "EVENT_COMMERCIAL_ACCEPTED"; break;
		case EVENT_ENTERED_SYSTEM: return "EVENT_ENTERED_SYSTEM"; break;
		case EVENT_DELIVERED: return "EVENT_DELIVERED"; break;
		case EVENT_PRIMARY_LOGIN: return "EVENT_PRIMARY_LOGIN"; break;
		case EVENT_ENTRUSTED_LOGIN: return "EVENT_ENTRUSTED_LOGIN"; break;
		case EVENT_SYSCERT_LOGIN: return "EVENT_SYSCERT_LOGIN"; break;
		case EVENT_UNDELIVERED_AV_CHECK: return "EVENT_UNDELIVERED_AV_CHECK"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_FileMetaType2str "Return text code of the library file meta-type.

:param metaType: Meta-type.
:type metaType: :ref:`isds_FileMetaType`
:return: (string) Text meta-type.
"
%inline
%{
	const char *isds_FileMetaType2str(isds_FileMetaType metaType)
	{
		switch (metaType) {
		case FILEMETATYPE_MAIN: return "FILEMETATYPE_MAIN"; break;
		case FILEMETATYPE_ENCLOSURE: return "FILEMETATYPE_ENCLOSURE"; break;
		case FILEMETATYPE_SIGNATURE: return "FILEMETATYPE_SIGNATURE"; break;
		case FILEMETATYPE_META: return "FILEMETATYPE_META"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_raw_type2str "Return text code of the library raw type.

:param rawType: Raw type.
:type rawType: :ref:`isds_raw_type`
:return: (string) Text raw type.
"
%inline
%{
	const char *isds_raw_type2str(isds_raw_type rawType)
	{
		switch (rawType) {
		case RAWTYPE_INCOMING_MESSAGE: return "RAWTYPE_INCOMING_MESSAGE"; break;
		case RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE: return "RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE"; break;
		case RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE: return "RAWTYPE_CMS_SIGNED_INCOMING_MESSAGE"; break;
		case RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE: return "RAWTYPE_PLAIN_SIGNED_OUTGOING_MESSAGE"; break;
		case RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE: return "RAWTYPE_CMS_SIGNED_OUTGOING_MESSAGE"; break;
		case RAWTYPE_DELIVERYINFO: return "RAWTYPE_DELIVERYINFO"; break;
		case RAWTYPE_PLAIN_SIGNED_DELIVERYINFO: return "RAWTYPE_PLAIN_SIGNED_DELIVERYINFO"; break;
		case RAWTYPE_CMS_SIGNED_DELIVERYINFO: return "RAWTYPE_CMS_SIGNED_DELIVERYINFO"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

%feature("docstring") isds_sender_type2str "Return text code of the library sender type.

:param senderType: Sender type.
:type senderType: :ref:`isds_sender_type`
:return: (string) Text sender type.
"
%inline
%{
	const char *isds_sender_type2str(isds_sender_type senderType)
	{
		switch (senderType) {
		case SENDERTYPE_PRIMARY: return "SENDERTYPE_PRIMARY"; break;
		case SENDERTYPE_ENTRUSTED: return "SENDERTYPE_ENTRUSTED"; break;
		case SENDERTYPE_ADMINISTRATOR: return "SENDERTYPE_ADMINISTRATOR"; break;
		case SENDERTYPE_OFFICIAL: return "SENDERTYPE_OFFICIAL"; break;
		case SENDERTYPE_VIRTUAL: return "SENDERTYPE_VIRTUAL"; break;
		case SENDERTYPE_OFFICIAL_CERT: return "SENDERTYPE_OFFICIAL_CERT"; break;
		case SENDERTYPE_LIQUIDATOR: return "SENDERTYPE_LIQUIDATOR"; break;
		case SENDERTYPE_RECEIVER: return "SENDERTYPE_RECEIVER"; break;
		case SENDERTYPE_GUARDIAN: return "SENDERTYPE_GUARDIAN"; break;
		default:
			assert(0);
			return "UNKNOWN";
			break;
		}
	}
%}

/* ========================================================================= */
/* Encapsulating Python code. */
/* ========================================================================= */

/* None. */
