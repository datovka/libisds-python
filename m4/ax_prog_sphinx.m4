# ===========================================================================
# TODO -- insert URL here
# ===========================================================================
#
# SYNOPSIS
#
#   SX_INIT_SPHINX()
#   SX_RESET_DEFAULTS()
#   SX_SPHINX_FEATURE(ON|OFF)
#   SX_HTML_FEATURE(ON|OFF)
#   SX_DOCUMENTATION(PROJECT-NAME, SOURCE-PATH, [OUTPUT-DIR])
#   SX_FINISH_SPHINX()
#
# DESCRIPTION
#
# Modified from
# http://www.gnu.org/software/autoconf-archive/ax_prog_doxygen.html
#
# LICENSE
#
#   Copyright (c) 2013 Karel Slany <karel.slany@nic.cz>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

#serial 1


## --------------- ##
## Private macros. ##
## --------------- ##


# SX_ENV_APPEND(VARIABLE, VALUE)
# ------------------------------
# Appends VARIABLE="VALUE" for invoking sphinx-build.
AC_DEFUN([SX_ENV_APPEND], [
  AC_SUBST([SX_ENV], ["$SX_ENV $1='$2'"])
])


# SX_PROJECT_APPEND(VALUE)
# ------------------------
# Appends "VALUE" to project names.
AC_DEFUN([SX_PROJECT_APPEND], [
  AC_SUBST([SX_PROJECTS], ["$SX_PROJECTS $1"])
])


# SX_IF_FEATURE(NAME, ACTION_ON, ACTION_OFF)
# ------------------------------------------
# Expand code according to the feature value.
AC_DEFUN([SX_IF_FEATURE], [ifelse(SX_FEATURE_$1, [ON], [$2], [$3])
])


# SX_CHECK_DEPEND(REQUIRED_FEATURE, REQUIRED_STATE)
# -------------------------------------------------
# Verify that a required features has the right state before trying to turn on
# the SX_CURRENT_FEATURE.
AC_DEFUN([SX_CHECK_DEPEND], [
  AS_IF([test "$SX_FLAG_$1" != "$2"], [
    AC_MSG_ERROR([sphinx-SX_CURRENT_FEATURE ifelse([$2], "1",
      [requires], [contradicts]) sphinx-SX_CURRENT_FEATURE])
  ])
])


# SX_CLEAR_DEPEND(FEATURE, REQUIRED_FEATURE, REQUIRED_STATE)
# ----------------------------------------------------------
# Turn off the SX_CURRENT_FEATURE if the required feature is off.
AC_DEFUN([SX_CLEAR_DEPEND], [
  AS_IF([test "$SX_FLAG_$1" != "$2"], [
    AC_SUBST(SX_FLAG_[]SX_CURRENT_FEATURE, "0")
  ])
])



# SX_REQUIRE_PROG(VARIABLE, PROGRAM)
# ----------------------------------
# Require the specified program to be found for the SX_CURRENT_FEATURE to work.
AC_DEFUN([SX_REQUIRE_PROG], [
  AC_PATH_TOOL([$1], [$2])
  AS_IF([test "SX_FLAG_[]SX_CURRENT_FEATURE$$1}" = "1"], [
    AC_MSG_WARN([$2 not found - will not SX_CURRENT_DESCRIPTION])
    AC_SUBST(SX_BUILD_[]SX_CURRENT_FEATURE, "0")
  ])
])


# SX_ARG_ABLE(FEATURE, DESCRIPTION, CHECK_DEPEND, CLEAR_DEPEND,
#   REQUIRE, DO-IF-ON, DO-IF-OFF)
# ---------------------------------
# Parse the command-line option controlling a feature. CHECK_DEPEND is called
# if the user explicitly turns the feature on (and invokes SX_CHECK_DEPEND),
# otherwise CLEAR_DEPEND is called to turn off the default state if a required
# feature is disabled (using SX_CLEAR_DEPEND). REQUIRE performs additional
# requirement tests (SX_REQUIRE_PROG). Finally, an automake flag is set and
# DO-IF-ON or DO-IF-OFF are called according to the final state of the feature.
AC_DEFUN([SX_ARG_ABLE], [
  AC_DEFUN([SX_CURRENT_FEATURE], [$1])
  AC_DEFUN([SX_CURRENT_DESCRIPTION], [$2])

  AC_ARG_ENABLE(sphinx-$1, [
    AS_HELP_STRING(
      SX_IF_FEATURE([$1], [--disable-sphinx-$1], [--enable-sphinx-$1]),
      SX_IF_FEATURE([$1], [don't $2], [$2]))
  ], [
    # Action if given.
    case "$enableval" in
    #(
    y|Y|yes|Yes|YES)
      AC_SUBST([SX_BUILD_$1], "1")
      $3
      ;;
    #(
    n|N|no|No|NO)
      AC_SUBST([SX_BUILD_$1], "0")
      ;;
    #(
    *)
      AC_MSG_ERROR([invalid value '$enableval' given to sphinx-$1])
      ;;
    esac
  ], [
    # Action if not given.
    AC_SUBST([SX_BUILD_$1], [SX_IF_FEATURE([$1], "1", "0")])
    $4
  ])

  AS_IF([test "${SX_BUILD_$1}" = "1"], [
    $5
    :
  ])
  AM_CONDITIONAL(SX_COND_$1, [test "${SX_BUILD_$1}" = "1"])
  AS_IF([test "${SX_BUILD_$1}" = "1"], [
    $6
    :
  ], [
    $7
    :
  ])
])


# SX_DEBUG_SPHINX()
# -----------------
# For debugging purposes.
AC_DEFUN([SX_DEBUG_SPHINX], [
  echo "SX_ENV=\"$SX_ENV\""
  echo "SX_PROJECTS=\"$SX_PROJECTS\""
  echo macro sx_FEATURE_doc       SX_FEATURE_doc
  echo macro sx_FEATURE_html      SX_FEATURE_html
])


## -------------- ##
## Public macros. ##
## -------------- ##


# SX_INIT_SPHINX()
# ----------------
# Initialises all internal variables to default values.
AC_DEFUN_ONCE([SX_INIT_SPHINX], [
  SX_ENV=""
  SX_PROJECTS=""
  SX_RESET_DEFAULTS
])


# SX_RESET_DEFAULTS()
# -------------------
# Resets configurable internal variables to their default values.
AC_DEFUN([SX_RESET_DEFAULTS], [
  AC_DEFUN([SX_FEATURE_doc],  ON)
  AC_DEFUN([SX_FEATURE_html], ON)
])


# SX_NAME_FEATURE(STATE_VALUE)
# ----------------------------
# Set builders to be used.
AC_DEFUN([SX_SPHINX_FEATURE],    [AC_DEFUN([SX_FEATURE_doc],  [$1])])
AC_DEFUN([SX_HTML_FEATURE],      [AC_DEFUN([SX_FEATURE_html], [$1])])


# SX_DOCUMENTATION(PROJECT-NAME, SOURCE-PATH, [OUTPUT-DIR])
# ---------------------------------------------------------
# Adds documentation to be built.
# PROJECT-NAME also serves as the base name for the documentation files.
# The default output directory is "PROJECT-NAME-sphinx-doc".
AC_DEFUN([SX_DOCUMENTATION], [

  # Files:
  SX_PROJECT="$1"
  SX_SOURCE=$2
  SX_OUTPUT=ifelse([$3], [], [$1-sphinx-doc], [$3])

  SX_PROJECT_APPEND([${SX_PROJECT}])
  SX_ENV_APPEND(PROJECT, [${SX_PROJECT}])
  SX_ENV_APPEND(SOURCE, [${SX_SOURCE}])
  SX_ENV_APPEND(OUTPUT, [${SX_OUTPUT}])
  SX_ENV_APPEND(VERSION, [${PACKAGE_VERSION}])
  AC_SUBST(SX_SOURCE, [${SX_SOURCE}])
  AC_SUBST(SX_OUTPUT, [${SX_OUTPUT}])
  AC_SUBST(SX_VERSION, [${PACKAGE_VERSION}])

])


# SX_FINISH_SPHINX()
# ------------------
# Finishes the configuration, generates configuration options.
AC_DEFUN([SX_FINISH_SPHINX], [

  # Sphinx itself:
  SX_ARG_ABLE([doc], [generate any sphinx documentation], [], [], [
    SX_REQUIRE_PROG([SX_SPHINX_BUILD], [sphinx-build])
  ], [])

  # HTML generation
  SX_ARG_ABLE([html], [generate sphinx HTML documentation],
    [SX_CHECK_DEPEND(doc, 1)], [SX_CLEAR_DEPEND(doc, 1)],
    [SX_ENV_APPEND([GENERATE_HTML], [YES])], [])

])
