# encoding: utf-8

# Copyright (C) 2019 CZ.NIC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Python high level API for libisds.

It requires at least Python 3.7.
"""
from abc import ABCMeta, abstractmethod
from datetime import date
from enum import IntEnum, unique

import isds

##########################################################################################
# Exceptions


@unique
class ErrorCode(IntEnum):
    """Enumeration with ISDS error codes."""

    SUCCESS = isds.IE_SUCCESS
    ERROR = isds.IE_ERROR
    NOTSUP = isds.IE_NOTSUP
    INVAL = isds.IE_INVAL
    INVALID_CONTEXT = isds.IE_INVALID_CONTEXT
    NOT_LOGGED_IN = isds.IE_NOT_LOGGED_IN
    CONNECTION_CLOSED = isds.IE_CONNECTION_CLOSED
    TIMED_OUT = isds.IE_TIMED_OUT
    NOEXIST = isds.IE_NOEXIST
    NOMEM = isds.IE_NOMEM
    NETWORK = isds.IE_NETWORK
    HTTP = isds.IE_HTTP
    SOAP = isds.IE_SOAP
    XML = isds.IE_XML
    ISDS = isds.IE_ISDS
    ENUM = isds.IE_ENUM
    DATE = isds.IE_DATE
    TOO_BIG = isds.IE_2BIG
    TOO_SMALL = isds.IE_2SMALL
    NOTUNIQ = isds.IE_NOTUNIQ
    NOTEQUAL = isds.IE_NOTEQUAL
    PARTIAL_SUCCESS = isds.IE_PARTIAL_SUCCESS
    ABORTED = isds.IE_ABORTED
    SECURITY = isds.IE_SECURITY

    @property
    def description(self):
        """Return human readable error description."""
        return isds.isds_strerror(self)


class IsdsError(Exception):
    """Generic error from libisds.

    Attributes:
        code (ErrorCode): Exception error code.
        message (str): Human readable string describing the exception.
    """

    def __init__(self, code, message):
        self.code = code
        self.message = message
        super(IsdsError, self).__init__(code, message)


class ConnectionFailed(IsdsError):
    """Connection to ISDS failed."""


class AuthenticationFailed(IsdsError):
    """Authentication to ISDS failed."""


class TotpCodeRequired(IsdsError):
    """TOTP code has been sent through a side channel and is now required to complete the login procedure."""


##########################################################################################
# Helper classes
class Credentials(object):
    """Base class for credential classes.

    Attributes:
        username (str): User login name.
    """
    __metaclass__ = ABCMeta

    def __init__(self, username):
        self.username = username

    @abstractmethod
    def login(self, connection, test=False):
        """Login to connection.

        Derived class must implement this method.

        Arguments:
            connection (isds.isds_ctx): Connection object
            test (bool): Whether to login to test environment.

        Raises:
            AuthenticationFailed: If authentication fails.
        """


class Password(Credentials):
    """Login with plain password.

    Attributes:
        password (str): User login password.
    """
    def __init__(self, username, password):
        super(Password, self).__init__(username)
        self.password = password

    def login(self, connection, test=False):
        """Login to connection with password.

        Arguments:
            connection (isds.isds_ctx): Connection object
            test (bool): Whether to login to test environment.

        Raises:
            AuthenticationFailed: If authentication fails.
        """
        result = connection.login_pwd(self.username, password=self.password, test_env=test)
        if result != ErrorCode.SUCCESS:
            raise AuthenticationFailed(result, connection.long_message())


@unique
class KeyFormat(IntEnum):
    """Enumeration with certificate key formats."""

    PEM = isds.PKI_FORMAT_PEM
    DER = isds.PKI_FORMAT_DER
    ENG = isds.PKI_FORMAT_ENG


class Certificate(Credentials):
    """Login with certificate.

    Attributes:
        password (str): User password.
            You don't have to supply the password if the account just uses the certificate to log in.
        cert_format (KeyFormat): Certificate format.
        cert (str): Path to client certificate.
        key_format (KeyFormat): Private key format.
        key (str): Path to client private key.
        passphrase (str): String with password for private key decryption. Use 'None' for no pass-phrase.
    """

    def __init__(self, username, password=None, cert_format=KeyFormat.PEM, cert=None, key_format=KeyFormat.PEM,
                 key=None, passphrase=None):
        super(Certificate, self).__init__(username)
        self.password = password
        self.cert_format = cert_format
        self.cert = cert
        self.key_format = key_format
        self.key = key
        self.passphrase = passphrase

    def login(self, connection, test=False):
        """Login to connection with certificate.

        Arguments:
            connection (isds.isds_ctx): Connection object
            test (bool): Whether to login to test environment.

        Raises:
            AuthenticationFailed: If authentication fails.
        """
        result = connection.login_cert(self.username, self.password, cert_format=self.cert_format, cert=self.cert,
                                       key_format=self.key_format, key=self.key, passphrase=self.passphrase,
                                       test_env=test)
        if result != ErrorCode.SUCCESS:
            raise AuthenticationFailed(result, connection.long_message())


class Hotp(Credentials):
    """HMAC-based OTP login.

    Attributes:
        password (str): User password.
        hotp_code (str): HMAC-based OTP code.
    """

    def __init__(self, username, password, hotp_code):
        super(Hotp, self).__init__(username)
        self.password = password
        self.hotp_code = hotp_code

    def login(self, connection, test=False):
        """Login to connection using a HMAC-based OTP code.

        Arguments:
            connection (isds.isds_ctx): Connection object
            test (bool): Whether to login to test environment.

        Raises:
            AuthenticationFailed: If authentication fails.
        """
        result, otp_result = connection.login_hotp(self.username, self.password, self.hotp_code, test_env=test)
        if result != ErrorCode.SUCCESS:
            raise AuthenticationFailed(result, connection.long_message())


class Totp(Credentials):
    """Time-based OTP login.

    Attributes:
        password (str): User password.
        totp_code (str): Time-based OTP code (SMS code).
    """

    def __init__(self, username, password, totp_code=None):
        super(Totp, self).__init__(username)
        self.password = password
        self.totp_code = totp_code

    def login(self, connection, test=False):
        """Login to connection using a HMAC-based OTP code.

        Arguments:
            connection (isds.isds_ctx): Connection object.
            test (bool): Whether to login to test environment.

        Raises:
            TotpCodeRequired: If the request for the SMS code succeeded.
                The application is expected to fill in the obtained code into `totp_code` and retry the login.
            AuthenticationFailed: If authentication fails.
        """
        result, otp_result = connection.login_totp(self.username, self.password, self.totp_code, test_env=test)
        if result == ErrorCode.PARTIAL_SUCCESS:
            raise TotpCodeRequired(result, connection.long_message())
        elif result != ErrorCode.SUCCESS:
            raise AuthenticationFailed(result, connection.long_message())


@unique
class DataBoxType(IntEnum):
    """Enumeration with data box types."""

    OVM_MAIN = isds.DBTYPE_OVM_MAIN
    SYSTEM = isds.DBTYPE_SYSTEM
    OVM = isds.DBTYPE_OVM
    OVM_NOTAR = isds.DBTYPE_OVM_NOTAR
    OVM_EXEKUT = isds.DBTYPE_OVM_EXEKUT
    OVM_REQ = isds.DBTYPE_OVM_REQ
    OVM_FO = isds.DBTYPE_OVM_FO
    OVM_PFO = isds.DBTYPE_OVM_PFO
    OVM_PO = isds.DBTYPE_OVM_PO
    PO = isds.DBTYPE_PO
    PO_ZAK = isds.DBTYPE_PO_ZAK
    PO_REQ = isds.DBTYPE_PO_REQ
    PFO = isds.DBTYPE_PFO
    PFO_ADVOK = isds.DBTYPE_PFO_ADVOK
    PFO_DANPOR = isds.DBTYPE_PFO_DANPOR
    PFO_INSSPR = isds.DBTYPE_PFO_INSSPR
    PFO_AUDITOR = isds.DBTYPE_PFO_AUDITOR
    FO = isds.DBTYPE_FO


class _PersonName(object):
    """Name of a person.

    Attributes:
        first_name (str): First name.
        middle_name (str): Middle name.
        last_name (str): Last name.
        last_name_at_birth (str): Last name at birth.
    """
    def __init__(self, first_name=None, middle_name=None, last_name=None, last_name_at_birth=None, **kwargs):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.last_name_at_birth = last_name_at_birth
        super(_PersonName, self).__init__(**kwargs)


class _BirthInfo(object):
    """Information about birth.

    Attributes:
        birth_date (date): Date of birth.
        birth_city (str): City of birth.
        birth_country (str): Country of birth.
        birth_state (str): State of birth.
    """
    def __init__(self, birth_date=None, birth_city=None, birth_country=None, birth_state=None, **kwargs):
        self.birth_date = birth_date
        self.birth_city = birth_city
        self.birth_country = birth_country
        self.birth_state = birth_state
        super(_BirthInfo, self).__init__(**kwargs)


class _Address(object):
    """Information about address.

    Attributes:
        street (str): Street.
        conscription_number (str): Conscription number.
        orientation_number (str): Orientation number.
        city (str): City.
        zip_code (str): Zip code.
        state (str): Country.
    """
    def __init__(self, street=None, conscription_number=None, orientation_number=None, city=None, zip_code=None,
                 state=None, **kwargs):
        self.street = street
        self.conscription_number = conscription_number  # adNumberInMunicipality
        self.orientation_number = orientation_number  # adNumberInStreet
        self.city = city
        self.zip_code = zip_code
        self.state = state
        super(_Address, self).__init__(**kwargs)


class DataBoxOwner(_PersonName, _BirthInfo, _Address):
    """Data box owner info.

    Attributes:
        ic (str): Identification number in registry of commerce.
        firm_name (str): Company name.
        nationality (str): Nationality.
        email (str): Email address.
        telephone (str): Phone number.
        identifier (str): External identifier.
        registry_code (str): Code of external registry.
        db_id (str): Data box identifier.
        db_type (DataBoxType): Data box type.
        db_state (int): Data box state
        is_effective_ovm (bool): Whether data box represent a public office.
        has_open_addressing (bool): Whether data box has enabled open addressing.
    """
    def __init__(
            self, ic=None, firm_name=None, nationality=None, email=None, telephone=None, identifier=None,
            registry_code=None, db_id=None, db_type=None, db_state=None, is_effective_ovm=None,
            has_open_addressing=None, **kwargs):
        self.ic = ic
        self.firm_name = firm_name
        self.nationality = nationality
        self.email = email
        self.telephone = telephone
        self.identifier = identifier
        self.registry_code = registry_code
        self.db_id = db_id
        self.db_type = db_type
        self.db_state = db_state
        self.is_effective_ovm = is_effective_ovm  # dbEffectiveOVM
        self.has_open_addressing = has_open_addressing  # dbOpenAddressing
        super(DataBoxOwner, self).__init__(**kwargs)


class DataBoxList(list):
    """List of data box owners.

    Attributes:
        truncated (bool): Whether the list was truncated.
    """

    def __init__(self, iterable=None, truncated=False):
        super(list, self).__init__(iterable or ())
        self.truncated = truncated


##########################################################################################
# Client

# TODO: Ensure thread safety.
# TODO: If the connection fails, try to reconnect.  - Reconnection using HOTP
# and TOTP requires user interaction. TOTP authentication uses premium SMS
# messages which cost money. Reconnection using a certificate may in some cases
# also require user interaction to decrypt the private key.
def _decode_person_name(person_name, owner):
    """Decodes person name to data box owner data."""
    if person_name is None:
        return

    owner.first_name = person_name.pnFirstName
    owner.middle_name = person_name.pnMiddleName
    owner.last_name = person_name.pnLastName
    owner.last_name_at_birth = person_name.pnLastNameAtBirth


def _decode_birth_info(birth_info, owner):
    """Decodes birth info to data box owner data."""
    if birth_info is None:
        return

    owner.birth_city = birth_info.biCity
    owner.birth_country = birth_info.biCounty
    owner.birth_state = birth_info.biState

    bi_date = birth_info.biDate
    if birth_info.biDate:
        owner.birth_date = date(bi_date.year, bi_date.mon, bi_date.mday)


def _decode_address(address, owner):
    """Decodes address to data box owner data."""
    if address is None:
        return

    owner.street = address.adStreet
    owner.conscription_number = address.adNumberInMunicipality
    owner.orientation_number = address.adNumberInStreet
    owner.city = address.adCity
    owner.zip_code = address.adZipCode
    owner.state = address.adState


class Client:
    """ISDS client.

    Connects and authenticates lazily, i.e. when needed.

    Attributes:
        credentials (Credentials): User credentials.
        timeout (int): Timeout in seconds.
        test (bool): If True, connects to testing environment.
    """

    def __init__(self, credentials, timeout=None, test=False):
        self.credentials = credentials
        self.timeout = timeout
        self.test = test
        self._connection = None

    def __del__(self):
        """Log out from ISDS.

        Ignores any errors from log out.
        """
        if self._connection is not None:
            # Ignore all errors from logout.
            self._connection.logout()

    @property
    def connection(self):
        """Return connection object.

        If not connected, creates a connection.
        """
        if self._connection is None:
            self.connect()
        return self._connection

    def connect(self):
        """Connect to the ISDS.

        Raises:
            ConnectionFailed: If connection failed.
            AuthenticationFailed: If authentication failed.
        """
        self._connection = isds.isds_ctx()
        if self.timeout:
            result = self._connection.set_timeout(self.timeout * 1000)
            if result != ErrorCode.SUCCESS:
                raise ConnectionFailed(result, self._connection.long_message())
        self.credentials.login(self._connection, test=self.test)

    def ping(self):
        """Check if connection is still alive.

        Raises:
            ConnectionFailed: If connection is no longer active.
        """
        result = self.connection.ping()
        if result != ErrorCode.SUCCESS:
            raise ConnectionFailed(result, self._connection.long_message())

    def find_box(
            self, db_type, first_name=None, middle_name=None, last_name=None, last_name_at_birth=None,
            birth_date=None, birth_city=None, birth_country=None, birth_state=None,
            street=None, conscription_number=None, orientation_number=None, city=None, zip_code=None, state=None,
            db_id=None, ic=None, firm_name=None, nationality=None, email=None, telephone=None, identifier=None,
            registry_code=None, db_state=None, is_effective_ovm=None, has_open_addressing=None):
        """Find data boxes based on filter criteria."""
        query = isds.isds_db_owner_info()
        if first_name is not None or middle_name is not None or last_name is not None or last_name_at_birth is not None:
            query.personName = isds.isds_person_name(pnFirstName=first_name, pnMiddleName=middle_name,
                                                     pnLastName=last_name, pnLastNameAtBirth=last_name_at_birth)
        if birth_date is not None or birth_city is not None or birth_country is not None or birth_state is not None:
            birth_info = isds.isds_birth_info(biCity=birth_city, biCounty=birth_country, biState=birth_state)
            if birth_date is not None:
                birth_info.biDate = isds.isds_date(birth_date.year, birth_date.month, birth_date.day)
            query.birthInfo = birth_info
        if (street is not None or conscription_number is not None or orientation_number is not None or city is not None
                or zip_code is not None or state is not None):
            query.address = isds.isds_address(
                adStreet=street, adNumberInMunicipality=conscription_number, adNumberInStreet=orientation_number,
                adCity=city, adZipCode=zip_code, adState=state)

        query.dbID = db_id
        query.dbType = db_type
        query.ic = ic
        query.firmName = firm_name
        query.nationality = nationality
        query.email = email
        query.telNumber = telephone
        query.identifier = identifier
        query.registryCode = registry_code
        query.dbState = db_state
        query.dbEffectiveOVM = is_effective_ovm
        query.dbOpenAddressing = has_open_addressing

        return_code, result = self.connection.find_box(query)
        if return_code == ErrorCode.NOEXIST:
            # No data box was found, return empty list.
            return DataBoxList()
        if return_code not in (ErrorCode.SUCCESS, ErrorCode.TOO_BIG):
            raise IsdsError(return_code, self.connection.long_message())
        data_boxes = DataBoxList(truncated=(return_code == ErrorCode.TOO_BIG))
        for item in result:
            data_box = DataBoxOwner(
                db_id=item.dbID,
                db_type=item.dbType,
                ic=item.ic,
                firm_name=item.firmName,
                nationality=item.nationality,
                email=item.email,
                telephone=item.telNumber,
                identifier=item.identifier,
                registry_code=item.registryCode,
                db_state=item.dbState,
                is_effective_ovm=item.dbEffectiveOVM,
                has_open_addressing=item.dbOpenAddressing,
            )
            _decode_person_name(item.personName, data_box)
            _decode_birth_info(item.birthInfo, data_box)
            _decode_address(item.address, data_box)
            data_boxes.append(data_box)
        return data_boxes
