# -*- encoding: utf-8 -*-
import os
import subprocess
import sys
from distutils.command.build import build as _build

from setuptools import Command, Extension, setup
from setuptools.command.build_ext import build_ext as _build_ext

EXTENSIONS = [Extension('isds_memory_management', sources=['isds_memory_management.c']),
              Extension('_isds', sources=['isds.i'], libraries=['isds'])]
LONG_DESCRIPTION = open('README').read()
INSTALL_REQUIRES = ['enum34; python_version<="3.3.*"']
EXTRAS_REQUIRE = {
    'quality': ['isort', 'flake8', 'bandit', 'pydocstyle'],
    'test': ['mock; python_version=="2.7.*"'],
}


class Configure(Command):
    """Run configure (autoconf)."""
    description = "Run configure (autoconf)"
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        if not os.path.exists("config.status"):
            if not os.path.exists('configure'):
                subprocess.check_call('./autogen.sh')
            cmd = ["./configure"]
            if self.verbose < 1:
                cmd += ["--quiet"]
            subprocess.check_call(
                cmd,
                env=dict(os.environ, PYTHON_VERSION=os.environ.get('PYTHON_VERSION', sys.version[:3])))


class build_ext(_build_ext):
    """Custom build_ext command to include configure phase."""
    def finalize_options(self):
        if sys.version < '3':
            _build_ext.finalize_options(self)
        else:
            super().finalize_options()
            self.swig_opts.insert(0, '-py3')
            self.swig_opts.insert(1, '-DPY3')

        # Add isds_memory_management to link to _isds library.
        for ext in self.extensions:
            if ext.name == '_isds':
                ext.extra_objects.append(os.path.join(self.build_temp, 'isds_memory_management.o'))

    def run(self):
        # Run the subcommands first, then build itself
        for cmd_name in self.get_sub_commands():
            self.run_command(cmd_name)

        if sys.version < '3':
            _build_ext.run(self)
        else:
            super().run()

    sub_commands = [('configure', None)] + _build_ext.sub_commands


class build(_build):
    """Custom build command to compile first and collect python later."""
    sub_commands = [('build_ext', None)] + _build.sub_commands


setup(
    name='isds',
    version='0.0.0',
    license='GPLv3+',
    description='This project provides a simplistic wrapper of some libisds functions for Python.',
    long_description=LONG_DESCRIPTION,
    author=u'Karel Slaný',
    author_email='karel.slany@nic.cz',
    url='https://gitlab.labs.nic.cz/datovka/libisds-python',
    py_modules=['isds', 'pretty_isds'],
    ext_modules=EXTENSIONS,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Communications',
        'Topic :: Internet',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
    ],
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
    cmdclass={'configure': Configure, 'build_ext': build_ext, 'build': build},
)
