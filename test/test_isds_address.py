#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Address functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsAddress(unittest.TestCase):
    def test_adCity(self):
        test_isds_object(self, isds.isds_address(), 'adCity', 'City')

    def test_adStreet(self):
        test_isds_object(self, isds.isds_address(), 'adStreet', 'Street')

    def test_adNumberInStreet(self):
        test_isds_object(self, isds.isds_address(), 'adNumberInStreet', 'Orientational number')

    def test_adNumberInMunicipality(self):
        test_isds_object(self, isds.isds_address(), 'adNumberInMunicipality', 'Conscription number')

    def test_adZipCode(self):
        test_isds_object(self, isds.isds_address(), 'adZipCode', 'Postal code')

    def test_adState(self):
        test_isds_object(self, isds.isds_address(), 'adState', 'State')
