#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Auxiliary structures functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsTime(unittest.TestCase):
    def test_sec(self):
        test_isds_object(self, isds.isds_time(0, 0), 'sec', 8, empty_value=0)

    def test_usec(self):
        test_isds_object(self, isds.isds_time(0, 0), 'usec', 7, empty_value=0)


class TestIsdsDate(unittest.TestCase):
    def test_year(self):
        test_isds_object(self, isds.isds_date(0, 0, 0), 'year', 1992, empty_value=0)

    def test_mon(self):
        test_isds_object(self, isds.isds_date(0, 0, 0), 'mon', 4, empty_value=0)

    def test_mday(self):
        test_isds_object(self, isds.isds_date(0, 0, 0), 'mday', 20, empty_value=0)
