#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Birrh info functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsBirthInfo(unittest.TestCase):
    def test_biDate(self):
        val = isds.isds_date(1992, 4, 20)
        test_isds_object(self, isds.isds_birth_info(), 'biDate', val)

    def test_biCity(self):
        test_isds_object(self, isds.isds_birth_info(), 'biCity', 'city')

    def test_biCounty(self):
        test_isds_object(self, isds.isds_birth_info(), 'biCounty', 'County')

    def test_biState(self):
        test_isds_object(self, isds.isds_birth_info(), 'biState', 'State')
