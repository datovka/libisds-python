#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Data box owner information functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsDbOwnerInfo(unittest.TestCase):
    def test_dbID(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'dbID', 'abcdefg')

    def test_dbType(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'dbType', isds.DBTYPE_FO)

    def test_ic(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'ic', '1234')

    def test_personName(self):
        val = isds.isds_person_name()
        val.pnFirstName = "Uwe"
        val.pnLastName = "Filter"
        test_isds_object(self, isds.isds_db_owner_info(), 'personName', val)

    def test_firmName(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'firmName', 'Firm name')

    def test_birthInfo(self):
        val = isds.isds_birth_info()
        val.biCity = "City"
        test_isds_object(self, isds.isds_db_owner_info(), 'birthInfo', val)

    def test_address(self):
        val = isds.isds_address()
        val.adCity = "City"
        test_isds_object(self, isds.isds_db_owner_info(), 'address', val)

    def test_nationality(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'nationality', 'Nationality')

    def test_email(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'email', 'Email')

    def test_telNumber(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'telNumber', '+420000000000')

    def test_identifier(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'identifier', 'abcdef')

    def test_registryCode(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'registryCode', 'ab01')

    def test_dbState(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'dbState', 1)

    def test_dbEffectiveOVM(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'dbEffectiveOVM', False)
        test_isds_object(self, isds.isds_db_owner_info(), 'dbEffectiveOVM', True)

    def test_dbOpenAddressing(self):
        test_isds_object(self, isds.isds_db_owner_info(), 'dbOpenAddressing', False)
        test_isds_object(self, isds.isds_db_owner_info(), 'dbOpenAddressing', True)
