#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Data box user information functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsDbUserInfo(unittest.TestCase):
    def test_userID(self):
        test_isds_object(self, isds.isds_db_user_info(), 'userID', 'abcdefg')

    def test_userType(self):
        test_isds_object(self, isds.isds_db_user_info(), 'userType', isds.USERTYPE_ENTRUSTED)

    def test_userPrivils(self):
        test_isds_object(self, isds.isds_db_user_info(), 'userPrivils', 1)

    def test_personName(self):
        val = isds.isds_person_name()
        val.pnFirstName = "Uwe"
        val.pnLastName = "Filter"
        test_isds_object(self, isds.isds_db_user_info(), 'personName', val)

    def test_address(self):
        val = isds.isds_address()
        val.adCity = "City"
        test_isds_object(self, isds.isds_db_user_info(), 'address', val)

    def test_biDate(self):
        val = isds.isds_date(1992, 4, 20)
        test_isds_object(self, isds.isds_db_user_info(), 'biDate', val)

    def test_ic(self):
        test_isds_object(self, isds.isds_db_user_info(), 'ic', '1234')

    def test_firmName(self):
        test_isds_object(self, isds.isds_db_user_info(), 'firmName', 'Firm name')

    def test_caStreet(self):
        test_isds_object(self, isds.isds_db_user_info(), 'caStreet', 'Street 1')

    def test_caCity(self):
        test_isds_object(self, isds.isds_db_user_info(), 'caCity', 'City')

    def test_caZipCode(self):
        test_isds_object(self, isds.isds_db_user_info(), 'caZipCode', 'Postal code')

    def test_caState(self):
        test_isds_object(self, isds.isds_db_user_info(), 'caState', 'DE')
