#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Document functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsDocument(unittest.TestCase):
    def test_is_xml(self):
        test_isds_object(self, isds.isds_document(), 'is_xml', True, empty_value=False)

    def test_data(self):
        test_isds_object(self, isds.isds_document(), 'data', b'1234', empty_value=b'')
        # Check None is coerced to b''
        doc = isds.isds_document()
        doc.data = None
        self.assertEqual(doc.data, b'')

    def test_dmMimeType(self):
        test_isds_object(self, isds.isds_document(), 'dmMimeType', "text/plain")

    def test_dmFileMetaType(self):
        test_isds_object(self, isds.isds_document(), 'dmFileMetaType', isds.FILEMETATYPE_ENCLOSURE,
                         empty_value=isds.FILEMETATYPE_MAIN)

    def test_dmFileGuid(self):
        test_isds_object(self, isds.isds_document(), 'dmFileGuid', "file GUID")

    def test_dmUpFileGuid(self):
        test_isds_object(self, isds.isds_document(), 'dmUpFileGuid', "up file GUID")

    def test_dmFileDescr(self):
        test_isds_object(self, isds.isds_document(), 'dmFileDescr', "file descr")
