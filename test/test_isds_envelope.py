#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Envelope functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsEnvelope(unittest.TestCase):
    def test_dmID(self):
        test_isds_object(self, isds.isds_envelope(), 'dmID', '123456')

    def test_dbIDSender(self):
        test_isds_object(self, isds.isds_envelope(), 'dbIDSender', 'abcd12')

    def test_dmSender(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSender', 'Sender name')

    def test_dmSenderAddress(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderAddress', 'Sender address')

    def test_dmSenderType(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderType', 40)

    def test_dmRecipient(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipient', "Recipient name")

    def test_dmRecipientAddress(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipientAddress', "Recipient address")

    def test_dmAmbiguousRecipient(self):
        test_isds_object(self, isds.isds_envelope(), 'dmAmbiguousRecipient', False)
        test_isds_object(self, isds.isds_envelope(), 'dmAmbiguousRecipient', True)

    def test_dmOrdinal(self):
        test_isds_object(self, isds.isds_envelope(), 'dmOrdinal', 4)

    def test_dmMessageStatus(self):
        test_isds_object(self, isds.isds_envelope(), 'dmMessageStatus', isds.MESSAGESTATE_RECEIVED)

    def test_dmAttachmentSize(self):
        test_isds_object(self, isds.isds_envelope(), 'dmAttachmentSize', 4)

    def test_dmDeliveryTime(self):
        val = isds.isds_time(10, 20)
        test_isds_object(self, isds.isds_envelope(), 'dmDeliveryTime', val)

    def test_dmAcceptanceTime(self):
        val = isds.isds_time(10, 40)
        test_isds_object(self, isds.isds_envelope(), 'dmAcceptanceTime', val)

    def test_hash(self):
        val = isds.isds_hash()
        val.algorithm = isds.HASH_ALGORITHM_MD5
        val.value = b'1234'
        test_isds_object(self, isds.isds_envelope(), 'hash', val)

    def test_timestamp(self):
        test_isds_object(self, isds.isds_envelope(), 'timestamp', b'1234', empty_value=b'')
        # Check None is coerced to b''
        env = isds.isds_envelope()
        env.timestamp = None
        self.assertEqual(env.timestamp, b'')

    def test_events(self):
        val = []
        event = isds.isds_event()
        val.append(event)
        test_isds_object(self, isds.isds_envelope(), 'events', val, empty_value=[])

    def test_dmSenderOrgUnit(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderOrgUnit', "Sender organisation unit")

    def test_dmSenderOrgUnitNum(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderOrgUnitNum', 6)

    def test_dbIDRecipient(self):
        test_isds_object(self, isds.isds_envelope(), 'dbIDRecipient', "abcd14")

    def test_dmRecipientOrgUnit(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipientOrgUnit', "Recipient organisation unit")

    def test_dmRecipientOrgUnitNum(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipientOrgUnitNum', 7)

    def test_dmToHands(self):
        test_isds_object(self, isds.isds_envelope(), 'dmToHands', "To hands")

    def test_dmAnnotation(self):
        test_isds_object(self, isds.isds_envelope(), 'dmAnnotation', "Annotation")

    def test_dmRecipientRefNumber(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipientRefNumber', "Recipient reference number")

    def test_dmSenderRefNumber(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderRefNumber', "Sender reference number")

    def test_dmRecipientIdent(self):
        test_isds_object(self, isds.isds_envelope(), 'dmRecipientIdent', "Recipient identification")

    def test_dmSenderIdent(self):
        test_isds_object(self, isds.isds_envelope(), 'dmSenderIdent', "Sender identification")

    def test_dmLegalTitleLaw(self):
        test_isds_object(self, isds.isds_envelope(), 'dmLegalTitleLaw', 120)

    def test_dmLegalTitleYear(self):
        test_isds_object(self, isds.isds_envelope(), 'dmLegalTitleYear', 2008)

    def test_dmLegalTitleSect(self):
        test_isds_object(self, isds.isds_envelope(), 'dmLegalTitleSect', "Legal section")

    def test_dmLegalTitlePar(self):
        test_isds_object(self, isds.isds_envelope(), 'dmLegalTitlePar', "Legal paragraph")

    def test_dmLegalTitlePoint(self):
        test_isds_object(self, isds.isds_envelope(), 'dmLegalTitlePoint', "Legal point")

    def test_dmPersonalDelivery(self):
        test_isds_object(self, isds.isds_envelope(), 'dmPersonalDelivery', False)
        test_isds_object(self, isds.isds_envelope(), 'dmPersonalDelivery', True)

    def test_dmAllowSubstDelivery(self):
        test_isds_object(self, isds.isds_envelope(), 'dmAllowSubstDelivery', False)
        test_isds_object(self, isds.isds_envelope(), 'dmAllowSubstDelivery', True)

    def test_dmType(self):
        test_isds_object(self, isds.isds_envelope(), 'dmType', "V")

    def test_dmOVM(self):
        test_isds_object(self, isds.isds_envelope(), 'dmOVM', False)
        test_isds_object(self, isds.isds_envelope(), 'dmOVM', True)

    def test_dmPublishOwnID(self):
        test_isds_object(self, isds.isds_envelope(), 'dmPublishOwnID', False)
        test_isds_object(self, isds.isds_envelope(), 'dmPublishOwnID', True)
