#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Event functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsEvent(unittest.TestCase):
    def test_time(self):
        val = isds.isds_time(10, 20)
        test_isds_object(self, isds.isds_event(), 'time', val)

    def test_type(self):
        test_isds_object(self, isds.isds_event(), 'type', isds.EVENT_DELIVERED)

    def test_description(self):
        test_isds_object(self, isds.isds_event(), 'description', 'Event')
