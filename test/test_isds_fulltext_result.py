#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Full-text result functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsFulltextResult(unittest.TestCase):
    def test_dbID(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'dbID', "abcd123")

    def test_dbType(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'dbType', isds.DBTYPE_FO, isds.DBTYPE_SYSTEM)

    def test_name(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'name', 'Name')

    def test_address(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'address', 'Address')

    def test_ic(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'ic', "1234abcd")

    def test_biDate(self):
        val = isds.isds_date(1992, 4, 20)
        test_isds_object(self, isds.isds_fulltext_result(), 'biDate', val)

    def test_dbEffectiveOVM(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'dbEffectiveOVM', True, empty_value=False)

    def test_active(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'active', True, empty_value=False)

    def test_public_sending(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'public_sending', True, empty_value=False)

    def test_commercial_sending(self):
        test_isds_object(self, isds.isds_fulltext_result(), 'commercial_sending', True, empty_value=False)
