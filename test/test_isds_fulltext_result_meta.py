#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Full-text result metadata functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsFulltextResultMeta(unittest.TestCase):
    def test_total_matching_boxes(self):
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'total_matching_boxes', 4)

    def test_current_page_beginning(self):
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'current_page_beginning', 4)

    def test_current_page_size(self):
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'current_page_size', 4)

    def test_last_page(self):
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'last_page', False)
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'last_page', True)

    def test_boxes(self):
        val = []
        fr = isds.isds_fulltext_result()
        val.append(fr)
        test_isds_object(self, isds.isds_fulltext_result_meta(), 'boxes', val, empty_value=[])
