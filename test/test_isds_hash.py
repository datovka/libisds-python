#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Hash functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsHash(unittest.TestCase):
    def test_algorithm(self):
        test_isds_object(self, isds.isds_hash(), 'algorithm', isds.HASH_ALGORITHM_SHA_256,
                         empty_value=isds.HASH_ALGORITHM_MD5)

    def test_value(self):
        test_isds_object(self, isds.isds_hash(), 'value', b'1234', empty_value=b'')
