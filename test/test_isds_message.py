#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Message functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsMessage(unittest.TestCase):
    def test_raw(self):
        test_isds_object(self, isds.isds_message(), 'raw', b'abcdef', empty_value=b'')
        # Check None is coerced to b''
        msg = isds.isds_message()
        msg.raw = None
        self.assertEqual(msg.raw, b'')

    def test_raw_type(self):
        test_isds_object(self, isds.isds_message(), 'raw_type', isds.RAWTYPE_PLAIN_SIGNED_INCOMING_MESSAGE,
                         empty_value=isds.RAWTYPE_INCOMING_MESSAGE)

    def test_envelope(self):
        val = isds.isds_envelope()
        val.dmID = "1234"
        test_isds_object(self, isds.isds_message(), 'envelope', val)

    def test_documents(self):
        val = []
        document = isds.isds_document()
        val.append(document)
        test_isds_object(self, isds.isds_message(), 'documents', val, empty_value=[])
