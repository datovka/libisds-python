#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Message author functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsMessageAuthor(unittest.TestCase):
    def test_userType(self):
        test_isds_object(self, isds.isds_message_author(), 'userType', isds.SENDERTYPE_ADMINISTRATOR)

    def test_rawUserType(self):
        test_isds_object(self, isds.isds_message_author(), 'rawUserType', "ADMINISTRATOR")

    def test_authorName(self):
        test_isds_object(self, isds.isds_message_author(), 'authorName', "Uwe Filter")
