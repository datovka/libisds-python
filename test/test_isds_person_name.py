#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# Person name functionality.
#

import unittest

import isds

from .utils import test_isds_object


class TestIsdsPersonName(unittest.TestCase):
    def test_pnFirstName(self):
        test_isds_object(self, isds.isds_person_name(), 'pnFirstName', "First name")

    def test_pnMiddleName(self):
        test_isds_object(self, isds.isds_person_name(), 'pnMiddleName', "Middle name")

    def test_pnLastName(self):
        test_isds_object(self, isds.isds_person_name(), 'pnLastName', "Last name")

    def test_pnLastNameAtBirth(self):
        test_isds_object(self, isds.isds_person_name(), 'pnLastNameAtBirth', "Last name at birth")
