from unittest import TestCase

from isds import isds_ctx
from pretty_isds import (AuthenticationFailed, Certificate, Client, ConnectionFailed, ErrorCode, Hotp, KeyFormat,
                         Password, Totp, TotpCodeRequired)

try:
    from unittest.mock import Mock, call, patch, sentinel
except ImportError:
    from mock import Mock, call, patch, sentinel


class TestErrorCode(TestCase):
    def test_description(self):
        self.assertEqual(ErrorCode.ERROR.description, 'Unspecified error')


class TestPassword(TestCase):
    def test_login(self):
        credentials = Password(username=sentinel.username, password=sentinel.password)
        mock = Mock(spec=isds_ctx)
        mock.login_pwd.return_value = ErrorCode.SUCCESS

        credentials.login(mock)

        self.assertEqual(mock.mock_calls,
                         [call.login_pwd(sentinel.username, password=sentinel.password, test_env=False)])

    def test_login_failed(self):
        credentials = Password(username=sentinel.username, password=sentinel.password)
        mock = Mock(spec=isds_ctx)
        mock.login_pwd.return_value = ErrorCode.ERROR
        mock.long_message.return_value = sentinel.message

        with self.assertRaises(AuthenticationFailed) as catcher:
            credentials.login(mock)
        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        calls = [call.login_pwd(sentinel.username, password=sentinel.password, test_env=False),
                 call.long_message()]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_test(self):
        credentials = Password(username=sentinel.username, password=sentinel.password)
        mock = Mock(spec=isds_ctx)
        mock.login_pwd.return_value = ErrorCode.SUCCESS

        credentials.login(mock, test=True)

        self.assertEqual(mock.mock_calls,
                         [call.login_pwd(sentinel.username, password=sentinel.password, test_env=True)])


class TestCertificate(TestCase):
    def test_login(self):
        credentials = Certificate(username=sentinel.username, cert=sentinel.cert)
        mock = Mock(spec=isds_ctx)
        mock.login_cert.return_value = ErrorCode.SUCCESS

        credentials.login(mock)

        calls = [call.login_cert(sentinel.username, None, cert_format=KeyFormat.PEM, cert=sentinel.cert,
                                 key_format=KeyFormat.PEM, key=None, passphrase=None, test_env=False)]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_failed(self):
        credentials = Certificate(username=sentinel.username, cert=sentinel.cert)
        mock = Mock(spec=isds_ctx)
        mock.login_cert.return_value = ErrorCode.ERROR
        mock.long_message.return_value = sentinel.message

        with self.assertRaises(AuthenticationFailed) as catcher:
            credentials.login(mock)
        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        calls = [call.login_cert(sentinel.username, None, cert_format=KeyFormat.PEM, cert=sentinel.cert,
                                 key_format=KeyFormat.PEM, key=None, passphrase=None, test_env=False),
                 call.long_message()]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_test(self):
        credentials = Certificate(username=sentinel.username, cert=sentinel.cert)
        mock = Mock(spec=isds_ctx)
        mock.login_cert.return_value = ErrorCode.SUCCESS

        credentials.login(mock, test=True)

        calls = [call.login_cert(sentinel.username, None, cert_format=KeyFormat.PEM, cert=sentinel.cert,
                                 key_format=KeyFormat.PEM, key=None, passphrase=None, test_env=True)]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_all_args(self):
        credentials = Certificate(
            username=sentinel.username, password=sentinel.password, cert_format=KeyFormat.DER, cert=sentinel.cert,
            key_format=KeyFormat.ENG, key=sentinel.key, passphrase=sentinel.passphrase)
        mock = Mock(spec=isds_ctx)
        mock.login_cert.return_value = ErrorCode.SUCCESS

        credentials.login(mock)

        calls = [call.login_cert(sentinel.username, sentinel.password, cert_format=KeyFormat.DER, cert=sentinel.cert,
                                 key_format=KeyFormat.ENG, key=sentinel.key, passphrase=sentinel.passphrase,
                                 test_env=False)]
        self.assertEqual(mock.mock_calls, calls)


class TestHotp(TestCase):
    def test_login(self):
        credentials = Hotp(username=sentinel.username, password=sentinel.password, hotp_code=sentinel.hotp)
        mock = Mock(spec=isds_ctx)
        mock.login_hotp.return_value = (ErrorCode.SUCCESS, sentinel.result)

        credentials.login(mock)

        self.assertEqual(mock.mock_calls,
                         [call.login_hotp(sentinel.username, sentinel.password, sentinel.hotp, test_env=False)])

    def test_login_failed(self):
        credentials = Hotp(username=sentinel.username, password=sentinel.password, hotp_code=sentinel.hotp)
        mock = Mock(spec=isds_ctx)
        mock.login_hotp.return_value = (ErrorCode.ERROR, sentinel.result)
        mock.long_message.return_value = sentinel.message

        with self.assertRaises(AuthenticationFailed) as catcher:
            credentials.login(mock)
        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        calls = [call.login_hotp(sentinel.username, sentinel.password, sentinel.hotp, test_env=False),
                 call.long_message()]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_test(self):
        credentials = Hotp(username=sentinel.username, password=sentinel.password, hotp_code=sentinel.hotp)
        mock = Mock(spec=isds_ctx)
        mock.login_hotp.return_value = (ErrorCode.SUCCESS, sentinel.result)

        credentials.login(mock, test=True)

        self.assertEqual(mock.mock_calls,
                         [call.login_hotp(sentinel.username, sentinel.password, sentinel.hotp, test_env=True)])


class TestTotp(TestCase):
    def test_login(self):
        credentials = Totp(username=sentinel.username, password=sentinel.password, totp_code=sentinel.totp)
        mock = Mock(spec=isds_ctx)
        mock.login_totp.return_value = (ErrorCode.SUCCESS, sentinel.result)

        credentials.login(mock)

        self.assertEqual(mock.mock_calls,
                         [call.login_totp(sentinel.username, sentinel.password, sentinel.totp, test_env=False)])

    def test_login_partial(self):
        credentials = Totp(username=sentinel.username, password=sentinel.password, totp_code=sentinel.totp)
        mock = Mock(spec=isds_ctx)
        mock.login_totp.return_value = (ErrorCode.PARTIAL_SUCCESS, sentinel.result)
        mock.long_message.return_value = sentinel.message

        with self.assertRaises(TotpCodeRequired) as catcher:
            credentials.login(mock)
        self.assertEqual(catcher.exception.code, ErrorCode.PARTIAL_SUCCESS)
        self.assertEqual(catcher.exception.message, sentinel.message)

        calls = [call.login_totp(sentinel.username, sentinel.password, sentinel.totp, test_env=False),
                 call.long_message()]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_failed(self):
        credentials = Totp(username=sentinel.username, password=sentinel.password, totp_code=sentinel.totp)
        mock = Mock(spec=isds_ctx)
        mock.login_totp.return_value = (ErrorCode.ERROR, sentinel.result)
        mock.long_message.return_value = sentinel.message

        with self.assertRaises(AuthenticationFailed) as catcher:
            credentials.login(mock)
        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        calls = [call.login_totp(sentinel.username, sentinel.password, sentinel.totp, test_env=False),
                 call.long_message()]
        self.assertEqual(mock.mock_calls, calls)

    def test_login_test(self):
        credentials = Totp(username=sentinel.username, password=sentinel.password, totp_code=sentinel.totp)
        mock = Mock(spec=isds_ctx)
        mock.login_totp.return_value = (ErrorCode.SUCCESS, sentinel.result)

        credentials.login(mock, test=True)

        self.assertEqual(mock.mock_calls,
                         [call.login_totp(sentinel.username, sentinel.password, sentinel.totp, test_env=True)])


class TestCredentials:
    """Test class which simulates Credentials interface."""

    def login(self, connection, test=False):
        """Simulate successful login."""
        pass


class TestClient(TestCase):
    # The connection test use the real Credentials to check whether login is performed.
    def test_connect(self):
        credentials = Password(sentinel.username, sentinel.password)
        client = Client(credentials)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.login_pwd.return_value = ErrorCode.SUCCESS

            client.connect()

        calls = [call(),
                 call().login_pwd(sentinel.username, password=sentinel.password, test_env=False)]
        self.assertEqual(context_mock.mock_calls, calls)

        self.assertEqual(client.connection, context_mock.return_value)

    def test_connect_timeout(self):
        credentials = Password(sentinel.username, sentinel.password)
        client = Client(credentials, timeout=10)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.set_timeout.return_value = ErrorCode.SUCCESS
            context_mock.return_value.login_pwd.return_value = ErrorCode.SUCCESS

            client.connect()

        calls = [call(),
                 call().set_timeout(10000),
                 call().login_pwd(sentinel.username, password=sentinel.password, test_env=False)]
        self.assertEqual(context_mock.mock_calls, calls)

        self.assertEqual(client.connection, context_mock.return_value)

    def test_connect_timeout_failed(self):
        client = Client(sentinel.credentials, timeout=10)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.set_timeout.return_value = ErrorCode.ERROR
            context_mock.return_value.long_message.return_value = sentinel.message

            with self.assertRaises(ConnectionFailed) as catcher:
                client.connect()

        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        self.assertEqual(context_mock.mock_calls, [call(), call().set_timeout(10000), call().long_message()])
        self.assertEqual(client.connection, context_mock.return_value)

    def test_connect_test(self):
        credentials = Password(sentinel.username, sentinel.password)
        client = Client(credentials, test=True)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.login_pwd.return_value = ErrorCode.SUCCESS

            client.connect()

        calls = [call(),
                 call().login_pwd(sentinel.username, password=sentinel.password, test_env=True)]
        self.assertEqual(context_mock.mock_calls, calls)

        self.assertEqual(client.connection, context_mock.return_value)

    def test_connection(self):
        # Test connection property
        credentials = Password(sentinel.username, sentinel.password)
        client = Client(credentials)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.login_pwd.return_value = ErrorCode.SUCCESS

            connection = client.connection

        calls = [call(),
                 call().login_pwd(sentinel.username, password=sentinel.password, test_env=False)]
        self.assertEqual(context_mock.mock_calls, calls)

        self.assertEqual(connection, context_mock.return_value)

    def test_connection_cached(self):
        # Test connection property returns cached result
        credentials = Password(sentinel.username, sentinel.password)
        client = Client(credentials)

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.login_pwd.return_value = ErrorCode.SUCCESS

            connection = client.connection
            connection2 = client.connection

        calls = [call(),
                 call().login_pwd(sentinel.username, password=sentinel.password, test_env=False)]
        self.assertEqual(context_mock.mock_calls, calls)

        self.assertEqual(connection, connection2)

    # All other test use TestCredentials, because login doesn't matter here.
    def test_destructor(self):
        client = Client(TestCredentials())

        with patch('isds.isds_ctx') as context_mock:

            client.connect()
            del client

        self.assertEqual(context_mock.mock_calls, [call(), call().logout()])

    def test_destructor_unconnected(self):
        client = Client(TestCredentials())

        with patch('isds.isds_ctx') as context_mock:
            del client

        self.assertEqual(context_mock.mock_calls, [])

    def test_ping(self):
        client = Client(TestCredentials())

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.ping.return_value = ErrorCode.SUCCESS

            client.ping()

        self.assertEqual(context_mock.mock_calls, [call(), call().ping()])

    def test_ping_failed(self):
        client = Client(TestCredentials())

        with patch('isds.isds_ctx') as context_mock:
            context_mock.return_value.ping.return_value = ErrorCode.ERROR
            context_mock.return_value.long_message.return_value = sentinel.message

            with self.assertRaises(ConnectionFailed) as catcher:
                client.ping()

        self.assertEqual(catcher.exception.code, ErrorCode.ERROR)
        self.assertEqual(catcher.exception.message, sentinel.message)

        self.assertEqual(context_mock.mock_calls, [call(), call().ping(), call().long_message()])
