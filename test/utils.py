"""Test utilities."""
from isds import isds_date, isds_hash, isds_time


def _is_comparable(obj):
    """Return whether object can be compared."""
    # Basically checks ISDS objects without explicit comparison methods
    return not hasattr(obj, '__swig_destroy__') or isinstance(obj, (isds_date, isds_time, isds_hash))


def _get_isds_data(obj):
    attributes = [a for a in dir(obj) if not a.startswith('_') and a not in ('this', 'thisown')]
    data = {}
    for attr in attributes:
        data[attr] = _get_data(getattr(obj, attr))
    return data


def _get_data(value):
    if isinstance(value, list):
        return [_get_data(i) for i in value]
    elif _is_comparable(value):
        return value
    else:
        return _get_isds_data(value)


def test_isds_object(case, obj, key, value, empty_value=None):
    """Test ISDS object attribute getter and setter.

    Arguments:
        case (TestCase): TestCase instance.
        obj (object): ISDS object.
        key (str): ISDS object attribute to test.
        value (any): Non-empty test value.
        empty_value (any): Empty test value.
    """
    initial_data = dict(_get_isds_data(obj), **{key: empty_value})
    changed_data = dict(_get_isds_data(obj), **{key: _get_data(value)})

    # Check plain object has all data empty
    case.assertEqual(_get_isds_data(obj), initial_data)
    # Change value and check new data
    setattr(obj, key, value)
    case.assertEqual(_get_isds_data(obj), changed_data)
    # Check value to None and check all data are empty
    setattr(obj, key, empty_value)
    case.assertEqual(_get_isds_data(obj), initial_data)
    # Change value and check new data again
    setattr(obj, key, value)
    case.assertEqual(_get_isds_data(obj), changed_data)
